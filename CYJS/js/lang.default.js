function CYDefaultLang(lang) {
    if (lang == 'en') {
        cyString['DAYS'] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        cyString['DAYS SHORT'] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        cyString['MONTHS'] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        cyString['MONTHS SHORT'] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        cyString['PROJECT'] = "CYJS PROJECT";
        cyString['MESSAGE'] = "MESSAGE";
        cyString['CONFIRMATION'] = "CONFIRMATION";
        cyString['PLEASE INPUT'] = "PLEASE INPUT";
        cyString['PLEASE CONFIRM'] = "Please confirm.";
        cyString['OK'] = "Ok";
        cyString['CANCEL'] = "Cancel";
        cyString['ERROR LOADING'] = "Error loading";
        cyString['HELLO'] = "Hello ";
        cyString['YOUR LOGIN'] = "Your login ";
        cyString['LOGOFF'] = "Logoff ?";
        cyString['BAD LOGIN'] = "Bad login parameters";
        cyString['BAD SELECTION'] = "Bad selection.";
        cyString['ALL'] = "All";
        cyString['FILTER'] = "Filter";
        cyString['RESET ALL FILTERS'] = "Reset all filters";
        cyString['UPLOAD HERE'] = "Drag/drop a file or click here";
        cyString['DRAG HERE'] = "Drag a file here";
        cyString['DELETE'] = "Delete";
        cyString['BAD BROWSER'] = "Bad browser";
        cyString['BAD FILETYPE'] = "Bad filetype";
        cyString['ERROR {{statusCode}}'] = "Error {{statusCode}}";
        cyString['MAX FILES REACHED'] = "Max files reached";
        cyString['FILE IS TOO BIG ({{filesize}}MiB). MAX FILESIZE: {{maxFilesize}}MiB.'] = "File is too big ({{filesize}}MiB), max filesize is {{maxFilesize}}MiB";
        cyString['NO FILE'] = "No file";
        cyString['DOWNLOAD'] = "Download";
        cyString['NEW FILE'] = "New file";
        cyString['SYNC'] = "Sync in progress ..";
        cyString['OPEN'] = "Open";
        cyString['CLOSE'] = "Close";
        cyString['FULLSCREEN'] = "Full screen";
        cyString['LOCK'] = "Lock";
        cyString['UNLOCK'] = "Unlock";
        cyString['DUPLICATE'] = 'Duplicate';
        cyString['SHARE'] = 'Public share';
        cyString['LOCAL SHARE'] = 'Share';
        cyString['LOADING'] = 'Loading ...';
        cyString['YES'] = 'Yes';
        cyString['NO'] = 'No';
        cyString['NO NETWORK - SYNC'] = 'No network - Sync this request ?';
        cyString['RENAME'] = 'Rename';
        cyString['DESCRIPTION'] = 'Description';
        cyString['FILENAME'] = 'Filename';
        cyString['UNDEFINED'] = 'Undefined';
        cyString['FILE'] = 'File';
        cyString['FILES'] = 'Files';
        cyString['ERROR'] = 'Error';
        cyString['EDIT'] = 'Edit';
        cyString['DESKTOP'] = 'Desktop';
        cyString['SAVE'] = 'Save';
        cyString['SAVED'] = 'Saved';
        cyString['RESTORE'] = 'Restore';
        cyString['PASSWORD'] = 'Password';
        cyString['COPY'] = 'Copy';
        cyString['PASTE'] = 'Paste';
        cyString['MAILTO'] = 'Mail to';
        cyString['RESET'] = 'Reset';
        cyString['RELOAD'] = 'Reload';
        cyString['DONE'] = 'Successfully done.';
        cyString['CLEAR'] = 'Clear';
        cyString['THEME'] = 'Theme';
        cyString['DOUBLECLICKME'] = 'Double click me';
        cyString['SELECTME'] = 'Click to select';
        cyString['BYTE'] = 'byte';
        cyString['BYTES'] = 'bytes';
        cyString['DATEFORMAT'] = 'mm/dd/yyyy';
        cyString['TIMEFORMAT'] = 'hh:mm';
        cyString['TIMELONGFORMAT'] = 'hh:mm:ss';
    } else if (lang == 'th') {
        cyString['DAYS'] = ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'วเสาร์'];
        cyString['DAYS SHORT'] = ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'];
        cyString['MONTHS'] = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'];
        cyString['MONTHS SHORT'] = ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'];
        cyString['PROJECT'] = "CYJS PROJECT";
        cyString['MESSAGE'] = "ข่าวสาร";
        cyString['CONFIRMATION'] = "ยืนยัน";
        cyString['PLEASE INPUT'] = "กรุณาใส่ข้อมูล";
        cyString['PLEASE CONFIRM'] = "กรุณายืนยัน";
        cyString['OK'] = "ตกลง";
        cyString['CANCEL'] = "ยกเลิก";
        cyString['ERROR LOADING'] = "การโหลดข้อผิดพลาด";
        cyString['HELLO'] = "สวัสดี ";
        cyString['YOUR LOGIN'] = "การเข้าสู่ระบบของคุณ ";
        cyString['LOGOFF'] = "ออกจากระบบ ?";
        cyString['BAD LOGIN'] = "พารามิเตอร์การเข้าสู่ระบบที่ไม่ถูกต้อง";
        cyString['BAD SELECTION'] = "เลือกไม่ดี";
        cyString['ALL'] = "ทั้งหมด";
        cyString['FILTER'] = "กรอง";
        cyString['RESET ALL FILTERS'] = "รีเซ็ตตัวกรองทั้งหมด";
        cyString['UPLOAD HERE'] = "ลาก / วางไฟล์หรือคลิกที่นี่";
        cyString['DRAG HERE'] = "ลากไฟล์ที่นี่";
        cyString['DELETE'] = "ลบ";
        cyString['BAD BROWSER'] = "เบราว์เซอร์ที่ไม่ถูกต้อง";
        cyString['BAD FILETYPE'] = "ประเภทไฟล์ไม่ถูกต้อง";
        cyString['ERROR {{statusCode}}'] = "ความผิดพลาด {{statusCode}}";
        cyString['MAX FILES REACHED'] = "ไฟล์ถึงสูงสุดแล้ว";
        cyString['FILE IS TOO BIG ({{filesize}}MiB). MAX FILESIZE: {{maxFilesize}}MiB.'] = "ไฟล์มีขนาดใหญ่เกินไป ({{filesize}}MiB), ขนาดไฟล์สูงสุดคือ {{maxFilesize}}MiB";
        cyString['NO FILE'] = "ไม่มีไฟล์";
        cyString['NO NETWORK - SYNC'] = "ไม่มีเครือข่ายซิงโครไนซ์คำขอนี้ ?";
        cyString['DOWNLOAD'] = "ดาวน์โหลด";
        cyString['NEW FILE'] = "ไฟล์ใหม่";
        cyString['SYNC'] = "การประสานความคืบหน้าใน ..";
        cyString['OPEN'] = "เปิด";
        cyString['CLOSE'] = "ปิด";
        cyString['FULLSCREEN'] = "เต็มจอ";
        cyString['LOCK'] = "ล็อค";
        cyString['DUPLICATE'] = 'ซ้ำ';
        cyString['SHARE'] = 'หุ้นสาธารณะ';
        cyString['LOCAL SHARE'] = 'หุ้น';
        cyString['UNLOCK'] = "ปลดล็อค";
        cyString['LOADING'] = 'กำลังโหลด ...';
        cyString['YES'] = 'ใช่';
        cyString['NO'] = 'ไม่';
        cyString['RENAME'] = 'ตั้งชื่อใหม่';
        cyString['DESCRIPTION'] = 'ลักษณะ';
        cyString['FILENAME'] = 'ลักษณะ';
        cyString['UNDEFINED'] = 'ไม่ได้กำหนด';
        cyString['FILE'] = 'ไฟล์';
        cyString['FILES'] = 'ไฟล์';
        cyString['ERROR'] = 'ความผิดพลาด';
        cyString['EDIT'] = 'แก้ไข';
        cyString['DESKTOP'] = 'เดสก์ทอป';
        cyString['SAVE'] = 'ประหยัด';
        cyString['SAVED'] = 'ทำการสำรองข้อมูลแล้ว';
        cyString['RESTORE'] = 'ฟื้นฟู';
        cyString['PASSWORD'] = 'รหัสผ่าน';
        cyString['COPY'] = 'สำเนา';
        cyString['PASTE'] = 'แปะ';
        cyString['MAILTO'] = 'mail ไปที่';
        cyString['RESET'] = 'รีเซ็ต';
        cyString['RELOAD'] = 'โหลด';
        cyString['DONE'] = 'เสร็จเรียบร้อยแล้ว';
        cyString['CLEAR'] = 'ปราบ';
        cyString['THEME'] = 'กระทู้';
        cyString['DOUBLECLICKME']
        cyString['SELECTME'] = 'คลิกเพื่อเลือก';
        'คลิกสองครั้งฉัน';
        cyString['BYTE'] = 'byte';
        cyString['BYTES'] = 'bytes';
        cyString['DATEFORMAT'] = 'mm/dd/yyyy';
        cyString['TIMEFORMAT'] = 'hh:mm';
        cyString['TIMELONGFORMAT'] = 'hh:mm:ss';
    } else if (lang == 'de' || lang == 'an') {
        cyString['DAYS'] = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
        cyString['DAYS SHORT'] = ['Son', 'Mon', 'Die', 'Mit', 'Don', 'Fre', 'Sam'];
        cyString['MONTHS'] = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
        cyString['MONTHS SHORT'] = ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];
        cyString['PROJECT'] = "CYJS PROJEKT";
        cyString['MESSAGE'] = "ANZEIGE";
        cyString['CONFIRMATION'] = "BESTATIGUNG";
        cyString['PLEASE INPUT'] = "BITTE EINGEBEN";
        cyString['PLEASE CONFIRM'] = "BITTE BESTATIGEN";
        cyString['OK'] = "Ok";
        cyString['CANCEL'] = "ANNULLIEREN";
        cyString['ERROR LOADING'] = "Ladefehler";
        cyString['HELLO'] = "Hallo ";
        cyString['YOUR LOGIN'] = "Dein Login ";
        cyString['LOGOFF'] = "Abmelden ?";
        cyString['BAD LOGIN'] = "Falsche Verbindungselemente";
        cyString['BAD SELECTION'] = "Falsche Auswahl";
        cyString['ALL'] = "Alle";
        cyString['FILTER'] = "Filter";
        cyString['RESET ALL FILTERS'] = "Alle Filter zurücksetzen";
        cyString['UPLOAD HERE'] = "Legen Sie eine Datei ab oder klicken Sie hier";
        cyString['DRAG HERE'] = "Ziehen Sie eine Datei hier";
        cyString['DELETE'] = "Löschen";
        cyString['BAD BROWSER'] = "Schlechter Browser";
        cyString['BAD FILETYPE'] = "Falscher Dateityp";
        cyString['ERROR {{statusCode}}'] = "Error {{statusCode}}";
        cyString['MAX FILES REACHED'] = "Max Dateien erreicht";
        cyString['FILE IS TOO BIG ({{filesize}}MiB). MAX FILESIZE: {{maxFilesize}}MiB.'] = "Datei ist zu groß ({{filesize}}MiB), maximale Größe {{maxFilesize}}MiB";
        cyString['NO FILE'] = "Keine Datei";
        cyString['NO NETWORK - SYNC'] = "Kein Netzwerk - Synchronisieren Sie diese Anforderung ?";
        cyString['DOWNLOAD'] = "Herunterladen";
        cyString['NEW FILE'] = "Neue Datei";
        cyString['SYNC'] = "Synchronisation läuft ..";
        cyString['OPEN'] = "Öffnen";
        cyString['CLOSE'] = "Schließen";
        cyString['FULLSCREEN'] = "Ganzer Bildschirm";
        cyString['LOCK'] = "Sperren";
        cyString['DUPLICATE'] = "Duplikat";
        cyString['SHARE'] = 'Öffentlicher Anteil';
        cyString['LOCAL SHARE'] = 'Anteile';
        cyString['UNLOCK'] = "Freischalten";
        cyString['LOADING'] = 'Geladen ...';
        cyString['YES'] = 'Ja';
        cyString['NO'] = 'Nein';
        cyString['RENAME'] = 'Umbenennen';
        cyString['DESCRIPTION'] = 'Beschreibung';
        cyString['FILENAME'] = 'Dateiname';
        cyString['UNDEFINED'] = 'Nicht definiert';
        cyString['FILE'] = 'Datei';
        cyString['FILES'] = 'Dateien';
        cyString['ERROR'] = 'Error';
        cyString['EDIT'] = 'Bearbeiten';
        cyString['DESKTOP'] = 'Desktop';
        cyString['SAVE'] = 'Speichern';
        cyString['SAVED'] = 'Gespeichert';
        cyString['RESTORE'] = 'Wiederherstellen';
        cyString['PASSWORD'] = 'Passwort';
        cyString['COPY'] = 'Kopieren';
        cyString['PASTE'] = 'Einfügen';
        cyString['MAILTO'] = 'Mail an';
        cyString['RESET'] = 'Zurücksetzen';
        cyString['RELOAD'] = 'Neu laden';
        cyString['DONE'] = 'Operation durchgeführt.';
        cyString['CLEAR'] = 'Löschen';
        cyString['THEME'] = 'Schablone';
        cyString['DOUBLECLICKME'] = 'Doppelklick mich';
        cyString['SELECTME'] = 'Klicke um auszuwählen';
        cyString['BYTE'] = 'byte';
        cyString['BYTES'] = 'bytes';
        cyString['DATEFORMAT'] = 'tt/mm/jjjj';
        cyString['TIMEFORMAT'] = 'hh:mm';
        cyString['TIMELONGFORMAT'] = 'hh:mm:ss';
    } else {
        cyString['DAYS'] = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
        cyString['DAYS SHORT'] = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
        cyString['MONTHS'] = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        cyString['MONTHS SHORT'] = ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc'];
        cyString['PROJECT'] = "PROJET CYJS";
        cyString['MESSAGE'] = "MESSAGE";
        cyString['CONFIRMATION'] = "CONFIRMATION";
        cyString['PLEASE INPUT'] = "VEUILLEZ SAISIR";
        cyString['PLEASE CONFIRM'] = "Veuillez confirmer.";
        cyString['OK'] = "Ok";
        cyString['CANCEL'] = "Annuler";
        cyString['ERROR LOADING'] = "Erreur de chargement";
        cyString['HELLO'] = "Bonjour ";
        cyString['YOUR LOGIN'] = "Votre login ";
        cyString['LOGOFF'] = "D&eacute;connecter ?";
        cyString['BAD LOGIN'] = "El&eacute;ments de connexion incorrects";
        cyString['BAD SELECTION'] = "Sélection incorrecte.";
        cyString['ALL'] = "Tout";
        cyString['FILTER'] = "Filtre";
        cyString['RESET ALL FILTERS'] = "Effacer tous les filtres";
        cyString['UPLOAD HERE'] = "D&eacute;posez un fichier ou cliquez ici";
        cyString['DRAG HERE'] = "Glissez un fichier ici";
        cyString['DELETE'] = "Supprimer";
        cyString['BAD BROWSER'] = "Navigateur non support&eacute;";
        cyString['BAD FILETYPE'] = "Type de fichier interdit";
        cyString['ERROR {{statusCode}}'] = "Erreur {{statusCode}}";
        cyString['MAX FILES REACHED'] = "Nombre maxi de fichiers atteint";
        cyString['FILE IS TOO BIG ({{filesize}}MiB). MAX FILESIZE: {{maxFilesize}}MiB.'] = "Fichier trop gros ({{filesize}}MiB), taille maxi {{maxFilesize}}MiB";
        cyString['NO FILE'] = "Pas de fichier";
        cyString['NO NETWORK - SYNC'] = "Pas de réseau - Synchroniser cette requête ?";
        cyString['DOWNLOAD'] = "T&eacute;l&eacute;charger";
        cyString['NEW FILE'] = "Nouveau fichier";
        cyString['SYNC'] = "Synchronisation en cours ..";
        cyString['OPEN'] = "Ouvrir";
        cyString['CLOSE'] = "Fermer";
        cyString['FULLSCREEN'] = "Plein écran";
        cyString['LOCK'] = "Verrouiller";
        cyString['DUPLICATE'] = "Dupliquer";
        cyString['SHARE'] = 'Partage public';
        cyString['LOCAL SHARE'] = 'Partager';
        cyString['UNLOCK'] = "Déverrouiller";
        cyString['LOADING'] = 'Chargement ...';
        cyString['YES'] = 'Oui';
        cyString['NO'] = 'Non';
        cyString['RENAME'] = 'Renommer';
        cyString['DESCRIPTION'] = 'Description';
        cyString['FILENAME'] = 'Nom du fichier';
        cyString['UNDEFINED'] = 'Ind&eacute;fini';
        cyString['FILE'] = 'Fichier';
        cyString['FILES'] = 'Fichiers';
        cyString['ERROR'] = 'Erreur';
        cyString['EDIT'] = 'Editer';
        cyString['DESKTOP'] = 'Bureau';
        cyString['SAVE'] = 'Sauvegarder';
        cyString['SAVED'] = 'Sauvegarde effectuée';
        cyString['RESTORE'] = 'Restaurer';
        cyString['PASSWORD'] = 'Mot de passe';
        cyString['COPY'] = 'Copier';
        cyString['PASTE'] = 'Coller';
        cyString['MAILTO'] = 'Mail à';
        cyString['RESET'] = 'Réinitialiser';
        cyString['RELOAD'] = 'Recharger';
        cyString['DONE'] = 'Opération effectuée.';
        cyString['CLEAR'] = 'Effacer';
        cyString['THEME'] = 'Thème';
        cyString['DOUBLECLICKME'] = 'Double-cliquez moi';
        cyString['SELECTME'] = 'Cliquez pour sélectionner';
        cyString['BYTE'] = 'octet';
        cyString['BYTES'] = 'octets';
        cyString['DATEFORMAT'] = 'jj/mm/aaaa';
        cyString['TIMEFORMAT'] = 'hh:mm';
        cyString['TIMELONGFORMAT'] = 'hh:mm:ss';
    }
}
