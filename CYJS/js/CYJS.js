const DEBUG = 1;
// bit 1 pour plus de debug
const DEBUG_MORE = 2;
// bit 2 pour debug dynamique
const DEBUG_DYN = 4;
// Valeur à retourner par onRemove dans CYSelectableButtonPool pour ne pas supprimer un choix
const CYSELECTABLEDONTREMOVE = -1;
var debug = DEBUG;
// Available languages
var cyAvailableLang = "fr,en,th,de,an";
// Taille mini nécessaire pour sidemenu permanent
// Doit correspondre à max-width dans section media screen de style.css
var cyMinWindowWidth = 800;
// Delai par defaut pour autofade popup
var cyAlertDelay = 0;
// Duree affichage tooltip
var cyTooltipDuration = 5000;
// Time-out gestion interne double-click
var cyDoubleClicTimeout = 300;
// Web service pour CYFileList() et ..
var cyWebSvc = '';
// Sidemenu auto si resize window
var cySideMenuAuto = true;
// Auto sidemenu closed at startup
cySideMenuClosed = false;
// Pour éviter sidemenu auto après resize
cyUserClickedIconMenu = false;
// Definir ici la fonction de login
var CYLoginFct = function () {
    // Login simple sans controle
    CYLogin('luke');
    CYLogged('luke');
    CYLoadPage(cyDefaultPage);
};
var cyCurrentPage = '';
var cyUsername = '';
var cyLang = '';
// Largeur sidemenu
var cySideMenuWidth, cyInitialSideMenuWidth;
// Largeur mini pour ne montrer que les icones
var cySideMenuSmallWidth = "62px";
// Section body pour ajout HTML dynamique
var cyPageContent = "#IDpagecontent";
// Largeur popup alert standard
cyAlertWidth = 400;
// Largeur popup confirm standard
cyConfirmWidth = 400;
// Largeur popup prompt standard
cyPromptWidth = 400;
// CSS pattern
var cyCSS = {
    Font: "MavenPro Medium",
    FontBold: "MavenPro Black",
    AltBackgroundColor: "#f5f5f5",
    BackgroundColor: "#007fff",
    BackgroundColorAlt: "#fff",
    BorderColor: "#e5d5d5",
    ButtonBackgroundColor: "#eee",
    HoverTextColor: "#fff",
    HoverBackgroundColor: "#007fff",
    InputShadow: "#38c",
    InputBackgroundColor: "#fff",
    TextColor: "#333",
    ReverseTextColor: "#fff",
    TooltipTextColor: "#fff",
    TooltipBackgroundColor: "#333",
    TableHeaderColor: "#333",
    TableHeaderBackgroundColor: "#c6c6c6"
};
// Durée par défaut pour effet open/close
var cyEffectDuration = 200;
// Fonction Iconification par défaut - appelée par CYIconify() avec true ou false
// selon entrée ou sortie de CYIconify()
var CYIsIconified = function (id, etat) {
    debug && console.log("CYJS : ICONIFICATION " + id + " = " + etat);
}
// Doit retourner true si iconification possible
var CYIsIconifyAble = function (id, etat) {
    debug && console.log("CYJS : ICONIFICATION POSSIBLE " + id + " = " + etat);
    return true;
}
// ------------------------------------------
// Rien ne devrait être changé à partir d'ici
// ------------------------------------------
// Traductions
var cyString = [];
// Server name
var cyServerName = location.protocol + "//" + location.host;
// Variables GET
var GET = {};
// true quand clic gauche appuyé
var cyMouseDown = false;
// Position courante souris
var cyCurrentMousePosition = { x: -1, y: -1 };
// Objet Javascript sous le curseur
var cyCurrentObject = null;
// Id (si défini) sous le curseur
var cyCurrentId = null;
// Classes (si définies) sous le curseur
var cyCurrentClass = null;
// Tag courant sous le curseur
var cyCurrentTag = null;
// Largeur fenetre courant
var cyCurrentWidth = 0;
// Keepalive - 0 pour inhiber
var cyKeepAlive = 30000;
// localforage
var cyDB = false;
// Cache local
var cyCache = true;
// Auto sync
var cySync = true;
// Network
var cyCheckNetwork = true;
// Device touch enabled
var cyIsmobile = ('ontouchstart' in document.documentElement) == true;
// Page courante dans #IDmain
var cyCurrentPage = "";
// Timer pour gestion doucle-click CYClick()
var cyDblClickTimer = 0;
// body background image to set when CYLogoff(), default is original
var cyBackImg = '';
// Dernière touche utilisée
var cyKey = -1;
// Dernier code clavier utilisé
var cyKeyCode = -1;
// Etat touche CTRL
var cyKeyCTRL = -1;
// Dernier mot entré (validé par CR)
var cyWord = '';
// Log custom
//var cyOriginalLog = console.log;
//console.log = function() {
//    cyOriginalLog.apply(this, arguments);
//};
// Specif IE
if (navigator.systemLanguage != undefined) {
    // Math.trunc() n'existe pas :(
    Math.trunc = function (a) {
        return (a > 0 ? Math.floor(a) : Math.ceil(a));
    };
}
// Variables GET - comme par ex lang=fr
document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
    function decode(s) {
        return decodeURIComponent(s.split("+").join(" "));
    }
    GET[decode(arguments[1])] = decode(arguments[2]);
});

// Init du framework
function CYInit(callback) {
    // Francais par defaut
    if (cyLang == '' || GET["lang"] != undefined)
        cyLang = (GET["lang"] == undefined) ? 'fr' : GET["lang"];
    // Verifie langues dispos
    if (cyAvailableLang.search(cyLang) == -1)
        cyLang = 'fr';
    debug && console.log('CYJS : CYInit default language is ' + cyLang + ', GET=' + JSON.stringify(GET));
    if (typeof (window["CYDefaultLang"]) == 'function')
        CYDefaultLang(cyLang);
    // Crée des var CSS selon cyCSS
    let s = document.createElement("style");
    let datacss = ':root {';
    s.type = "text/css";
    for (css in cyCSS) {
        if (typeof cyCSS[css] == "string")
            datacss += '--cyCSS' + css + ': ' + cyCSS[css] + ';';
    }
    datacss += ' }';
    s.appendChild(document.createTextNode(datacss));
    let h = document.head || document.getElementsByTagName("head")[0];
    h.appendChild(s);
    // Ajoute methode findall qui fait comme find mais traite en plus le root selector
    $.fn.findall = function (selector) {
        return this.filter(selector).add(this.find(selector));
    };
    // Extraire tous les styles d'un objet - $(..).getStyleObject()
    $.fn.getStyleObject = function () {
        var dom = this.get(0);
        var style;
        var returns = {};
        if (window.getComputedStyle) {
            var camelize = function (a, b) {
                return b.toUpperCase();
            }
            style = window.getComputedStyle(dom, null);
            for (var i = 0; i < style.length; i++) {
                var prop = style[i];
                var camel = prop.replace(/\-([a-z])/g, camelize);
                var val = style.getPropertyValue(prop);
                returns[camel] = val;
            }
            return returns;
        }
        if (dom.currentStyle) {
            style = dom.currentStyle;
            for (var prop in style) {
                returns[prop] = style[prop];
            }
            return returns;
        }
        return this.css();
    };
    // Position souris temps reel
    $(document).mousemove(function(event) {
        cyCurrentMousePosition.x = event.clientX;
        cyCurrentMousePosition.y = event.clientY;
        cyCurrentObject = document.elementFromPoint(event.clientX,event.clientY);
        cyCurrentId = $(cyCurrentObject).attr('id');
        cyCurrentClass = $(cyCurrentObject).attr('class');
        cyCurrentTag = cyCurrentObject.tagName;
    }); 
    // Saisie fantome clavier
    $(document).keyup(function(ev) {
        cyKey = ev.key;
        cyKeyCode = ev.keyCode;
        cyKeyCTRL = ev.ctrlKey;
        if (ev.keyCode == 13) {
            cyWord = "";
        } else
            cyWord += ev.key;
        // CTRL-I - Debugger d'objets intégré
        if(cyKeyCTRL && cyKey == 'i') {
            function CYInfoObject(object) {
                let msg = '';
                msg += "<div>";
                if(object.nextElementSibling != null)
                    msg += "<button><i class='fa fa-long-arrow-right'></i></button>";
                if(object.previousElementSibling != null)
                    msg += "<button><i class='fa fa-long-arrow-left'></i></button>";
                if(object.children.length != 0)
                    msg += "<button><i class='fa fa-level-down'></i></button>";
                if(object.parentElement != null)
                    msg += "<button><i class='fa fa-level-up'></i></button>";
                msg += "</div>";
                msg += "<div><pre>Tag:<br>    " + object.tagName + '<br>';
                msg += 'Attributs:<br>';
                // Construit la liste de tous les attributs
                $(object).each(function () {
                    $.each(this.attributes, function () {
                        if (this.specified)
                            msg += sprintf('    %s="%s"<br>', this.name, this.value);
                    });
                });
                // Construit la liste de toutes les data
                msg += "Data:<br>";
                let data = $(object).data();
                for (d in data) {
                    let v = data[d];
                    if (typeof v == 'object')
                        v = '{ ... }';
                    msg += sprintf("    %s=%s<br>", d, v);
                }
                // Construit la liste de tous les CSS 
                msg += "CSS:<br>";
                let css = $(object).getStyleObject();
                for (c in css)
                    msg += sprintf("    %-30.30s : %-20.20s<br>", c, css[c]);
                msg += "</pre></div>";
                return msg;
            };
            let idAlert = CYAlert({ classes: "ClassDivDebug", title: "Information sur l'objet", height: "400", width: "500px", message: CYInfoObject(cyCurrentObject), modal: false, autofade: 0,
                openFunction: function(alert) {
                    let _CYInfoObject = function(alert) {
                        $(alert).find('button').on('click', function() {
                            let object = $(alert).data('object');
                            $(object).removeClass('ClassDivDebugHover');
                            let button = $(this).find('i').attr('class');
                            switch(button) {
                                case 'fa fa-level-up': object = object.parentElement; break;
                                case 'fa fa-level-down': object = object.children[0]; break;
                                case 'fa fa-long-arrow-left': object = object.previousElementSibling; break;
                                case 'fa fa-long-arrow-right': object = object.nextElementSibling; break;
                            }
                            $(alert).data('object', object);
                            $(alert).find('div').html(CYInfoObject(object));
                            $(object).addClass('ClassDivDebugHover');
                            _CYInfoObject(alert);
                        });
                    };
                    _CYInfoObject(alert);
                },
                closeFunction: function(alert) {
                    let object = $(alert).data('object');
                    $(object).removeClass('ClassDivDebugHover');
                }
            });
            // L'objet courrant est stocké en data, ce qui permet de gérer plusieurs debuggers simultanément
            $(idAlert).data('object', cyCurrentObject);
            $(cyCurrentObject).addClass('ClassDivDebugHover');
        }
    });
    // Gérer le double-click
    // $(id).CYCclick(callbackclick, callbackdblclick);
    // ou
    // $(id).CYCclick(callbackclick) => dans ce cas le callback sera appelé avec un 3ème arg valant true
    $.fn.CYClick = function (callbackclick, callbackdblclick) {
        var time0 = 0;
        var that = this;

        this.on("click", function (event) {
            var d = new Date();
            var time1 = d.getTime();

            event.stopImmediatePropagation();
            if (time1 - time0 <= cyDoubleClicTimeout) {
                clearTimeout(cyDblClickTimer);
                if (typeof callbackdblclick === "function")
                    callbackdblclick(this, event);
                else if (typeof callbackclick === "function")
                    callbackclick(this, event, true);
                return;
            } else {
                time0 = time1;
                if (typeof callbackclick === "function") {
                    cyDblClickTimer = setTimeout(function () {
                        callbackclick(this, event, false);
                    }, cyDoubleClicTimeout);
                }
            }
        });
    };
    // Centrage absolu d'un objet selon l'argument
    $.fn.CYCenter = function (refer) {
        if (refer == undefined || refer == null || refer == '')
            refer = window;
        this.css("position", "absolute");
        this.css("top", Math.max(0, (($(refer).height() - $(this).outerHeight()) / 2) +
            $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(refer).width() - $(this).outerWidth()) / 2) +
            $(window).scrollLeft()) + "px");
        return this;
    };
    // Positionne un objet en top
    // Exemples :
    //   $("#id").CYToTop()
    //   $("#id").CYToTop(".ui-dialog,.ClassWindow,.ClassIconFile")
    $.fn.CYToTop = function(selector) {
        if(selector == undefined)
            selector = '*';
        let zindex = CYMaxZindex(selector);
        let oldzindex = this.css("z-index");
        if (oldzindex == undefined || oldzindex == zindex)
            return;
        this.css("z-index", zindex + 1);
    };
    // Force affichage de l'obj dans la page s'il dépasse 
    $.fn.CYReCenter = function (options) {
        let offset = $(this).offset();
        if (typeof options != 'object')
            options = {};
        if (options.defaultMargin == undefined)
            options.defaultMargin = 10;
        if ($(cyPageContent).outerWidth() > 0 && $(cyPageContent).outerHeight() > 0) {
            console.log("CENTER1 " + JSON.stringify(offset));
            // Controle selon largeur
            if (offset.left + $(this).outerWidth() > $(cyPageContent).outerWidth())
                offset.left = $(cyPageContent).outerWidth() - $(this).outerWidth() - options.defaultMargin;
            // Controle selon hauteur
            if (offset.top + $(this).outerHeight() > $(cyPageContent).outerHeight())
                offset.top = $(cyPageContent).outerHeight() - $(this).outerHeight() - options.defaultMargin;
            console.log("CENTER2 " + JSON.stringify(offset));
            $(this).css({ left: offset.left + 'px', top: offset.top + 'px' });
        }
        return this;
    };
    // Retourne hauteur visible
    // var h = $("#IDmain").visibleHeight()
    $.fn.visibleHeight = function () {
        var elBottom, elTop, scrollBot, scrollTop, visibleBottom, visibleTop;
        scrollTop = $(window).scrollTop();
        scrollBot = scrollTop + $(window).height();
        elTop = this.offset().top;
        elBottom = elTop + this.outerHeight();
        visibleTop = elTop < scrollTop ? scrollTop : elTop;
        visibleBottom = elBottom > scrollBot ? scrollBot : elBottom;
        return visibleBottom - visibleTop;
    };
    /// Pose/enlève un spinner sur un objet, avec un modal optionnel
    // $("#mondiv").CYSpinLoader() => positionner par défaut
    // $("#mondiv").CYSpinLoader(false) => l'enlever
    // $("#mondiv").CYSpinLoader({ css1 : 'XXX', css2 : 'YYY', ..., modal: true|false, opaque: true|false|0.5 }) => customiser
    // Exemple : $("#IDmain").CYSpinLoader({ modal: true, opaque: false, "border-top": "8px solid yellow" })
    $.fn.CYSpinLoader = function (status) {
        if (status != undefined && status == false)
            $(this).find('.ClassSpinLoader,.ClassSpinLoaderModal').remove();
        else {
            if (!$(this).find('.ClassSpinLoader').length)
                $(this).append('<span class="ClassSpinLoader"></span>');
            if (typeof status == 'object') {
                let props = Object.keys(status);
                let that = this;
                props.forEach(function (prop) {
                    if (prop == 'modal') {
                        if (status[prop] == false) {
                            $(that).find('.ClassSpinLoaderModal').remove();
                            $(that).find('.ClassSpinLoader').css('z-index', '');
                        }
                        else if (!$(that).find('.ClassSpinLoaderModal').length) {
                            let zi = CYMaxZindex() + 1;
                            $(that).find('.ClassSpinLoader').before('<div class="ClassSpinLoaderModal"></div>');
                            $(that).find('.ClassSpinLoaderModal').css('z-index', zi);
                            // Le spinner par-dessus le voile
                            $(that).find('.ClassSpinLoader').css('z-index', zi + 1);
                        }
                    }
                    else if (prop == 'opaque') {
                        if (status[prop] == false)
                            $(that).find('.ClassSpinLoaderModal').css("opacity", "");
                        else
                            $(that).find('.ClassSpinLoaderModal').css("opacity", status[prop] == true ? "1" : status[prop]);
                    }
                    else
                        $(that).find('.ClassSpinLoader').css(prop, status[prop]);
                });
            }
        }
        return this;
    };
    // $(id).CYToFront()
    // $(id).CYToFront(".diviframe,.iosmenusub")
    $.fn.CYToFront = function (brothers) {
        let zindex = 1;
        if (brothers == undefined)
            brothers = '*';
        $(brothers).each(function () {
            if ($(this).css("display") != "none") {
                let izindex = $(this).css('z-index');
                if (izindex != 'auto' && izindex >= zindex)
                    zindex = (1 * izindex) + 1;
            }
        });
        debug && console.log("CYJS : CYToFront tag=" + $(this).prop("tagName") + " id=" + $(this).attr("id") + "/" + zindex);
        $(this).css("z-index", zindex);
        return this;
    };
    /*
     * Raccourcis des fonctions pour jQuery.
     */
    $.fn.CYInput = function (cyoptions) { CYInput($(this)); return this; };
    $.fn.CYTooltip = function (cyoptions) { CYTooltip($(this), cyoptions); return this; };
    $.fn.CYButton = function (cyoptions) { CYButton($(this), cyoptions); return this; };
    $.fn.CYCheckBox = function (cyoptions) { CYCheckBox($(this), cyoptions); return this; };
    $.fn.CYSpinner = function (cyoptions) { CYSpinner($(this), cyoptions); return this; };
    $.fn.CYSelect = function (cyoptions) { CYSelect($(this), cyoptions); return this; };
    $.fn.CYSortable = function (cyoptions) { let ret = CYSortable($(this), cyoptions); return (typeof cyoptions == 'string' && cyoptions == 'get') ? ret : this };
    $.fn.CYSelectable = function (cyoptions) { let ret = CYSelectable($(this), cyoptions); return (typeof cyoptions == 'string' && cyoptions == 'get') ? ret : this };
    $.fn.CYTab = function (cyoptions) { CYTab($(this), cyoptions); return this; };
    $.fn.CYDatePicker = function (cyoptions, callbackselect) { CYDatePicker($(this), cyoptions, callbackselect); return this; };
    $.fn.CYFlipSwitch = function (cyoptions) { CYFlipSwitch($(this)); return this; };
    $.fn.CYCalendar = function (cyoptions) { CYCalendar($(this), cyoptions); return this; };
    $.fn.CYTree = function (cyoptions) { CYTree($(this), cyoptions); return this; };
    $.fn.CYEditor = function (cyoptions) { CYEditor($(this), cyoptions); return this; };
    $.fn.CYPanel = function (cyoptions) { CYPanel($(this), cyoptions); return this; };
    $.fn.CYAutoComplete = function (tags, cyoptions, callback) { CYAutoComplete($(this), tags, cyoptions, callback); return this; };
    $.fn.CYMenu = function (cyoptions) { CYMenu($(this), cyoptions); return this; };
    $.fn.CYBlink = function (cyoptions) { CYBlink($(this), cyoptions); return this; };

    // Conversion camelcase selon HTML5 - ne tient pas compte de la casse en entrée
    // "camel-case" -> "camelCase"
    // "-camel--case" -> "Camel-Case"
    // "--camel-case" -> "-CamelCase"
    if (typeof String.prototype.toCamel !== 'function') {
        String.prototype.toCamel = function () {
            var ret = this.toLowerCase();
            ret = ret.replace(/[-]([a-z])/g, function (s) { return s[1].toUpperCase(); });
            return (ret);
        };
    }
    // Comparaison de debut de chaine
    // si str = "fichier.txt" alors str.left("fic") => true
    // si str = "fichier.txt" alors str.left("FIC") => true
    // si str = "Fichier.TXT" alors str.left("fic") => true
    // Pour tenir compte de la casse, ajouter le flag str.left("fic", true)
    if (typeof String.prototype.left !== 'function') {
        String.prototype.left = function (compare, casse) {
            var lcompare = compare.length;
            var lstring = this.length;
            if (lcompare > lstring)
                return (false);
            if (casse == undefined || casse == false)
                return (this.substr(0, lcompare).toLowerCase() == compare.toLowerCase());
            else
                return (this.substr(0, lcompare) == compare);
        };
    }
    // Comparaison de fin de chaine
    // si str = "fichier.txt" alors str.right(".txt") => true
    // si str = "fichier.txt" alors str.right(".TXT") => true
    // si str = "Fichier.TXT" alors str.right(".txt") => true
    // Pour tenir compte de la casse, ajouter le flag str.right(".txt", true)
    if (typeof String.prototype.right !== 'function') {
        String.prototype.right = function (compare, casse) {
            var lcompare = compare.length;
            var lstring = this.length;
            if (lcompare > lstring)
                return (false);
            if (casse == undefined || casse == false)
                return (this.substr(lstring - lcompare).toLowerCase() == compare.toLowerCase());
            else
                return (this.substr(lstring - lcompare) == compare);
        };
    }
    // Replace All - usage evident .. ;)
    if (typeof String.prototype.replaceAll !== 'function') {
        String.prototype.replaceAll = function (search, replacement) {
            var target = this;
            return target.replace(new RegExp(search, 'g'), replacement);
        };
    }
    /* String hash
     *      var hash = chaine.hashCode()
     */
    if (typeof String.prototype.hashCode !== 'function') {
        String.prototype.hashCode = function () {
            var hash = 0,
                i, chr;
            if (this.length === 0)
                return hash;
            for (i = 0; i < this.length; i++) {
                chr = this.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash |= 0; // Converti en 32 bit
            }
            return hash;
        };
    }
    /*
     * Recherche avancée dans array. Retourne une rangée, un champ ou une chaine vide, mais jamais undefined
     *      tablo.findKey("chaine")
     *          retourne la rangée dont le champ "id" correspond à la chaine
     *      tablo.findKey("chaine", "", "autreid")
     *          retourne la rangée dont le champ "autreid" correspond à la chaine*
     *      tablo.findKey("chaine", "nom")
     *          retourne le contenu du champ "nom" dont le champ "id" correspond à la chaine
     *      tablo.findKey("chaine", "nom", "autreid")
     *          retourne le contenu du champ "nom" dont le champ "autreid" de la même rangée correspond à la chaine
     * Si rien n'est trouvé, dans tous les cas une chaine vide est retournée, ou defaultvalue si précisée.
     */
    if (typeof Array.prototype.findKey !== 'function') {
        Array.prototype.findKey = function (search, returnedfield, searchfield, defaultvalue) {
            if (searchfield === undefined || searchfield === '')
                searchfield = 'id';
            if (defaultvalue === undefined)
                defaultvalue = '';
            var ret = this.find(function (row) { return row[searchfield] == search; });
            if (ret === undefined)
                return defaultvalue;
            else if (returnedfield === undefined || returnedfield === '')
                return ret;
            else if (ret[returnedfield] !== undefined)
                return ret[returnedfield];
            else
                return defaultvalue;
        }
    }
    /* 
     * tablo.removeKey(value, searchfield)
     * Suppression dans un tableau de l'objet dont searchfield vaut value
     *     tablo = tablo.removeKey("valeur1", "champ");
     * Si searchfield est omis, il est supposé valoir "id"
     *     tablo = tablo.removeKey("valeur1");
     * équivaut à
     *     tablo = tablo.removeKey("valeur1", "id");
     */
    if (typeof Array.prototype.removeKey !== 'function') {
        Array.prototype.removeKey = function (value, searchfield) {
            if (searchfield == undefined)
                return this.filter(function (row) { return row.id != value; });
            else
                return this.filter(function (row) { return row[searchfield] != value; });
        }
    }
    // Defauts pour dialog
    $.ui.dialog.prototype.fullscreen = false;
    var oldcreateTitlebar = $.ui.dialog.prototype._createTitlebar;
    $.ui.dialog.prototype._createTitlebar = function () {
        oldcreateTitlebar.call(this);
        var oldHeight = this.options.height,
            oldWidth = this.options.width,
            oldPosition = this.options.position;

        /* A continuer pour iconifier un dialog
        this.uiDialogTitlebarIconify = $("<button type='button'></button>").button({
            label: "Iconify",
            icons: {
                primary: "ui-icon-caret-1-s"
            },
            text: false
        }).addClass("ui-dialog-titlebar-iconify").appendTo(this.uiDialogTitlebar); */

        this.uiDialogTitlebarFullScreen = $("<button type='button'></button>").button({
            label: "<i class='fa fa-window-restore'></i>",
            text: false
        }).addClass("ui-dialog-titlebar-fullscreen").appendTo(this.uiDialogTitlebar);

        this._on(this.uiDialogTitlebarFullScreen, {
            click: function (event) {
                event.preventDefault();
                if (this.fullscreen)
                    this._setOptions({ position: oldPosition, height: oldHeight, width: oldWidth });
                else {
                    var offset = $("#IDmain").offset();
                    var position = oldPosition;
                    var width = $("#IDmain").outerWidth() - 10;
                    var height = $("#IDmain").outerHeight() - 10;
                    position.of = "#IDmain";
                    position.collision = "flipfit";
                    this._setOptions({ position: position, height: height, width: width });
                }
                this.fullscreen = !this.fullscreen;
                this._position("center");
            }
        });

        /* A continuer pour iconifier un dialog
        this._on(this.uiDialogTitlebarIconify, {
            click: function(event) {
                event.preventDefault();
                console.log("CCCCCCCCCCCCCCCCCCCCCCCCCCCCClick" + this.getAttribute("class"));
                CYIconify({
                    object: this,
                    icontitle: "titre",
                    //icon: cyoptions.icon,
                    dblclick: function() {
                        //var zindex = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile") + 1;
                        //cellwindow.css("z-index", zindex);
                        console.log("2CCCCCCCCCCCCCCCCCCCCCCCCCCCCClick" + $(this).attr("class"));
                    }
                });
            }
        }); */
    };
    // Defaults for Inputmask
    // https://robinherbots.github.io/Inputmask/#/documentation
    if ($.isFunction($.fn.inputmask)) {
        // Default masking definitions (9: numeric, a: alphabetical, *: alphanumeric)
        // Predefined mask alias (datetime, numeric, cssunit, url, IP, email, mac, vin
        // Exemples :
        //   data-inputmask-regex="[1-8]{4}"
        //   data-inputmask-alias="email"
        //   data-inputmask="'alias': 'datetime'">
        //   data-inputmask-placeholder="jj/mm/aaaa" data-inputmask="'alias': 'datetime', 'inputformat': 'dd/mm/yyyy', 'min': '01/01/1917', 'max': '30/08/2016'"
        //   data-inputmask-placeholder="HH:MM:SS" data-inputmask="'alias': 'datetime', 'inputformat': 'HH:MM:ss'" 
        //   <div contenteditable="true" data-inputmask="'mask': '9', 'repeat': 10, 'greedy' : false"></div>\
        // Ici le [9] signifie 'chiffre optionnel' :
        //   <textarea data-inputmask="'mask': '99[9]-9999999'" data-inputmask-clearmaskonlostfocus="false"></textarea>
        // Supported symbols for datetime :
        //   d : Day of the month as digits; no leading zero for single-digit days.
        //   dd : Day of the month as digits; leading zero for single-digit days.
        //   ddd : Day of the week as a three-letter abbreviation.
        //   dddd : Day of the week as its full name.
        //   m : Month as digits; no leading zero for single-digit months.
        //   mm: Month as digits; leading zero for single-digit months.
        //   mmm : Month as a three-letter abbreviation.
        //   mmmm : Month as its full name.
        //   yy : Year as last two digits; leading zero for years less than 10.
        //   yyyy : Year as 4 digits.
        //   h : Hours; no leading zero for single-digit hours (12-hour clock).
        //   hh : Hours; leading zero for single-digit hours (12-hour clock).
        //   hx : Hours; no limit; x = number of digits ~ use as h2, h3, ... -H
        //   Hours; no leading zero for single-digit hours (24-hour clock).
        //   HH : Hours; leading zero for single-digit hours (24-hour clock).
        //   Hx : Hours; no limit; x = number of digits ~ use as H2, H3, ...
        //   M : Minutes; no leading zero for single-digit minutes. Uppercase M unlike CF timeFormat's m to avoid conflict with months.
        //   MM : Minutes; leading zero for single-digit minutes. Uppercase MM unlike CF timeFormat's mm to avoid conflict with months.
        //   s : Seconds; no leading zero for single-digit seconds.
        //   ss : Seconds; leading zero for single-digit seconds.
        //   l : Milliseconds. 3 digits.
        //   L : Milliseconds. 2 digits.
        //   t : Lowercase, single-character time marker string: a or p.
        //   tt : Two-character time marker string: am or pm.
        //   T : Single-character time marker string: A or P.
        //   TT : Two-character time marker string: AM or PM.
        //   Z : US timezone abbreviation, e.g. EST or MDT. With non-US timezones or in the Opera browser, the GMT/UTC offset is returned, e.g. GMT-0500
        //   o : GMT/UTC timezone offset, e.g. -0500 or +0230.
        //   S : The date's ordinal suffix (st, nd, rd, or th). Works well with d.
        Inputmask.extendAliases({
            "filename": {
                placeholder: "",
                regex: "^[áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒa-zA-Z0-9]+([- '_]{1}[áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒa-zA-Z0-9]+)*([.][a-zA-Z0-9]{1,}){0,1}$"
            },
            "name": {
                placeholder: "",
                regex: "^[a-zA-Z\u00C0-\u00FF]+([- ']{1}[a-zA-Z\u00C0-\u00FF]+)*[a-zA-Z\u00C0-\u00FF]*$"
            },
            "Name": {
                placeholder: "",
                regex: "^[A-Z\u00C0-\u00DE]{1}[a-z\u00E0-\u017F]*([- ']{1}[A-Z\u00C0-\u00DE][a-z\u00E0-\u017F]+)*[a-z\u00E0-\u017F]*$"
            },
            "login": {
                placeholder: "",
                regex: "^[a-zA-Z]+([-_]{1}[a-zA-Z0-9]+)*[a-zA-Z0-9]*$"
            }
        });
        $.fn.cyInputMask = function (obj) {
            cyEnv.Debug && console.log(cyEnv.LogPrefix + "cyInputMask tag='" + $(this).prop("tagName") + "' class='" + $(this).attr("class") + "' id='" + $(this).attr("id") + "'");
            // Formats regex maison prédéfinis
            // il suffit de définir regex: "file"
            let regFormats = {
                file: "^[áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒa-zA-Z0-9]+([- '_]{1}[áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒa-zA-Z0-9]+)*([.][a-zA-Z0-9]{1,}){0,1}$",
                name: "^[a-zA-Z\u00C0-\u00FF]+([- ']{1}[a-zA-Z\u00C0-\u00FF]+)*[a-zA-Z\u00C0-\u00FF]*$",
                Name: "^[A-Z\u00C0-\u00DE]{1}[a-z\u00E0-\u017F]*([- ']{1}[A-Z\u00C0-\u00DE][a-z\u00E0-\u017F]+)*[a-z\u00E0-\u017F]*$",
                login: "^[A-Z\u00C0-\u00DE]{1}[a-z\u00E0-\u017F]*([- ']{1}[A-Z\u00C0-\u00DE][a-z\u00E0-\u017F]+)*[a-z\u00E0-\u017F]*$"
            };
            if (obj != undefined && obj.regex != undefined && regFormats[obj.regex] != undefined)
                obj.regex = regFormats[obj.mask];
            obj = Object.assign({
                clearIncomplete: true,
                autoUnmask: true,
                oncomplete: function (e) {
                    cyEnv.Debug && console.log(cyEnv.LogPrefix + "cyInputMask oncomplete [" + $(this).val() + "]");
                    // data-autonext="NextIdInput"
                    var next = $(this).data('autonext');
                    if (next != undefined)
                        $("#" + next).focus();
                },
                onincomplete: function (e) {
                    cyEnv.Debug && console.log(cyEnv.LogPrefix + "cyInputMask onincomplete [" + $(this).val() + "]");
                },
                onUnMask: function (maskedValue, unmaskedValue) {
                    cyEnv.Debug && console.log(cyEnv.LogPrefix + "cyInputMask onUnmask '" + maskedValue + "' '" + unmaskedValue + "'");
                    if (this.opts.alias == 'email')
                        return (unmaskedValue == "" ? "" : maskedValue);
                    else
                        return (unmaskedValue);
                }
            }, obj);
            cyEnv.Debug && console.log(cyEnv.LogPrefix + "cyInputMask '" + JSON.stringify(obj));
            $(this).inputmask(obj);
            return this;
        };
    }
    // Défauts pour dropzone
    Dropzone.prototype.defaultOptions.dictDefaultMessage = CYTranslateString("UPLOAD HERE");
    Dropzone.prototype.defaultOptions.dictFallbackMessage = CYTranslateString("BAD BROWSER");
    Dropzone.prototype.defaultOptions.dictFallbackText = '';
    Dropzone.prototype.defaultOptions.dictInvalidFileType = CYTranslateString("BAD FILETYPE");
    Dropzone.prototype.defaultOptions.dictResponseError = CYTranslateString("ERROR {{statusCode}}");
    Dropzone.prototype.defaultOptions.dictCancelUpload = CYTranslateString("Annuler");
    Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = CYTranslateString("CANCEL");
    Dropzone.prototype.defaultOptions.dictRemoveFile = CYTranslateString("DELETE");
    Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = CYTranslateString("MAX FILES REACHED");
    Dropzone.prototype.defaultOptions.dictFileTooBig = CYTranslateString("FILE IS TOO BIG ({{filesize}}MiB). MAX FILESIZE: {{maxFilesize}}MiB.");
    Dropzone.prototype.defaultOptions.thumbnailWidth = 120; // px
    Dropzone.prototype.defaultOptions.thumbnailHeight = 120; // px
    Dropzone.prototype.defaultOptions.addRemoveLinks = true;
    Dropzone.prototype.defaultOptions.maxFilesize = 1; // Mb
    // Défauts pour les DataTable
    $.extend(true, $.fn.dataTable.defaults, {
        lengthMenu: [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, CYTranslateString('ALL')]
        ],
        order: [
            [0, 'asc']
        ],
        pageLength: 10,
        paging: true,
        pagingType: "full_numbers",
        info: true,
        ordering: true,
        searching: true,
        search: { "regex": true, "smart": false },
        autoWidth: false,
        scrollX: false,
        scrollCollapse: false,
        language: {
            "url": cyPath + "/CYJS/js/lang/" + cyLang + ".json"
        },
        // rowCallback appelé pour chaque ligne à chaque pagination
        // row : { "_DT_RowIndex" : 36 }
        // data : { les données de la ligne }
        //rowCallback: function(row, data) {
        //    debug && console.log("CYJS : dataTable rowCallback");
        //    if ($(row).hasClass("myselection"))
        //        $(row).addClass('selected');
        //},
        //
        // drawCallback appelé à chaque pagination mais après les rowCallback
        // settings.oInit contient toutes les options spécifiéees à la création,
        // certaines étant renommées, comme lengthMenu -> settings.oInit.aLengthMenu.
        // On y retrouvera settings.oInit.pageLength, settings.oInit.searching, 
        // settings.oInit.selectcolumns ...
        // Et this désigne l'objet
        //drawCallback: function(settings) {
        //    debug && console.log("CYJS : dataTable drawCallback");
        //    var thistablediv = this;
        //    var id = "#" + $(this).attr('id');
        // Pour rafraîchir d'éventuels tooltips après pagination :
        //    this.CYTooltip();
        //},
        //
        // createdRow est appelé àà chaque création de ligne
        //createdRow: function(row, data, dataIndex) {
        //    $(row).addClass('my-class');
        //}
        // Pour intercepter la 'affichage ou non d'une colonne via data-selectcolumns, on peut intercepter :
        //    table.on('column-visibility.dt', function(e, settings, column, state) {
        //        console.log('Column ' + column + ' is now ' + (state ? 'visible' : 'hidden'));
        // où table = $("#id").DataTable()
        //});
        // Pour resetter le filtre principal :
        //     $('#table').DataTable().search('').draw();
        // et effacer le input :
        //     $('#table').prev('.dataTables_filter').find('input').val('');
        // Pour resetter les filtres de colonnes :
        //     $('#table').DataTable().search('').columns().search('').draw();
        // et effacer les input et select :
        //     $('#table' + ' thead>tr>th input,'#table' + ' thead>tr>th select').val('');
        initComplete: function (settings) {
            var thistablediv = this;
            var api = this.api();
            var id = "#" + $(this).attr('id');
            debug && console.log("CYJS : CYInit DataTable initComplete " + id);
            // Selecteur de columns auto si attribut data-selectcolumns='1' present
            // Exemple :
            // var columns = [
            // {
            //    width: "20%",
            //    data: "Societe",
            //    searchable: true, - false pour invalider les recherches ds cette colonne
            //    visible: true - false pour la cacher par defaut
            // }];
            // Les classes ClassDataTableColumns et ClassDataTableColumnsToggle st gerees automatiquement
            var i = 0;
            $(id + "[data-selectcolumns]").each(function () {
                if ($(this).attr("data-selectcolumns") == '1' || $(this).attr("data-selectcolumns") == 'true') {
                    var div = '<div class="ClassDataTableColumns">';
                    api.columns().header().each(function (column) {
                        div += (i > 0 ? '&nbsp;-&nbsp;' : '') + '<a class="ClassDataTableColumnsToggle" data-column="' + i + '" style="opacity:' + ($(column).index() == -1 ? '0.4' : '1') + '">' + $(column).text() + '</a>';
                        i++;
                    });
                    div += '</div>';
                    $(id + "_wrapper").prepend(div);
                    $(id + "_wrapper > .ClassDataTableColumns > .ClassDataTableColumnsToggle").on('click', function (e) {
                        e.preventDefault();
                        // Numero de colonne cliquee
                        var column = $(id).DataTable().column($(this).attr('data-column'));
                        // Toggle visibilite
                        column.visible(!column.visible());
                        if (!column.visible())
                            $(this).css("opacity", "0.4");
                        else
                            $(this).css("opacity", "1");
                    });
                }
            });
            // Applique champs de recherche si besoin (attribut data-filter='1' dans th)
            $(id + " th[data-filter]").each(function () {
                if ($(this).attr("data-filter") == '1' || $(this).attr("data-filter") == 'true') {
                    var title = $(this).text().trim();
                    $(this).html('<label>' + title + '</label>');
                    var inputfield = '<input class="ClassDataTableFilter" data-filter="1" onclick="event.stopPropagation()" type="search" placeholder="' + CYTranslateString('FILTER') + ' ' + title + '" />';
                    $(this).append(inputfield);
                }
            });
            $(id + " input[data-filter]").on('keyup change', function () {
                if ($(this).attr("data-filter") == '1' || $(this).attr("data-filter") == 'true') {
                    let values = CYExplode("|", this.value);
                    $(id).DataTable().column($(this).parent().index() + ':visible').search(this.value, true, false).draw();
                    // Efface un eventuel filtre automatique
                    $(this).parent("th").find("select.ClassDataTableAutoSelect").val('');
                }
            });
            // Select automatique si attribut data-selectauto='1' present
            $(id + "[data-selectauto]").each(function () {
                if ($(this).attr("data-selectauto") == '1' || $(this).attr("data-selectauto") == 'true') {
                    api.columns().every(function (colonne) {
                        var column = this;
                        // Si date-select='true' sur un th j'affiche le selectauto (ou est absent - compatibilité),
                        // S'il vaut false, pas de selectauto sur cette colonne
                        if (column.header().attributes['data-select'] != undefined && (column.header().attributes['data-select'].value == '0' || column.header().attributes['data-select'].value == 'false'))
                            return;
                        // Ajoute le select
                        var select = $('<select onclick="event.stopPropagation()" class="ClassDataTableAutoSelect"><option value="">' + CYTranslateString('ALL') + '</option></select>').appendTo($(column.header())).on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());
                            column.search(val ? '^' + val + '$' : '', true, false).draw();
                            // Efface un eventuel filtre
                            $(this).parent("th").find("input.ClassDataTableFilter").val('');
                        });
                        // Récupère le type de tri défini
                        let sType = column.settings()[0].aoColumns[column.index()].sType;
                        // Constitue les options de chaque select
                        column.data().unique().sort(function (a, b) {
                            switch (sType) {
                                case 'date-fr':
                                    return moment(a, "DD/MM/YY") < moment(b, "DD/MM/YY") ? -1 : 1;
                                case 'numeric':
                                case 'int':
                                case 'float':
                                case 'double':
                                case 'bool':
                                    return (a * 1) > (b * 1) ? -1 : 1;
                                default:
                                    return a < b ? -1 : 1;
                            }
                        }).each(function (d, j) {
                            // Plante si un @ dans d !!!
                            //var d1 = ''; // Il peut y avoir du code, un <a> par exemple
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });
            // Si changement de page je purge les sélections courantes AVANT de changer de page
            // Nécessaire pour éditer une ligne existante car les classes "selected" sont conservées
            // d'une page à l'autre
            $(id).on('page.dt', function (event) {
                // Appui sur CTRL pour selection multiple à étudier
                //if (!event.ctrlKey)
                $(id + ' tbody > tr').removeClass('selected');
            });
            // Memo :
            //var table = $(id).DataTable();
            //var info = table.page.info();
            // console.log('Showing page: '+info.page+' of '+info.pages);
            // Affiche..
            $(this).show();
            //$("thead").next().hide();
        }
    });
    // Valeurs sType de tri de columns pour les datatables
    //        sType: "date-fr" pour trier les date jj/mm/aaaa ou jj/mm/aa
    // Rappel : on peut utiliser en standard type: "numeric" ou "string"
    $.extend($.fn.dataTableExt.oSort, {
        "date-fr-pre": function (a) {
            var d = a.split('/');
            // Retourne dans tous les cas AAAAMMJJ en numérique
            // Si l'année est sur 2 digits je préfixe le siècle
            if (d[2] < 100)
                return ('20' + d[2] + d[1] + d[0]) * 1;
            else
                return (d[2] + d[1] + d[0]) * 1;
        },
        "date-fr-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "date-fr-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });
    /*
     * This plugin jumps to the right page of the DataTable to show the required row
     * Version 1.0 from http://www.edouardlabre.com)
     * Returns DataTables Rows API instance
     * Example :
     *    var table = $('#example').DataTable();
     *    var new_row = {
     *      DT_RowId: 'row_example',
     *      name: 'example',
     *      value: 'an example row'
     *    };
     *    table.row.add( new_row ).draw().show().draw(false);
     */
    $.fn.dataTable.Api.register('row().show()', function () {
        var page_info = this.table().page.info();
        var new_row_index = this.index();
        var row_position = this.table().rows({ search: 'applied' })[0].indexOf(new_row_index);
        // Already on right page ?
        if ((row_position >= page_info.start && row_position < page_info.end) || row_position < 0)
            // Return row object
            return this;
        // Find page number
        var page_to_display = Math.floor(row_position / this.table().page.len());
        // Go to that page
        this.table().page(page_to_display);
        // Return row object
        return this;
    });
    // Loader automatique
    if ($('#IDloader').length == 0) {
        $('body').prepend('<div id="IDloader"></div>');
    }
    // Scroll top automatique sur 'page-content'
    if ($('#IDscrolltop').length == 0) {
        $('body').append('<div onclick="window.location.href=' + "'#IDpage'" + '" id="IDscrolltop" class="ui-button" style="display: none"><i class="fa fa-arrow-up fa-lg"></div>');
        $('#IDscrolltop').click(function () {
            $('page-content').animate({ scrollTop: 0 }, 1000);
        });
    }
    $('page-content').scroll(function () {
        if ($('page-content').scrollTop() == 0) {
            $('#IDscrolltop').fadeOut("slow", function () {
                $(this).css("z-index", 0);
            });
        } else
            $('#IDscrolltop').css("z-index", CYMaxZindex() + 1).fadeIn("slow");
    });
    if (cyKeepAlive)
        CYCheckNetwork(cyKeepAlive);
    // Local cache
    if (cyCache) {
        cyDB = localforage.createInstance({ name: cyProject.replace(/ /g, "") });
        cyDB.clear();
    }
    // Clic footer
    $("#IDfooter").on("click", function () {
        $(".ClassMenu,.ClassContextMenu").hide();
    });
    // Chargement CSS ET JS
    CYLoadCSS(cyPath + '/CYJS/css/style.css', function () {
        CYLoadCSS(cyPath + '/css/custom.css', function () {
            cySideMenuWidth = cyInitialSideMenuWidth = $("#IDsidemenu").css('width');
            // Gestion du toggle menu
            var timerIDheadericonmenu = 0;
            $("#IDheadericonmenu").click(function (event) {
                cyUserClickedIconMenu = true;
                $("usermenu").hide("slide", {
                    direction: "right"
                }, 500);
                // Sortie si double-clic
                // if(event.originalEvent.detail > 1)
                //    return;
                // Gestion du double-clic
                var d = new Date();
                var t = d.getTime();
                var dblclic = false;
                if (t - timerIDheadericonmenu <= cyDoubleClicTimeout) {
                    dblclic = true;
                    timerIDheadericonmenu = 0;
                } else
                    timerIDheadericonmenu = t;
                if (!dblclic) {
                    if ($("#IDmain").css('left') == cySideMenuWidth)
                        CYOpenSideMenu(true, false, true, cySideMenuSmallWidth);
                    else if (parseInt($("#IDsidemenu").css('width')) > 0)
                        CYOpenSideMenu(false, false, true);
                    else
                        CYOpenSideMenu(true, true, true);
                } else {
                    cySideMenuWidth = cyInitialSideMenuWidth;
                    CYOpenSideMenu(true, false, true);
                }
            });
            // Sauve width pour le resize qui suit
            cyCurrentWidth = $(window).width();
            // Ecran trop petit - démarrage
            if (cySideMenuClosed || cyCurrentWidth < cyMinWindowWidth)
                CYOpenSideMenu(false, false, false);
            // IDmain doit toujours contenir la totalité applicative
            // Dimensionnement dynamique window
            if (cySideMenuAuto) {
                $(window).resize(function () {
                    // IOS/Safari genere des resize n'importe comment ..:(
                    if ($(window).width() != cyCurrentWidth) {
                        cyCurrentWidth = $(window).width();
                        $("usermenu").hide("slide", {
                            direction: "right"
                        }, 500);
                        if (!cyUserClickedIconMenu) {
                            if (cyCurrentWidth < cyMinWindowWidth)
                                CYOpenSideMenu(false, false, false);
                            else
                                CYOpenSideMenu(true, false, false);
                        }
                    }
                    if (typeof (window["CYResizeCustom"]) == 'function')
                        CYResizeCustom();
                });
            }
            // Gestion du toggle user
            $("#IDheadericonuser").click(function () {
                // Supprime tout tooltip
                $('.ui-tooltip').fadeOut(100, function () { $(this).remove() });
                $("usermenu").toggle("slide", {
                    direction: "right"
                }, 500);
                // Ferme sidemain si besoin
                if (!parseInt($("#IDmain").css('left')))
                    CYOpenSideMenu(false, true, true);
            });
            // Clic sur main
            $("main").click(function () {
                // Ferme usermenu
                $("usermenu").hide("slide", {
                    direction: "right"
                }, 500);
                // Ferme sidemain si besoin
                if (!parseInt($("#IDmain").css('left')))
                    CYOpenSideMenu(false, true, true);
            });
            CYLoadCSS(cyPath + '/CYJS/css/jstree.css');
            CYLoadCSS(cyPath + '/CYJS/css/jquery.dataTables.css');
            CYLoadCSS(cyPath + '/CYJS/css/datepicker.css',
                // Fichier de langue pour datepicker
                CYLoadJS(cyPath + '/CYJS/js/datepicker/lang/' + cyLang + '.js'));
            CYLoadCSS(cyPath + '/CYJS/css/fullcalendar.css', function () {
                // Fichier de langue pour fullcalendar
                CYLoadJS(cyPath + '/CYJS/js/fullcalendar/lang/' + cyLang + '.js');
                CYLoadCSS(cyPath + '/CYJS/css/scheduler.css');
            });
            CYLoadCSS(cyPath + '/CYJS/css/trumbowyg.css',
                // Fichier de langue pour trumbowyg editor
                CYLoadJS(cyPath + '/CYJS/js/trumbowyg/langs/' + cyLang + '.min.js'));
            // J'insère dynamiquement le script contenant les strings de langue dans js/lang/'lang'.js
            // Il devra contenir un tableau associatif cyString['chaine'] ou 'chaine' devra etre spécifié
            // dans le fichier HTML dans un tag <l>chaine</l> ou dans les label, opton, th, title ..
            // callback necessaire pour continuer, besoin de la langue pour les messages cyString ..;)
            CYLoadJS(cyPath + '/js/lang/' + cyLang + '.js', callback);
        });
    });
}
/*
 * Cache interne localforage. Peut etre désactivé si cyCache = false
 *     CYDB({ cat: "html", command: "set", key: 'pagename', data: datahtml, callback: function () { ... } });
 *     CYDB({ cat: "html", command: "get", key: 'pagename', callback: function () { ... } });
 *     CYDB({ cat:"html", command: "remove", key: 'pagename', callback: function () { ... } });
 *     CYDB({ cat: "html", command: "keys", callback: function () { ... } }) ou CYDB({ command: callback: function () { ... } })
 *     CYDB({ command: "clear", function () { ... } });
 */
function CYDB(cyoptions) {
    if (!cyCache)
        return;
    debug && console.log("CYJS : CYDB command " + cyoptions.command + " cat " + cyoptions.cat + " key " + cyoptions.key);
    (debug & DEBUG_MORE) && cyoptions.data != undefined && console.log("CYJS : CYDB " + cyoptions.data);
    cyoptions.key = cyoptions.cat + '-' + cyoptions.key;
    switch (cyoptions.command) {
        case 'set':
            cyDB.setItem(cyoptions.key, cyoptions.data, function (err, value) {
                if (typeof (cyoptions.callback) == "function")
                    cyoptions.callback(value);
            });
            break;
        case 'get':
            cyDB.getItem(cyoptions.key, function (err, value) {
                if (typeof (cyoptions.callback) == "function")
                    cyoptions.callback(value);
            });
            break;
        case 'remove':
            cyDB.removeItem(cyoptions.key, function (err, value) {
                if (typeof (cyoptions.callback) == "function")
                    cyoptions.callback(value);
            });
            break;
        case 'keys':
            cyDB.keys(function (err, values) {
                var goodvalues = [];

                values.forEach(function (item, index, object) {
                    if (!cyoptions.cat.length || item.substring(0, cyoptions.cat.length) == cyoptions.cat)
                        goodvalues.push(item);
                });
                if (typeof (cyoptions.callback) == "function")
                    cyoptions.callback(goodvalues);
            });
            break;
        case 'clear':
            cyDB.clear(function () {
                if (typeof (cyoptions.callback) == "function")
                    cyoptions.callback();
            });
    }
}
/*
 * Détection réseau et sync si besoin
 */
function CYCheckNetwork(msec) {
    var url = cyPath + '/CYJS/js/cyjscheck.php';

    setInterval(function () {
        var d = new Date();
        $.ajax({
            data: d.getTime(),
            type: "GET",
            url: url,
            datatype: "text",
            success: function (msg) {
                if (cyCheckNetwork)
                    return;
                CYAlert({ message: "ONLINE", autofade: 2000 });
                $("#IDheadericonnetwork").fadeTo('2000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1);
                if (cySync) {
                    CYDB({
                        cat: "sync",
                        command: "keys",
                        callback: function (keys) {
                            debug && console.log('CYJS : CYCheckNetwork sync ' + keys);
                            if (typeof keys == 'object' && keys.length) {
                                CYAlert({ message: "SYNC", autofade: 2000 });
                                // Une clé est "sync-99999999999-http://...&BASE64"
                                // BASE64 permettant de distinguer l'appel à un
                                // même webservice mais avec des data POST différentes
                                keys.forEach(function (longkey) {
                                    // Saute "sync-"
                                    var shortkey = longkey.substring(5);
                                    CYDB({
                                        cat: "sync",
                                        command: "get",
                                        key: shortkey,
                                        callback: function (data) {
                                            // Saute "99999999999-"
                                            var kurl = shortkey.substring(shortkey.indexOf("-") + 1);
                                            // Saute "&BASE64..." en fin de chaine
                                            kurl = kurl.replace(/&([^&]*)$/, "");
                                            debug && console.log('CYJS : CYCheckNetwork post ' + kurl + ' data="' + CYImplode(',', data) + '"');
                                            $.post(kurl, data, function (ret, status) {
                                                debug && console.log('CYJS : CYCheckNetwork post ret ok');
                                                CYDB({
                                                    cat: "sync",
                                                    command: "remove",
                                                    key: shortkey
                                                });
                                            }).fail(function (xhr, status, error) {
                                                debug && console.log('CYJS : CYCheckNetwork post ' + kurl + ' failed, xhr=' + JSON.stringify(xhr) + ', status=' + status + ', error=' + error);
                                            });
                                        }
                                    });
                                });
                            }
                        }
                    });
                }
                cyCheckNetwork = true;
            },
        }).fail(function (xhr, status, error) {
            debug && console.log('CYJS : CYCheckNetwork ' + url + ' failed, xhr=' + JSON.stringify(xhr) + ', status=' + status + ', error=' + error);
            if (cyCheckNetwork) {
                CYAlert({ message: "OFFLINE", autofade: 2000 });
                $("#IDheadericonnetwork").fadeTo('2000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3).fadeTo('1000', 1).fadeTo('1000', 0.3);
            }
            cyCheckNetwork = false;
        });
    }, msec);
}
/*
 * Chargement dynamique d'un script JS
 * callback peut être une fonction ou un nom de fonction
 * callbackfail appelé en cas d'échec peut être une fonction ou un nom de fonction
 */
function CYLoadJS(js, callback, callbackfail) {
    debug && console.log('CYJS : CYLoadJS loading script ' + js);
    var success = function (datajs, status) {
        debug && console.log('CYJS : CYLoadJS script loaded ' + js);
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.appendChild(document.createTextNode(datajs));
        var head = document.head || document.getElementsByTagName("head")[0];
        head.appendChild(s);
        if (callback != undefined && callback != '') {
            debug && console.log('CYJS : CYLoadJS callback');
            if (typeof (window[callback]) == 'function')
                window[callback](js);
            else
                callback(js);
        }
        return (0);
    };
    $.ajax({
        url: js,
        type: "POST",
        datatype: "script",
        success: function (datajs, status) {
            success(datajs, status);
        }
    }).fail(function () {
        debug && console.log("CYJS : CYLoadJS failed");
        if (callbackfail != undefined && callbackfail != '') {
            debug && console.log('CYJS : CYLoadJS callbackfail');
            if (typeof (window[callbackfail]) == 'function')
                window[callbackfail](js);
            else
                callbackfail(js);
        }
    });
}
/*
 * Chargement dynamique d'une feuille de style CSS
 * callback peut être une fonction ou un nom de fonction
 * callbackfail appelé en cas d'échec peut être une fonction ou un nom de fonction
 */
function CYLoadCSS(css, callback, callbackfail) {
    debug && console.log('CYJS : CYLoadCSS loading css ' + css);
    var success = function (datacss, status) {
        debug && console.log('CYJS : CYLoadCSS style loaded ' + css);
        // CSS pattern
        for (var c in cyCSS)
            datacss = datacss.replaceAll("%" + c + "%", cyCSS[c]);
        var s = document.createElement("style");
        s.type = "text/css";
        s.appendChild(document.createTextNode(datacss));
        var head = document.head || document.getElementsByTagName("head")[0];
        head.appendChild(s);
        if (callback != undefined && callback != '') {
            debug && console.log('CYJS : CYLoadCSS callback');
            if (typeof (window[callback]) == 'function')
                window[callback](css);
            else
                callback(css);
        }
    };
    $.ajax({
        url: css,
        type: "POST",
        datatype: "text",
        success: function (datacss, status) {
            success(datacss, status);
        }
    }).fail(function () {
        debug && console.log("CYJS : CYLoadCSS failed");
        if (callbackfail != undefined && callbackfail != '') {
            debug && console.log('CYJS : CYLoadCSS callbackfail');
            if (typeof (window[callbackfail]) == 'function')
                window[callbackfail](css);
            else
                callbackfail(css);
        }
    });
}
/*
 * Si page ne commence pas par '/', charge html/page.html (suffixe .html ajouté)
 * Sinon c'est supposé être le chemin intégral.
 * Le contenu retourné sera mis dans l'idToSet si ce dernier est précisé et sera
 * automatiquement traduit.
 * Si page ne commence pas par '/', appelle ensuite le script js/page.js (si pas déjà chargé)
 * et l'insère en tant que script. Il DOIT comporter une fonction CyInit_page (même vide) qui
 * sera automatiquement appelée. Appelle ensuite la feuille de style dans css/page.css (où page
 * est le nom du script) qui sera immediatement insérée.
 * callback si présent sera appelée dans tous les cas avec le contenu de la page.
 * Si pas de idToSet ni de callback, idToSet = "#IDmain"
 * V2 : cache automatique via CYDB(). Si idToSet n'existe pas dans ce cas, la page + JS + CSS est préchargée
 * 16/07/20 : un obj permet de tout définir :
 *   {
 *       page : page,
 *       idToSet: "IDmain", (defaut)
 *       callback: callback (ou rien)
 *       args: obj (objet à transmettre à la fonction CyInit_page(obj))
 *   }
 */
function CYLoadPage(obj, idToSet, callback) {
    let page = '';
    let initargs = {};
    let autoLoader = false;

    if (typeof obj == "object") {
        if (obj.page != undefined)
            page = obj.page;
        if (obj.idToSet != undefined)
            idToSet = obj.idToSet;
        if (obj.callback != undefined)
            callback = obj.callback;
        if (obj.args != undefined)
            initargs = obj.args;
        if (obj.autoLoader != undefined)
            autoLoader = obj.autoLoader;
    } else
        page = obj;
    if (idToSet == undefined && callback == undefined)
        var idToSet = "#IDmain";
    if (idToSet != undefined && idToSet != '' && idToSet.indexOf('#') != 0)
        idToSet = "#" + idToSet;
    debug && console.log('CYJS : CYLoadPage ' + page + ' ' + idToSet);
    // Gestion auto page loader sur IDmain
    if (autoLoader && idToSet == '#IDmain')
        CYLoader(true);
    if (page.substr(0, 1) != "/")
        var q = cyServerName + cyPath + '/html/' + page + '.html';
    else
        var q = cyServerName + page;
    var success = function (datahtml) {
        debug && console.log('CYJS : CYLoadPage ' + page + ' sent to ' + idToSet);
        if (idToSet != undefined && idToSet != '') {
            if (idToSet == '#IDmain') {
                // Purge de l'objet - s'il y a des dialog jquery-ui, j'invoque leur méthode close pour
                // terminer proprement, l'user a peut-être mis une closeFunction pour ses besoins
                // rattachés au parent, je les vire
                // Remarque : s'il y a un form dans un dialog, le destroy va le replacer dans IDmain ;
                // le empty() qui suit permettra de le virer
                $('body').find(".ui-dialog").each(function () {
                    // Set by jquery-ui
                    let id = '#' + $(this).attr('aria-describedby');
                    $(id).dialog('close');
                });
            }
            $(idToSet).empty();
            // Le contenu de la page html est envoyé dans idToSet
            $(idToSet).html(datahtml);
            // Clones éventuels
            CYCloneHTML(idToSet);
            CYTranslate(idToSet);
        }
        // Le script js DOIT contenir une fonction CyInit_nomduscriptsanssuffixe()
        var functionName = "CyInit_" + page;
        // Le script est deja chargé ?
        if (typeof (window[functionName]) == 'function') {
            debug && console.log('CYJS : CYLoadPage ' + page + ' script ' + page + '.js already loaded');
            // Execute la fonction d'init du script js
            debug && console.log('CYJS : CYLoadPage ' + page + ' call to ' + functionName + '()');
            window[functionName](initargs);
        } else if (page.substr(0, 1) != "/") {
            CYLoadCSS(cyPath + '/css/' + page + '.css', function () {
                CYLoadJS(cyPath + '/js/' + page + '.js', function () {
                    window[functionName](initargs);
                });
                // Si le CSS n'existe pas, j'appelle qd même le JS
            }, function () {
                CYLoadJS(cyPath + '/js/' + page + '.js', function () {
                    window[functionName](initargs);
                });
            });
        }
        // Sortable panels
        $("#IDmain").CYSortable();
        // Gestion auto page loader
        if (autoLoader && idToSet == '#IDmain')
            CYLoader(false);
        cyCurrentPage = page;
        // Callback si besoin
        if (callback != undefined && callback != '') {
            debug && console.log('CYJS : CYLoadPage callback');
            if (typeof (window[callback]) == 'function')
                window[callback](datahtml);
            else
                callback(datahtml);
        }
    };
    $.ajax({
        url: q,
        type: "POST",
        datatype: "text",
        success: function (datahtml, status) {
            debug && console.log('CYJS : CYLoadPage ' + page + ' html loaded ' + q);
            cyCurrentPage = page;
            // Mise en cache
            CYDB({
                cat: "html",
                command: "set",
                key: page,
                data: datahtml
            });
            if ($(idToSet).length || typeof (callback) == 'function')
                success(datahtml);
            else if (page.substr(0, 1) != "/") {
                CYLoadCSS(cyPath + '/css/' + page + '.css', function () {
                    CYLoadJS(cyPath + '/js/' + page + '.js');
                    // Si le CSS n'existe pas, j'appelle qd même le JS
                }, function () {
                    CYLoadJS(cyPath + '/js/' + page + '.js');
                });
            }
        }
    }).fail(function () {
        // Extrait du cache
        CYDB({
            cat: "html",
            command: "get",
            key: page,
            callback: function (value) {
                if (value != null)
                    success(value);
                else {
                    CYAlert('ERROR LOADING' + ' ' + q);
                    if (autoLoader)
                        CYLoader(false);
                }
            }
        });
    });
}
/*
 * Refresh page courante
 */
function CYReloadPage() {
    CYLoadPage(cyCurrentPage);
}
/*
 * Appel webservice
 * url peut etre un objet (qui dans ce cas prime sur les autres args) :
 *     {
 *         url: "http:....",
 *         sync: false, // force non sync even if cySync is true
 *         cache: false, // force non cache use even if cyCache is true
 *         data: { .. }, // optionnal JSON data
 *         callbackok: function () { ... },
 *         callbackfail: function () { ... }
 *         type: "POST" (defaut) ou "GET"
 *         dataType : "none" (defaut) ou xml/html/script/json/arraybuffer
 *     }
 * si callbackok présent, appel si ok
 * si callbackfail présent, il sera appelé si le webservice plante et si rien dans le cache
 */
function CYExec(url, callbackok, callbackfail) {
    var sync = cySync;
    var cache = cyCache;
    var type = "POST";
    var data = {};
    var dataType = "";
    var processData = true;
    var contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    var now = Date.now() % 200000;

    if (typeof url == "object") {
        if (url.callbackok != undefined)
            callbackok = url.callbackok;
        if (url.callbackfail != undefined)
            callbackfail = url.callbackfail;
        if (url.sync != undefined)
            sync = url.sync;
        if (url.cache != undefined)
            cache = url.cache;
        if (typeof url.data == "object")
            data = url.data;
        if (url.type != undefined)
            type = url.type;
        if (url.dataType != undefined)
            dataType = url.dataType;
        if (url.processData != undefined)
            processData = url.processData;
        if (url.contentType != undefined)
            contentType = url.contentType;
        url = url.url;
    }
    debug && console.log('CYJS : CYExec ' + now + ' ' + url + ' cache ' + cache + ' sync ' + sync + ' dataType "' + dataType + '" data ' + JSON.stringify(data));
    return $.ajax({
        url: url,
        data: data,
        type: type,
        dataType: dataType,
        processData: processData,
        contentType: contentType,
        success: function (ret) {
            debug && console.log('CYJS : CYExec ' + now + ' success');
            // Mise en cache
            if (cache) {
                CYDB({
                    cat: "exec",
                    command: "set",
                    key: url + "&" + CYBtoa(JSON.stringify(data)),
                    data: ret
                });
            }
            if (callbackok != undefined)
                callbackok(ret);
        },
        error: function (xhr, status, error) {
            debug && console.log('CYJS : CYExec ' + now + ' ' + url + ' failed, status=' + status + ', error=' + error + ', xhr=' + JSON.stringify(xhr));
            // Extrait du cache
            if (cache) {
                CYDB({
                    cat: "exec",
                    command: "get",
                    // key = url + "&" + BASE64(data object)
                    key: url + "&" + CYBtoa(JSON.stringify(data)),
                    callback: function (value) {
                        // Rien ds le cache
                        if (value != null) {
                            if (callbackok != undefined)
                                callbackok(value);
                        }
                        // Pas de réseau, pas de synchro voulue
                        else if (cyCheckNetwork || !sync) {
                            if (callbackfail != undefined)
                                callbackfail(xhr, status, error, url);
                        }
                        // Synchro
                        else if (sync) {
                            CYConfirm({ message: "NO NETWORK - SYNC" }, function () {
                                CYDB({
                                    cat: "sync",
                                    command: "set",
                                    key: CYUniqueId() + '-' + url + "&" + CYBtoa(JSON.stringify(data)),
                                    data: data
                                });
                            });
                        }
                    }
                });
            } else if (callbackfail != undefined)
                callbackfail(xhr, status, error, url);
        }
    });
}
/*
 * Appel historique CYExec mais data en object JSON plutot qu'en url
 */
function CYExecData(url, data, callbackok, callbackfail) {
    debug && console.log('CYJS : CYExecData ' + url + ' data="' + CYImplode(',', data) + '"');
    return CYExec({
        url: url,
        data: data,
        callbackok: callbackok,
        callbackfail: callbackfail
    });
}
/*
 * Appelle le lien fourni.
 * - via un tag <a> dynamique si pas de données POST à transmettre via cyoptions.data
 *     CYURL(webservice, { download: filename, newtab: true });
 * - via un <form> dynamique si données POST à transmettre via cyoptions.data
 *     CYURL(webservice, { newtab: false, data: { r: "request", args: jsonobject } });
 */
function CYURL(url, cyoptions) {
    debug && console.log('CYJS : CYURL ' + url + " cyoptions {" + JSON.stringify(cyoptions) + '}');

    if (typeof cyoptions == "object" && cyoptions.data != undefined) {
        var $form = $("<form method='POST' action='" + url + "'></form>");
        Object.keys(cyoptions.data).forEach(function (k) {
            //console.log("keys="+k+"="+ JSON.stringify(cyoptions.data[k]));
            $form.append(CYTag("input", { name: k, type: 'hidden' }));
            $form.find('input[name="' + k + '"]').val(cyoptions.data[k]);
        });
        // Pour ouvrir dans un onglet separe
        if (cyoptions.newtab == true || cyoptions.newtab == "true")
            $form.attr("target", "_blank");
        // Pour telecharger un fichier
        if (cyoptions.download != undefined && cyoptions.download != '')
            $form.append(CYTag("input", { name: "download", type: 'hidden', value: cyoptions.download }));
        $form.appendTo("body").submit();
        $form.remove();
    } else {
        var a = document.createElement("a");
        a.href = url;
        if (typeof cyoptions == "object") {
            // Pour ouvrir dans un onglet separe
            if (cyoptions.newtab == true || cyoptions.newtab == "true")
                a.setAttribute("target", "_blank");
            // Pour telecharger un fichier
            if (cyoptions.download != undefined && cyoptions.download != '')
                a.download = cyoptions.download;
        }
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }
}
/*
 * Appel fetch() Prévu initialement pour récupérer du JSON.
 *       url : "http:/.......",
 * - options = "http:/......"
 * - options = {
 *       method: 'POST', // (défault)
 *       headers: {
 *           'Accept': 'application/json',
 *           'Content-Type': 'application/json'
 *       }, // (default)
 *       body: JSON.stringify({ "a": 1, "b": 2 ... }),
 *       mode: "cors", // cors/no-cors/same-origin
 *       credentials: "same-origin",
 *       modal: true,
 *       callbackThen: function() {}
 *       callbackCatch: function() {}
 *       callbackFinally: function() {}
 *   }
 * Exemples :
 *     CYFetchJSON("http://place67.com/data.json")
 *     CYFetchJSON("http://place67.com/data.json", {
 *       body: { a: 1, b: "22" }, 
 *       callbackThen: function(json) { .. },
 *       callbackCatch: function(errormsg) { .. },
 *       callbackFinally: function() { .. }
 *     })
 */
function CYFetchJSON(url, options) {
    let method = "POST",
        headers = { 'Content-Type': 'application/json' },
        body = "",
        mode = "same-origin",
        credentials = "same-origin",
        modal = true,
        now = Date.now() % 200000;

    if (typeof options == "object") {
        if (options.method != undefined)
            method = options.method;
        if (options.headers != undefined)
            headers = options.headers;
        if (options.body != undefined)
            body = options.body;
        if (options.mode != undefined)
            mode = options.mode;
        if (options.credentials != undefined)
            credentials = options.credentials;
        if (options.method != undefined)
            method = options.method;
    }
    debug && console.log('CYJS : CYFetchJSON ' + now + ' ' + url + ' body ' + JSON.stringify(body));
    modal && $("#IDmain").CYSpinLoader({ modal: true });
    fetch(url, {
        method: method,
        headers: headers,
        body: JSON.stringify(body),
        mode: mode,
        credentials: credentials
    }).then(response => {
        if (response.ok)
            return response.json();
        return Promise.reject(response);
    }).then(json => {
        debug && console.log('CYJS : CYFetchJSON ' + now + ' then json length = ' + Object.keys(json).length);
        if (typeof options.callbackThen == "function") {
            debug && console.log('CYJS : CYFetchJSON ' + now + ' callbackThen');
            options.callbackThen(json);
        }
    }).catch(error => {
        let msg = error;
        if (typeof error.statusText == "string" && typeof error.status == "number")
            msg = error.statusText + ' ' + error.status;
        else if (typeof error.message == "string")
            msg = error.message;
        else if (typeof error == "object")
            msg = JSON.stringify(error);
        debug && console.log('CYJS : CYFetchJSON ' + now + ' catch error = ' + msg);
        if (typeof options.callbackCatch == "function") {
            debug && console.log('CYJS : CYFetchJSON ' + now + ' callbackCatch');
            options.callbackCatch(msg);
        }
    }).finally(() => {
        debug && console.log('CYJS : CYFetchJSON ' + now + ' finally');
        modal && $("#IDmain").CYSpinLoader(false);
        if (typeof options.callbackFinally == "function") {
            debug && console.log('CYJS : CYFetchJSON ' + now + ' callbackFinally');
            options.callbackFinally();
        }
    });
}
/*
 * Gestion du loader avec masque.
 * Il faut agir ainsi, donc en gérant successivement la visibility et le hide, car beaucoup
 * d'objets jQuery ont besoin d'être affichés pour fonctionner, comme fullCalendar.
 *     CYLoader("on") ou CYLoader("off")
 *     CYLoader(true) ou CYLoader(false)
 */
function CYLoader(onoff) {
    if (onoff == true || onoff == "on" || onoff == "ON") {
        debug && console.log('CYJS : CYLoader on');
        $("#IDmain").css("visibility", "hidden");
        $('#IDloader').css("z-index", 2147483647).fadeIn("slow");
    } else {
        debug && console.log('CYJS : CYLoader off');
        $("#IDmain").hide();
        $('#IDloader').fadeOut("slow", function () {
            $(this).css("z-index", "");
        });
        $("#IDmain").css("visibility", "visible");
        $("#IDmain").fadeIn("1000");
    }
}
/*
 * Effectue les traductions dans l'id qui peut être un id, une classe, un objet .. ou '*' par défaut
 */
function CYTranslate(id) {
    if (id == undefined)
        id = "*";
    (debug & DEBUG_MORE) && console.log('CYJS : CYTranslate ' + id);
    // Traduction des tag <l>Chaine</l> et des label/option/th/legend dans l'id specifié
    $(id).findall("l,label,option,th,legend").each(function () {
        var str = $(this).html().trim();
        var trans = CYTranslateString(str);
        // Tag <l>Chaine</l>
        if ($(this).prop("tagName") == 'L')
            $(this).replaceWith(trans);
        else
            $(this).html(trans);
    });
    // Traduction des attributs title
    $(id).findall("[title]").each(function () {
        var str = $(this).attr('title');
        var trans = CYTranslateString(str);
        $(this).attr('title', trans);
    });
    // Traduction des attributs ui-dialog-title
    $(id).findall("[ui-dialog-title]").each(function () {
        var str = $(this).attr('ui-dialog-title');
        var trans = CYTranslateString(str);
        $(this).attr('ui-dialog-title', trans);
    });
    // Traduction des attributs label
    $(id).findall("[label]").each(function () {
        var str = $(this).attr('label');
        var trans = CYTranslateString(str);
        $(this).attr('label', trans);
    });
    // Traduction des attributs placeholder
    $(id).findall("[placeholder]").each(function () {
        var str = $(this).attr('placeholder');
        var trans = CYTranslateString(str);
        $(this).attr('placeholder', trans);
    });
    // Traduction des attributs data-inputmask-placeholder
    $(id).findall("[data-inputmask-placeholder]").each(function () {
        var str = $(this).attr('data-inputmask-placeholder');
        var trans = CYTranslateString(str);
        $(this).attr('data-inputmask-placeholder', trans);
    });
    // Traduction des attributs data-inputmask-inputformat
    $(id).findall("[data-inputmask-inputformat]").each(function () {
        var str = $(this).attr('data-inputmask-inputformat');
        var trans = CYTranslateString(str);
        $(this).attr('data-inputmask-inputformat', trans);
    });
    // Traduction des value dans input submit
    $(id).findall("input[type=submit]").each(function () {
        var str = $(this).attr('value');
        var trans = CYTranslateString(str);
        $(this).attr('value', trans);
    });
}
/*
 * Traduction de str selon cyString[str]
 */
function CYTranslateString(str) {
    var trans = cyString[str];
    if (trans == undefined) {
        trans = str;
        (debug & DEBUG_MORE) && console.log('CYJS : CYTranslateString -> translate of "' + str + '" undefined');
    } else
        (debug & DEBUG_MORE) && console.log('CYJS : CYTranslateString -> translate "' + str + '" to "' + trans + '"');
    return (trans);
}
/*
 * Génération de code HTML en boucle pour dupliquer une section.
 * Avec substitution de %% par un compteur de boucle.
 * Avec récursivité auto !
 * Par défaut, le tag lui-même est cloné. Pour ne cloner que son contenu, utiliser data-cloneself="false"
 * Utiliser data-clone="X" pour X itérations.
 * Utiliser data-clonefirst="X" pour définir X en tant que premier indice.
 * Utiliser data-clonestep="X" pour le pas d'incrément.
 * Ex :
 *     <div class="dummy" data-clone="2" data-cloneself="false" data-clonefirst="1" data-clonestep="3">Clone number %%</div>
 * génèrera :
 *     <div class="dummy">Clone number 1</div>
 *     <div class="dummy">Clone number 4</div>
 * Sans data-cloneself (positionné par défaut) :
 *     Loop number 1
 *     Loop number 4
 */
function CYCloneHTML(id) {
    debug && console.log('CYJS : CYCloneHTML "' + id + '"');
    $(id).findall("[data-clone]").each(function () {
        $(this).find("[data-clone]").each(function () {
            CYCloneHTML(this);
        });
        var boucles = $(this).attr('data-clone');
        $(this).removeAttr('data-clone');
        if ($(this).attr('data-clonestart') != undefined) {
            var start = parseInt($(this).attr('data-clonestart'));
            $(this).removeAttr('data-clonestart');
        } else
            var start = 1;
        if ($(this).attr('data-clonestep') != undefined) {
            var step = parseInt($(this).attr('data-clonestep'));
            $(this).removeAttr('data-clonestep');
        } else
            var step = 1;
        var self = true;
        if ($(this).attr('data-cloneself') != undefined) {
            if ($(this).attr('data-cloneself') == 'false')
                self = false;
            $(this).removeAttr('data-cloneself');
        }
        if (self)
            var modele = $(this).wrap('<p>').parent().html();
        else
            var modele = $(this).html();
        var html = '';
        for (var i = 0, j = start; i < boucles; i++, j += step * 1)
            html += CYReplaceAll(modele, '%%', j);
        if (self)
            $(this).parent().replaceWith(html);
        else
            $(this).replaceWith(html);
        CYTranslate(this);
    });
}
/*
 * Modifier le titre dans header, ou l'effacer si pas de titre précisé
 * icon facultatif(à gauche du titre)
 * Si customTitle est précisé, il sera inséré dans #IDheadermaintitlecustom
 * qui sera automatiquement ajouté si absent après #IDheadermaintitle
 */
function CYMainTitle(titre, icon, customTitle) {
    debug && console.log('CYJS : CYMainTitle titre="' + titre + '" icon="' + icon + '"');
    if (titre == undefined || titre == '')
        $("#IDheadermaintitle").html('');
    else
        $("#IDheadermaintitle").html(CYTranslateString(titre));
    $("#IDheadericonmain").removeClass();
    if (icon != undefined && icon != '')
        $("#IDheadericonmain").addClass(CYFontAwesomeIconName(icon));
    if (customTitle != undefined) {
        if (!$("#IDheadermaintitlecustom").length)
            $("#IDheadermaintitle").after('<span id="IDheadermaintitlecustom"></span>');
        $("#IDheadermaintitlecustom").html(customTitle);
    }
}
/*
 * Simple login sans auth
 */
function CYLogin(user) {
    if (user != undefined && user != '')
        cyUsername = user;
}
/*
 * Logout et redirection si url présente
 */
function CYLogoff(url) {
    debug && console.log('CYJS : CYLogoff');
    $(".ClassIconFile").remove();
    $(".ClassEditor").remove();
    $("#IDheaderusername").html('');
    cyUsername = cyAutoLogin = '';
    if (cyCache)
        CYDB({ command: "clear" });
    $("page").hide();
    $("body").css('background-image', cyBackImg);
    if (url != undefined && url != '') {
        debug && console.log('CYJS : CYLogoff redirect to ' + url);
        window.location.href = url;
    }
    CYLoginFct();
}

function CYLogged(headerright) {
    cyBackImg = $("body").css('background-image');
    $("body").css('background-image', 'none');
    $("page").show();
    $("#IDheaderusername").html(headerright);
}

function CYSetCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function CYGetCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
/*
 * Affiche (true) / cache (false) sidemenu
 * En cas de windows.resize, l'animation est aléatoire, donc je l'évite.
 * Une fonction custom peut être positionnée => cyOpenSideMenuCustom()
 */
function CYOpenSideMenu(show, keepmain, animate, w) {
    debug && console.log('CYJS : CYOpenSideMenu show=' + show + ',keep=' + keepmain + ",animate=" + animate + ",w=" + w);
    if (show == false) {
        if (animate) {
            $("#IDmain").animate({
                left: "0px"
            }, 500);
            $("#IDsidemenu").animate({
                width: "0px"
            }, 500);
        } else {
            $("#IDmain").css('left', "0px");
            $("#IDsidemenu").css('width', "0px");
        }
    } else {
        if (w == undefined)
            w = cySideMenuWidth;
        if (animate) {
            if (!keepmain) {
                $("#IDmain").animate({
                    left: w
                }, 500);
                $("#IDmain").css('left', w);
            }
            $("#IDsidemenu").animate({
                width: w
            }, 500);
            $("#IDsidemenu").css('width', w);
        } else {
            if (!keepmain)
                $("#IDmain").css('left', w);
            $("#IDsidemenu").css('width', w);
        }
    }
    if (typeof (window["cyOpenSideMenuCustom"]) == 'function')
        cyOpenSideMenuCustom(show);
}
/*
 * Gestion auto menu slider, un simple div dans sidemenu.html suffit :
 *        <div id="IDmenuslider">
 *            <i class="fa fa-ellipsis-v"></i>
 *        </div>
 * On peut spécifier un id différent de "IDmenuslider"
 */
function CYMenuSlider(id) {
    if (id == undefined || id == '')
        var id = "#IDmenuslider";
    debug && console.log('CYJS : CYMenuSlider id=' + id);
    // Detection relachement clic gauche
    $(document).mouseup(function () {
        cyMouseDown = false;
    });
    $(id).mousedown(function (event) {
        event.preventDefault();
        cyMouseDown = true;
    });
    $(document).mousemove(function (event) {
        if (cyMouseDown) {
            cySideMenuWidth = event.pageX;
            $("#IDmain").css('left', cySideMenuWidth + "px");
            $("#IDsidemenu").css('width', cySideMenuWidth + "px");
        }
    });
}
/*
 *  Message d'alerte modal.
 *  options peut être soit le contenu du message à afficher, soit l'objet suivant :
 *      {   title: "Le titre",
 *          width: 400,
 *          height: 300,
 *          maxWidth: false,
 *          maxHeight: false,
 *          minWidth: false,
 *          minHeight: false,
 *          modal: true,
 *          resizable: true,
 *          message: "Contenu du message",
 *          icon: "icone",
 *          autofade: 0,
 *          closeOnEscape: true,
 *          fullscreen: false,
 *          okbuttontext: 'OK',
 *          okfunctionName : function() ... ,
 *          close: true,
 *          classes: "hightlight",
 *          id : "ClassAlertID" // Dynamique si absent
 *          appendTo: "#IDmain" // Défaut
 *          openFunction: function { ... } // Fonction appelée à l'ouverture
 *          resizeFunction: function { ... } // Fonction appelée lors d'un resize
 *          closeFunction: function { ... } // Fonction appelée lors avant le destroy
 *      }
 *  Si icon est fourni et vide -> icone par defaut "default"
 *  Si icon est fourni et non vide -> icone a utiliser, ex: "exclamation-circle"
 *  okfunctionName est appelée à la fermeture et doit retourner true pour fermer
 *  le popup (peut etre fourni dans options)
 *  Retourne l'id dynamique cree
 *  cf. http://api.jqueryui.com/dialog
 */
function CYAlert(options, okfunctionName) {
    var thetitle = CYTranslateString("MESSAGE");
    var height = 'auto';
    var width = cyAlertWidth;
    var modal = false;
    var resizable = false;
    var position = { my: "center", at: "center", of: window };
    var maxHeight = maxWidth = minHeight = minWidth = fullscreen = false;
    var id = "IDalert" + CYUniqueId();
    var message = CYTranslateString("MESSAGE");
    var icon = '';
    var autofade = cyAlertDelay;
    var close = true;
    var closeOnEscape = true;
    var okbuttontext = "OK";
    var classes = "";
    var appendTo = "#IDmain";
    var openFunction = null;
    var resizeFunction = null;
    var closeFunction = null;

    if (CYAtoI($(".ClassAlert").css("width")) > 0)
        width = CYAtoI($(".ClassAlert").css("width"));
    debug && console.log('CYJS : CYAlert options="' + CYImplode(',', options, true) + '"');
    if (typeof options == "object") {
        if (options.title != undefined)
            thetitle = CYTranslateString(options.title);
        if (options.width != undefined)
            width = options.width;
        if (options.height != undefined)
            height = options.height;
        if (options.modal != undefined)
            modal = options.modal;
        if (options.resizable != undefined)
            resizable = options.resizable;
        if (options.fullscreen != undefined)
            fullscreen = options.fullscreen;
        if (options.maxHeight != undefined)
            maxHeight = options.maxHeight;
        if (options.maxWidth != undefined)
            maxWidth = options.maxWidth;
        if (options.minHeight != undefined)
            minHeight = options.minHeight;
        if (options.minWidth != undefined)
            minWidth = options.minWidth;
        if (options.message != undefined)
            message = CYTranslateString(options.message);
        if (options.icon != undefined)
            icon = options.icon;
        if (options.autofade != undefined)
            autofade = options.autofade;
        if (options.closeOnEscape != undefined)
            closeOnEscape = options.closeOnEscape;
        if (options.okbuttontext != undefined)
            okbuttontext = options.okbuttontext;
        if (options.okfunctionName != undefined)
            okfunctionName = options.okfunctionName;
        if (options.classes != undefined)
            classes = options.classes;
        if (options.position != undefined)
            position = options.position;
        if (options.id != undefined)
            id = options.id;
        if (options.close != undefined)
            close = options.close;
        if (options.appendTo != undefined)
            appendTo = options.appendTo;
        if (options.openFunction != undefined)
            openFunction = options.openFunction;
        if (options.resizeFunction != undefined)
            resizeFunction = options.resizeFunction;
        if (options.closeFunction != undefined)
            closeFunction = options.closeFunction;
    } else
        message = CYTranslateString(options);
    var boutons = [{
        text: CYTranslateString(okbuttontext),
        click: function () {
            if (okfunctionName != undefined && okfunctionName != '') {
                if (okfunctionName(this) != false)
                    $(this).dialog("close");
            } else
                $(this).dialog("close");
        }
    }];
    message = '<div style="width: auto; height: inherit">' + message + '</div>';
    if (icon != '') {
        if (icon.trim() == 'default')
            icon = 'fa fa-exclamation-circle';
        message = '<div><i class="' + CYFontAwesomeIconName(icon) + '"></i></div>' + message;
    }
    $(cyPageContent).append('<div class="ClassAlert" id="' + id + '"></div>');
    $("#" + id).html(message);
    // Astuce nécessaire pour positionner le dialog on top
    let zindex = modal ? CYAtoI($(".ui-widget-overlay").css("z-index")) + 1 : CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile", true) + 1;
    CYCreateCSS(".ui-dialog-on-top", "z-index: " + zindex, "ui-dialog-on-top");
    $("#" + id).dialog({
        dialogClass: "ui-dialog-on-top " + classes,
        title: thetitle,
        closeText: '',
        width: width,
        height: height,
        modal: modal,
        resizable: resizable,
        maxHeight: maxHeight,
        maxWidth: maxWidth,
        minHeight: minHeight,
        minWidth: minWidth,
        buttons: boutons,
        closeOnEscape: closeOnEscape,
        position: position,
        appendTo: appendTo,
        show: { effect: 'fade', duration: cyEffectDuration },
        hide: { effect: 'fade', duration: cyEffectDuration },
        dragStart: function (event, ui) {
            var zindex = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile");
            var parent = $(this).parent();
            if (parent.css("z-index") < zindex)
                parent.css("z-index", zindex + 1);
        },
        create: function (event, ui) {
            if (!fullscreen)
                $(this).parent().find(".ui-dialog-titlebar-fullscreen").hide();
            if (!close)
                $(this).parent().find(".ui-dialog-titlebar-close").hide();
            else
                $(this).parent().find(".ui-dialog-titlebar-close").removeClass('ui-button-icon-only').html("<i class='fa fa-window-close'></i>");
        },
        open: function (event) {
            // Pour réagir sur le contenu en cas de fullscreen -> comme resize ci-dessous
            if (fullscreen && resizeFunction != null) {
                $(".ui-dialog-titlebar-fullscreen").click(function (ev) {
                    resizeFunction($(this).closest('.ui-dialog').find('.ClassAlert'));
                });
            }
            $(this).CYTooltip();
            if (openFunction != null)
                openFunction(this);
            // Un click sur le dialog pour le positionner on top
            if(!modal)
                $(this).parent().on("mousedown", function () {
                    $(this).CYToTop(".ui-dialog,.ClassWindow,.ClassIconFile");
                });
        },
        close: function () {
            if (closeFunction != null)
                closeFunction(this);
            // Entre l'appel du dialog('close') et ici, il peut s'en passer des choses ..
            // et le ui-dialog n'a peut-être pas disparu..
            if ($(this).hasClass('ui-dialog-content'))
                $(this).dialog("destroy");
            // Le remove doit impérativement être là car jquery peut supprimer le ui-dialog sans nettoyer
            // son contenu
            $(this).remove();
        },
        resize: function (ev) {
            // Pour réagir sur le contenu en cas de resizing
            // this => objet ClassAlert
            if (resizeFunction != null)
                resizeFunction(this);
        }
    });
    if (autofade != 0) {
        setTimeout(function () {
            // Le dialog a ete ferme avant time-out
            if (!$("#" + id).length)
                return;
            if (okfunctionName != undefined && okfunctionName != '') {
                if (okfunctionName(null) != false)
                    $("#" + id).dialog("close");
            } else
                $("#" + id).dialog("close");
        }, autofade);
    }
    return ("#" + id);
}
/*
 *  Message de confirmation modal avec 2 boutons OK et CANCEL
 *  options peut être soit le contenu du message à afficher, soit l'objet suivant :
 *      {   title: "Le titre",
 *          width: 400,
 *          height: 300,
 *          maxWidth: false,
 *          maxHeight: false,
 *          minWidth: false,
 *          minHeight: false,
 *          modal: true,
 *          resizable: true,
 *          message: "Contenu du message",
 *          icon: "question-circle",
 *          defaultbutton: 0,
 *          closeOnEscape: true,
 *          fullscreen: false,
 *          okbuttontext: "OK",
 *          cancelbuttontext: "CANCEL",
 *          close: true,
 *          classes: "hightlight",
 *          id : "ClassAlertID" // Dynamique si absent
 *          appendTo: "#IDmain" // Défaut
 *          openFunction: function { ... } // Fonction appelée à l'ouverture
 *          closeFunction: function { ... } // Fonction appelée lors avant le destroy
 *          okFunction: function { ... } // Fonction appelée si click okbutton
 *          cancelFunction: function { ... } // Fonction appelée si click cancelbutton
 *      }
 *  Si title est vide -> defaut 'CONFIRMATION'
 *      title peut etre object, cf. CYAlert()
 *  Si icon est vide -> pas d'icone
 *  Si icon est non vide -> icone a utiliser, ex: "exclamation-circle"
 *  defaultbutton designe le numero du bouton par defaut : 0 ou 1
 *
 *  okFunction (opt) sera appelée si click sur OK et le modal sera fermé si elle
 *  retourne != false. Peut être défini prioritairement dans options.
 *
 *  cancelFunction (opt) sera appelée si click sur CANCEL et le modal sera fermé si elle
 *  retourne != false. Peut être défini prioritairement dans options.
 */
function CYConfirm(options, okFunction, cancelFunction) {
    var thetitle = CYTranslateString("CONFIRMATION");
    var height = 'auto';
    var width = cyConfirmWidth;
    var modal = true;
    var position = { my: "center", at: "center", of: window };
    var resizable = false;
    var maxHeight = maxWidth = minHeight = minWidth = fullscreen = false;
    var id = "IDconfirm" + CYUniqueId();
    var message = CYTranslateString("PLEASE CONFIRM");
    var icon = '';
    var defaultbutton = 0;
    var close = true;
    var closeOnEscape = true;
    var okbuttontext = "OK",
        cancelbuttontext = "CANCEL";
    var classes = "";
    var appendTo = "#IDmain";
    var openFunction = null;
    var closeFunction = null;

    if (CYAtoI($(".ClassConfirm").css("width")) > 0)
        width = CYAtoI($(".ClassConfirm").css("width"));
    debug && console.log('CYJS : CYConfirm options="' + CYImplode(',', options) + '"');
    if (typeof options == "object") {
        if (options.title != undefined)
            thetitle = CYTranslateString(options.title);
        if (options.width != undefined)
            width = options.width;
        if (options.height != undefined)
            height = options.height;
        if (options.modal != undefined)
            modal = options.modal;
        if (options.resizable != undefined)
            resizable = options.resizable;
        if (options.fullscreen != undefined)
            fullscreen = options.fullscreen;
        if (options.maxHeight != undefined)
            maxHeight = options.maxHeight;
        if (options.maxWidth != undefined)
            maxWidth = options.maxWidth;
        if (options.minHeight != undefined)
            minHeight = options.minHeight;
        if (options.minWidth != undefined)
            minWidth = options.minWidth;
        if (options.message != undefined)
            message = options.message;
        if (options.icon != undefined)
            icon = options.icon;
        if (options.defaultbutton != undefined)
            defaultbutton = options.defaultbutton;
        if (options.closeOnEscape != undefined)
            closeOnEscape = options.closeOnEscape;
        if (options.okbuttontext != undefined)
            okbuttontext = options.okbuttontext;
        if (options.cancelbuttontext != undefined)
            cancelbuttontext = options.cancelbuttontext;
        if (options.classes != undefined)
            classes = options.classes;
        if (options.position != undefined)
            position = options.position;
        if (options.id != undefined)
            id = options.id;
        if (options.close != undefined)
            close = options.close;
        if (options.appendTo != undefined)
            appendTo = options.appendTo;
        if (options.openFunction != undefined)
            openFunction = options.openFunction;
        if (options.closeFunction != undefined)
            closeFunction = options.closeFunction;
        if (options.okFunction != undefined)
            okFunction = options.okFunction;
        if (options.cancelFunction != undefined)
            cancelFunction = options.cancelFunction;
    } else
        message = options;
    var boutons = [{
        text: CYTranslateString(okbuttontext),
        click: function () {
            if (okFunction != undefined && okFunction != '') {
                if (okFunction(this) != false)
                    $(this).dialog("close");
            } else
                $(this).dialog("close");
        }
    },
    {
        text: CYTranslateString(cancelbuttontext),
        click: function () {
            if (cancelFunction != undefined && cancelFunction != '') {
                if (cancelFunction() != false)
                    $(this).dialog("close");
            } else
                $(this).dialog("close");
        }
    }
    ];
    message = '<div style="width: auto; height: inherit">' + message + '</div>';
    if (icon != '') {
        if (icon.trim() == 'default')
            icon = 'fa fa-exclamation-circle';
        message = '<div><i class="' + CYFontAwesomeIconName(icon) + '"></i></div>' + message;
    }
    $(cyPageContent).append('<div class="ClassConfirm" id="' + id + '"></div>');
    $("#" + id).html(message);
    // Astuce nécessaire pour positionner le dialog on top
    let zindex = modal ? CYAtoI($(".ui-widget-overlay").css("z-index")) + 1 : CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile", true) + 1;
    CYCreateCSS(".ui-dialog-on-top", "z-index: " + zindex, "ui-dialog-on-top");
    $("#" + id).dialog({
        dialogClass: "ui-dialog-on-top " + classes,
        title: thetitle,
        closeText: '',
        width: width,
        height: height,
        modal: modal,
        resizable: resizable,
        maxHeight: maxHeight,
        maxWidth: maxWidth,
        minHeight: minHeight,
        minWidth: minWidth,
        buttons: boutons,
        closeOnEscape: closeOnEscape,
        classes: classes,
        position: position,
        appendTo: appendTo,
        show: { effect: 'fade', duration: cyEffectDuration },
        hide: { effect: 'fade', duration: cyEffectDuration },
        dragStart: function (event, ui) {
            var zindex = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile");
            var parent = $(this).parent();
            if (parent.css("z-index") < zindex)
                parent.css("z-index", zindex + 1);
        },
        create: function (event, ui) {
            if (!fullscreen)
                $(this).parent().find(".ui-dialog-titlebar-fullscreen").hide();
            if (!close)
                $(this).parent().find(".ui-dialog-titlebar-close").hide();
            else
                $(this).parent().find(".ui-dialog-titlebar-close").removeClass('ui-button-icon-only').html("<i class='fa fa-window-close'></i>");
        },
        open: function (event, ui) {
            // Supprime la croix de fermeture du dialog
            //$(".ui-dialog-titlebar-close").remove();
            $(this).closest(".ui-dialog").find(".ui-dialog-buttonset").find(".ui-button").eq(defaultbutton).focus();
            $(this).CYTooltip();
            if (openFunction != null)
                openFunction(this);
            // Un click sur le dialog pour le positionner on top
            if(!modal)
                $(this).parent().on("mousedown", function () {
                    $(this).CYToTop(".ui-dialog,.ClassWindow,.ClassIconFile");
                });
        },
        close: function () {
            if (closeFunction != null)
                closeFunction(this);
            $(this).dialog("destroy");
            $(this).remove();
        }
    });
    return ("#" + id);
}
/*
 *  Saisie d'un input ou textarea en prompt modal
 *  options peut être soit le contenu du libellé à afficher, soit l'objet suivant:
 *      {   title: "Le titre",
 *          width: 400,
 *          height: 300,
 *          maxWidth: false,
 *          maxHeight: false,
 *          minWidth: false,
 *          minHeight: false,
 *          modal: true,
 *          resizable: true,
 *          label: "Libellé",
 *          icon: "icone",
 *          defaultbutton: 0,
 *          defaultvalue: '',
 *          required: false,
 *          maxlength: 0,
 *          size: 0,
 *          close: true,
 *          closeOnEscape: true,
 *          fullscreen: false,
 *          mask: "a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ._\s-",
 *          dialogClass: "highlight",
 *          id : "ClassAlertID" // Dynamique si absent
 *          appendTo: "#IDmain" // Défaut
 *          openFunction: function { ... } // Fonction appelée à l'ouverture
 *          closeFunction: function { ... } // Fonction appelée lors avant le destroy
 *          okFunction: function { ... } // Fonction appelée lors du click sur okbutton
 *      }
 *  Une valeur par defaut peut être presente
 *  required pour forcer / on ne peut quitter sans saisie (sauf Esc)
 *  Si title est vide -> defaut 'PLEASE INPUT'
 *      title peut etre object, cf. CYAlert()
 *  Si icon est vide -> pas d'icone
 *  Si icon est non vide -> icone a utiliser, ex: "exclamation-circle"
 *  okFunction sera appelée lors de la validation : si elle retourne false,
 *  on reste en saisie. Peut être défini prioritairement dans options.
 *  si nblignes > 1, j'utilise un textarea sur nblignes au lieu d'un simple input
 *  si nblignes est un objet => { lines: 5, maxlength: 50 }
 */
function CYPrompt(options, okFunction) {
    var thetitle = CYTranslateString("PLEASE INPUT");;
    var height = 'auto';
    var width = cyPromptWidth;
    var modal = resizable = true;
    var position = { my: "center", at: "center", of: window };
    var maxHeight = maxWidth = minHeight = minWidth = fullscreen = false;
    var id = "IDprompt" + CYUniqueId();
    var label = '';
    var icon = '';
    var defaultbutton = 0;
    var defaultvalue = '';
    var required = false;
    var lines = 1;
    var maxlength = 0;
    var size = 0;
    var close = true;
    var closeOnEscape = true;
    var mask = "";
    var classes = "";
    var appendTo = "#IDmain";
    var openFunction = null;
    var closeFunction = null;

    debug && console.log('CYJS : CYPrompt options="' + CYImplode(',', options) + '"');
    if (typeof options == "object") {
        if (options.title != undefined)
            thetitle = CYTranslateString(options.title);
        if (options.width != undefined)
            width = options.width;
        if (options.height != undefined)
            height = options.height;
        if (options.modal != undefined)
            modal = options.modal;
        if (options.resizable != undefined)
            resizable = options.resizable;
        if (options.fullscreen != undefined)
            fullscreen = options.fullscreen;
        if (options.maxHeight != undefined)
            maxHeight = options.maxHeight;
        if (options.maxWidth != undefined)
            maxWidth = options.maxWidth;
        if (options.minHeight != undefined)
            minHeight = options.minHeight;
        if (options.minWidth != undefined)
            minWidth = options.minWidth;
        if (options.label != undefined)
            label = CYTranslateString(options.label);
        if (options.icon != undefined)
            icon = options.icon;
        if (options.defaultbutton != undefined)
            defaultbutton = options.defaultbutton;
        if (options.defaultvalue != undefined)
            defaultvalue = CYTranslateString(options.defaultvalue);
        if (options.required != undefined)
            required = options.required;
        if (options.lines != undefined)
            lines = options.lines;
        if (options.maxlength != undefined)
            maxlength = options.maxlength;
        if (options.size != undefined)
            size = options.size;
        if (options.closeOnEscape != undefined)
            closeOnEscape = options.closeOnEscape;
        if (options.mask != undefined)
            mask = options.mask;
        if (options.classes != undefined)
            classes = options.classes;
        if (options.position != undefined)
            position = options.position;
        if (options.id != undefined)
            id = options.id;
        if (options.close != undefined)
            close = options.close;
        if (options.appendTo != undefined)
            appendTo = options.appendTo;
        if (options.openFunction != undefined)
            openFunction = options.openFunction;
        if (options.closeFunction != undefined)
            closeFunction = options.closeFunction;
        if (options.okFunction != undefined)
            okFunction = options.okFunction;
    } else
        label = CYTranslateString(options);
    if (icon != '') {
        if (icon.trim() == 'default')
            icon = 'data-icon="fa-exclamation-circle"';
        else
            icon = 'data-icon="' + icon + '"';
    }
    if (lines <= 1) {
        var objpromptvalue = "#" + id + " input";
        var html = '<input value="' + defaultvalue + '" type="text"';
        if (size)
            html += ' size="' + size + '"';
        if (maxlength)
            html += ' maxlength="' + maxlength + '"';
        if (mask != "")
            html += ' data-mask="' + mask + '"';
        html += '>';
    } else {
        var objpromptvalue = "#" + id + " textarea";
        var html = '<textarea rows="' + lines + '"';
        if (maxlength)
            html += ' maxlength="' + maxlength + '"';
        html += '>' + defaultvalue + '</textarea>';
    }
    $(cyPageContent).append('<form class="ClassForm ClassPrompt" id="' + id + '"><div class="ClassRowThin"><div class="ClassCell" style="width:100%"><label></label><div class="ClassInput ClassInputSingle" data-clear="true" ' + icon + '>' + html + '</div></div></div></form>');

    var oldpromptvalue = $(objpromptvalue).html();
    var boutons = [{
        default: defaultbutton == 1 ? "true" : "false",
        text: CYTranslateString('OK'),
        click: function () {
            // Il faut saisir !
            if (required && $(objpromptvalue).val() == '')
                return;
            if (okFunction != undefined && okFunction != '') {
                if (okFunction($(objpromptvalue).val(), this) != false)
                    $(this).dialog("close");
            } else
                $(this).dialog("close");
        }
    }];
    if (!required) {
        boutons.push({
            default: "false",
            text: CYTranslateString('CANCEL'),
            click: function () {
                $(objpromptvalue).html(oldpromptvalue);
                $(this).dialog("destroy").remove();
            }
        });
    }
    $("#" + id + " label").html(label);
    // Astuce nécessaire pour positionner le dialog on top
    let zindex = modal ? CYAtoI($(".ui-widget-overlay").css("z-index")) + 1 : CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile", true) + 1;
    CYCreateCSS(".ui-dialog-on-top", "z-index: " + zindex, "ui-dialog-on-top");
    $("#" + id).dialog({
        dialogClass: "ui-dialog-on-top ",
        title: thetitle,
        closeText: '',
        width: width,
        height: height,
        modal: modal,
        resizable: resizable,
        maxHeight: maxHeight,
        maxWidth: maxWidth,
        minHeight: minHeight,
        minWidth: minWidth,
        buttons: boutons,
        closeOnEscape: closeOnEscape,
        position: position,
        appendTo: appendTo,
        show: { effect: 'fade', duration: cyEffectDuration },
        hide: { effect: 'fade', duration: cyEffectDuration },
        dragStart: function (event, ui) {
            var zindex = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile");
            var parent = $(this).parent();
            if (parent.css("z-index") < zindex)
                parent.css("z-index", zindex + 1);
        },
        create: function (event, ui) {
            if (!fullscreen)
                $(this).parent().find(".ui-dialog-titlebar-fullscreen").hide();
            if (!close)
                $(this).parent().find(".ui-dialog-titlebar-close").hide();
            else
                $(this).parent().find(".ui-dialog-titlebar-close").removeClass('ui-button-icon-only').html("<i class='fa fa-window-close'></i>");
        },
        open: function (event, ui) {
            // Un bouton par "default" peut être positionné
            $("#" + id).keyup(function (e) {
                if (e.keyCode == 13) {
                    $("#" + id).parent().find("button[default=true]").trigger("click");
                    e.stopImmediatePropagation();
                }
            });
            // Bloque auto soumission du formulaire
            $("#" + id).submit(function (e) {
                e.preventDefault;
                e.stopImmediatePropagation();
                return (false);
            });
            CYInput(this);
            $(objpromptvalue).focus();
            $(this).CYTooltip();
            if (openFunction != null)
                openFunction(this);
            // Un click sur le dialog pour le positionner on top
            if(!modal)
                $(this).parent().on("mousedown", function () {
                    $(this).CYToTop(".ui-dialog,.ClassWindow,.ClassIconFile");
                });
        },
        close: function () {
            if (closeFunction != null)
                closeFunction(this);
            $(this).dialog("destroy");
            $(this).remove();
        }
    });
    return ("#" + id);
}
/*
 *  Formulaire
 *  options peut être soit le titre du form à afficher, soit l'objet suivant
 *      {   title: "Le titre",
 *          width: 400,
 *          height: 300,
 *          maxWidth: false,
 *          maxHeight: false,
 *          minWidth: false,
 *          minHeight: false,
 *          modal: true,
 *          resizable: true,
 *          buttons: buttons, // Object array, attribut "default": "true" optionnel
 *          closeOnEscape: true,
 *          fullscreen: true,
 *          close: true,
 *          class: "highlight",
 *          id : "ClassAlertID" // Dynamique si absent
 *          appendTo: "#IDmain" // Défaut
 *          openFunction: function { ... } // Fonction appelée à l'ouverture
 *          resizeFunction: function { ... } // Fonction appelée lors d'un resize
 *          closeFunction: function { ... } // Fonction appelée lors avant le destroy
 *      }
 *  Il faut impérativement le fermer avec close() et non destroy() pour tout nettoyer,
 *  car le destroy sera fait alors.
 */
function CYForm(id, options) {
    var title = '';
    var height = 'auto';
    var width = 'auto';
    var modal = resizable = fullscreen = close = true;
    var maxHeight = maxWidth = minHeight = minWidth = false;
    var position = { my: "center", at: "center", of: window };
    var boutons = [];
    var close = true;
    var closeOnEscape = true;
    var classes = "";
    var appendTo = "#IDmain";
    var openFunction = null;
    var resizeFunction = null;
    var closeFunction = null;

    if (id.indexOf('#') != 0)
        id = "#" + id;
    if (typeof options == undefined)
        options = {};
    if (typeof options == "object") {
        Object.assign(options, CYExtrAttrs(id));
        if (options.title != undefined)
            title = CYTranslateString(options.title);
        if (options.width != undefined)
            width = options.width;
        if (options.height != undefined)
            height = options.height;
        if (options.modal != undefined)
            modal = options.modal;
        if (options.resizable != undefined)
            resizable = options.resizable;
        if (options.fullscreen != undefined)
            fullscreen = options.fullscreen;
        if (options.close != undefined)
            close = options.close;
        if (options.maxHeight != undefined)
            maxHeight = options.maxHeight;
        if (options.maxWidth != undefined)
            maxWidth = options.maxWidth;
        if (options.minHeight != undefined)
            minHeight = options.minHeight;
        if (options.minWidth != undefined)
            minWidth = options.minWidth;
        if (options.buttons != undefined)
            boutons = options.buttons;
        if (options.closeOnEscape != undefined)
            closeOnEscape = options.closeOnEscape;
        if (options.classes != undefined)
            classes = options.classes;
        if (options.position != undefined)
            position = options.position;
        if (options.id != undefined)
            id = options.id;
        if (options.appendTo != undefined)
            appendTo = options.appendTo;
        if (options.openFunction != undefined)
            openFunction = options.openFunction;
        if (options.resizeFunction != undefined)
            resizeFunction = options.resizeFunction;
        if (options.closeFunction != undefined)
            closeFunction = options.closeFunction;
    } else
        title = CYTranslateString(options);
    debug && console.log('CYJS : CYForm id="' + id + '" options=' + CYImplode(',', options, true));
    // Astuce nécessaire pour positionner le dialog on top
    let zindex = modal ? CYAtoI($(".ui-widget-overlay").css("z-index")) + 1 : CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile", true) + 1;
    CYCreateCSS(".ui-dialog-on-top", "z-index: " + zindex, "ui-dialog-on-top");
    $(id).dialog({
        dialogClass: "ui-dialog-on-top " + classes,
        title: title,
        closeText: '',
        width: width,
        height: height,
        modal: modal,
        resizable: resizable,
        maxHeight: maxHeight,
        maxWidth: maxWidth,
        minHeight: minHeight,
        minWidth: minWidth,
        buttons: boutons,
        position: position,
        closeOnEscape: closeOnEscape,
        appendTo: appendTo,
        show: { effect: 'fade', duration: cyEffectDuration },
        hide: { effect: 'fade', duration: cyEffectDuration },
        dragStart: function (event, ui) {
            var zindex = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile");
            var parent = $(this).parent();
            if (parent.css("z-index") < zindex)
                parent.css("z-index", zindex + 1);
        },
        create: function (event, ui) {
            if (!fullscreen)
                $(this).parent().find(".ui-dialog-titlebar-fullscreen").hide();
            if (!close)
                $(this).parent().find(".ui-dialog-titlebar-close").hide();
            else
                $(this).parent().find(".ui-dialog-titlebar-close").removeClass('ui-button-icon-only').html("<i class='fa fa-window-close'></i>");
        },
        open: function (event) {
            // Necessaire, même si redondant car jQ ne les 'voit' pas depuis le popup
            CYSelect(id);
            $(id).find("[autofocus]").each(function () {
                debug && console.log('CYJS : CYForm focus on autofocus input');
                $(this).focus();
            });
            // Un bouton par "default" peut être positionné
            $(id).keyup(function (e) {
                if (e.keyCode == 13)
                    $(id).parent().find("button[default=true]").trigger("click");
            });
            // Bloque auto soumission du formulaire
            $(id).submit(function (e) {
                e.preventDefault;
                e.stopImmediatePropagation();
                return (false);
            });
            $(event.target).parent().CYTooltip();
            if (openFunction != null)
                openFunction(this);
            // Un click sur le dialog pour le positionner on top
            if(!modal)
                $(event.target).parent().on("mousedown", function () {
                    $(this).CYToTop(".ui-dialog,.ClassWindow,.ClassIconFile");
                });
        },
        close: function (event, ui) {
            // Fin aux events sur input ? A éviter car bloque data-clear sur un form en pleine page
            //$(id).find("input,textarea").off("focus");
            // Destroy des select - necessaire
            $(id).find("").each(function () {
                debug && console.log('CYJS : CYForm destroy select');
                $(this).selectmenu('destroy');
            });
            if (closeFunction != null)
                closeFunction(this);
            // Pas de remove ici, le form reste ds le HTML !!!
            $(this).dialog("destroy");
        },
        resize: function (ev, ui) {
            if (resizeFunction != null)
                resizeFunction(this, ui);
        }
    });
}
/*
 *  Recherche dans id des attributs de type data-xxx="..." et construit un object avec les xxx.
 *  Tient compte des conventions camelCase HTML5.
 *  Exemple:
 *      <input id="MonID" data-min="10" data-max="50" data-maxFileSize="12">
 *      CYExtrAttr("#MonID") va retourner l'objet { "min": "10", "max": "50" "maxFilesize: "12" }
 *  ATTENTION : les noms d'attributs sont toujours convertis en minuscules !
 */
function CYExtrAttrs(id) {
    var options = {};
    var attrs = [];
    $(id).each(function () {
        that = this;
        $.each(this.attributes, function () {
            if (this.specified && this.name.substring(0, 5) == 'data-')
                attrs.push({ name: this.name.substring(5).toCamel(), value: this.value });
        });
    });
    // Construit les options selon les attributs
    for (var i = 0; i < attrs.length; i++) {
        if (attrs[i].value == "false")
            options[attrs[i].name] = false;
        else if (attrs[i].value == "true")
            options[attrs[i].name] = true;
        else
            options[attrs[i].name] = attrs[i].value;
    }
    debug && console.log('CYJS : CYExtrAttrs options ' + JSON.stringify(options));
    return (options);
}
/*
 * Si id est précisé alors y positionne le focus, sinon là
 * où figure l'attribut autofocus
 */
function CYFocus(id) {
    if (id == undefined) {
        $("[autofocus]").each(function () {
            debug && console.log('CYJS : CYFocus on autofocus input');
            $(this).focus();
        });
    } else {
        if (id.indexOf('#') != 0)
            id = '#' + id;
        debug && console.log('CYJS : CYFocus on ' + id);
        $(id).focus();
    }
}
/*
 * Init all internal tools
 */
function CYInitAllTools(id) {
    debug && console.log('CYJS : CYInitAllTools ' + id);

    CYInput(id);
    CYDatePicker(id);
    CYSelect(id);
    CYSelectable(id);
    CYButton(id);
    CYCheckBox(id);
    CYFlipSwitch(id);
    CYTooltip(id);
    CYSpinner(id);
    CYTab(id);
    CYAccordion(id);
    CYPanel(id);
    CYMenu(id);
    //CYTree(id); loop is a pb
    CYEditor(id);
    CYFocus();
}
/*
 *  Usage :
 *  Dans JS :
 *   var columns = [
 *       {    width : "75%", data : "societe" },
 *       {   width : "25%", data : "ville" }
 *   ];
 *   var data = [
 *       {    "Societe" : "CYSTEME",
 *           "Ville" : "Strasbourg",
 *       },
 *       {    "Societe" : "LCCY",
 *           "Ville" : "Durningen",
 *       }
 *   ];
 *  Dans HTML :
 *  <table id="idtable" data-page-length='3' data-paging='1' data-searching='1' data-ordering='1' data-info='1' data-order='[[ 0, "desc" ]]' data-selectcolumns='true' data-selectauto='false'>
 *   <thead>
 *       <tr>
 *           <th data-filter='1'>
 *               Societe
 *           </th>
 *           <th>
 *               Ville
 *           </th>
 *       </tr>
 *    </thead>
 *  </table>
 * Exemple :
 *      var table = CYDataTable("#IDtableusers4", { columns: columns, data: users}, true);
 * Exemple ajouter une rangée :
 *      $("#table").DataTable().row.add(rowData).draw();
 * Exemple ajouter une rangée et sauter à la page correspondante :
 *      $("#table").DataTable().row.add(rowData).draw().show().draw(false);
 * Exemple ajouter une rangée, sauter à la page correspondante et y adjoindre une classe :
 *      let node = $("#table").DataTable().row.add(rowData).draw().show().draw(false).node();
 *      $(node).addClass("myNewRow");
 *  Si data est absent, les donnees de la table devront etre ds le <tbody>.
 *  Si selectrow vaut true on peut sélectionner une ligne, mais aussi si selectrow est une fonction, elle sera
 *  alors appelée à chaque sélection d'une ligne avec un tableau contenant les colonnes de la ligne ainsi qu'avec
 *  l'objet tr correspondant, tjrs pratique.
 *  Si cyoptions.multiple = true, on peut sélectionner plusieurs lignes avec <CTRL>.
 *  Si complete est présent, cette fonction sera appelée lorsque la table est prête (en même temps que initComplete)
 *  ClassDataTable pas obligatoire mais utile pour l'avenir
 *  Retourne l'objet DataTable.
 */
function CYDataTable(id, cyoptions, selectrow, complete) {
    if (id.indexOf('#') != 0)
        id = '#' + id;
    debug && console.log('CYJS : CYDataTable ' + id);
    if (typeof cyoptions != "object")
        var cyoptions = {};
    // Table deja créée
    if ($.fn.dataTable.isDataTable(id)) {
        debug && console.log('CYJS : CYDataTable destroying ' + id);
        // Le off est nécessaire sinon les anciens selectrow sont appelés lors
        // de la sélection d'une rangée !
        $(id).off('click', 'tbody tr');
        $(id).DataTable().clear().destroy();
        $(id).find(".ClassDataTableFilter,.ClassDataTableAutoSelect").remove();
    }
    // Options default
    var options = {};
    Object.assign(options, cyoptions);
    var table = $(id).DataTable(options);
    // Selection rangee
    if (selectrow != undefined && selectrow != false && selectrow != null && selectrow != '') {
        $(id).on('click', 'tbody tr', function (event) {
            event.stopPropagation();
            // Appui sur CTRL pour selection multiple
            if (!event.ctrlKey || cyoptions.multiple == false)
                $(id).find('tbody > tr').removeClass('selected');
            $(this).toggleClass('selected');
            if (typeof selectrow == 'function') {
                // Tip : selectedRows = table.rows('.selected').data();
                var row = table.row(this).data();
                // Dans selectrow() :
                // this designe la ligne <tr>, row les data de la ligne, table l'objet DataTable
                // Pour modifier une colonne de la row : row.nomColonne = "Valeur",
                // puis màj par : table.row(tr).data(row).draw(false) (false permet de rester sur la page courante, rien
                // repositionne à la première page)
                // Note : row peut aussi être ainsi déterminée : table.row(this).data() où
                // this est le 2ème argument dans selectrow()
                // Exemple pour ajouter une classe à la ligne sélectionnée : $(this).addClass("myselection");
                // Exemple pour enlever une classe de toutes les lignes : table.$('tr.myselection').removeClass('myselection')
                selectrow(row, this, table);
            }
        });
    }
    // Table prête
    $(id).on("init.dt", function () {
        // Nettoie/adapte le input filter
        $(id + "_filter > label > input").unwrap().attr("type", "text").attr("id", id.substring(1) + "_filterinput").wrap('<div class="ClassInput" data-clear="true" data-icon="fa-search"></div>');
        CYInput(id + "_filter");
        // Ajoute un bouton RAZ si filtre search
        if ($(id).data('searching') == true && !$(id + '_filter button').length) {
            $(id + '_filter').append('<button class="ClassButton" title="' + CYTranslateString('RESET ALL FILTERS') + '"><i class="fa fa-eraser"></i></button>');
            // Reset de tous les filtres
            $(id + '_filter button').CYTooltip().on('click', function (event) {
                event.stopImmediatePropagation();
                $(id).DataTable().search('').draw();
                $(id).prev('.dataTables_filter').find('input').val('');
                $(id).DataTable().search('').columns().search('').draw();
                $(id).find('.ClassDataTableFilter,.ClassDataTableAutoSelect').val('');
            });
        }
        // Complete callback optionnel
        if (typeof complete == 'function')
            complete(table, id);
    }).DataTable();
    return (table);
}
/*
 * Clic ligne
 * Row envoyée dans callback avec l'objet représentant le tr
 */
function CYDataTableSelectLine(id, callback) {
    if (id.indexOf('#') != 0)
        id = '#' + id;
    debug && console.log('CYJS : CYDataTableSelectLine ' + id + '"');
    $(id).on('click', 'tbody tr', function (event) {
        event.stopPropagation();
        // contenu de tte la rangée (correspond aux colonnes, ex : row.id)
        var row = $(id).DataTable().row($(this)).data();
        callback(row, this);
    });
}
/*
 * Clic cellule
 * Cellule envoyée dans callback avec l'objet représentant le td
 */
function CYDataTableSelectCell(id, callback) {
    if (id.indexOf('#') != 0)
        id = '#' + id;
    debug && console.log('CYJS : CYDataTableSelectCell ' + id + '"');
    $(id).on('click', 'tbody td', function (event) {
        event.stopPropagation();
        // contenu de la colonne
        var cell = $(id).DataTable().cell(this).data();
        var row = $(id).DataTable().cell(this).index().row;
        var col = $(id).DataTable().cell(this).index().column;
        callback(cell, row, col, this);
    });
}
/*
 * Double clic champ
 * table est retourné par CYDataTable()
 */
function CYDataTableEditCell(table, callback) {
    /// Devrait pas arriver mais deja constaté..:(
    if (table == undefined)
        return;
    // Recherche l'id retourné par CYDataTable()
    id = '#' + table.context[0].sTableId;
    $(id + ' tbody').on('dblclick', 'td', function () {
        if (!$(this).hasClass("ClassTDEditedCell")) {
            // Il peut y avoir des cas où la dernière cellule éditée est encore présente
            // notamment si on a fait Esc dans un datepicker
            $(id + ' tbody').find(".ClassTDEditedCell").each(function () {
                var td = this;
                $(this).removeClass("ClassTDEditedCell");
                $(this).find("input,textarea").each(function () {
                    $(td).text($(this).val());
                });
            });
            var thiscell = this;
            // contenu td (valeur)
            var oldvalue = table.cell(thiscell).data();
            // contenu de tte la rangée (correspond au colonne, ex : row.id)
            var row = table.row($(thiscell).parent()).data();
            // numero de colonne dans l'objet columns fourni à CYDataTable()
            var colonne = table.cell(thiscell).index().column;
            // Eventuel ttribut sur le th à remettre sur le td contenant l'input
            var dataclear = $(table.column(colonne).header()).attr('data-clear');
            if (dataclear != undefined && dataclear != '')
                $(thiscell).attr('data-clear', dataclear);
            // Attribut readonly sur le th correspondant ?
            var readonly = $(table.column(colonne).header()).attr('readonly');
            if (readonly != undefined)
                return;
            // td contenant data actuelle
            $(thiscell).addClass("ClassTDEditedCell");
            // Eventuels d'attributs sur le th correspondant à remettre sur le input
            var datamaxlength = $(table.column(colonne).header()).attr('data-maxlength');
            var datamask = $(table.column(colonne).header()).attr('data-mask');
            var datadatepicker = $(table.column(colonne).header()).attr('datepicker');
            var datadateformat = $(table.column(colonne).header()).attr('data-format');
            var datatype = $(table.column(colonne).header()).attr('data-type');
            // Input à positionner en place de la valeur
            if (datatype == undefined || datatype == '' || datatype == 'input') {
                var inputdata = '<input id="IDeditedcell" value="' + oldvalue + '" type="text"';
                if (datamaxlength != undefined && datamaxlength != '')
                    inputdata += ' maxlength="' + datamaxlength + '"';
                if (datamask != undefined && datamask != '')
                    inputdata += ' data-mask="' + datamask + '"';
                if (datadatepicker != undefined && datadatepicker != '') {
                    inputdata += ' datepicker="' + datadatepicker + '"';
                    if (datadateformat != undefined && datadateformat != '')
                        inputdata += ' data-format="' + datadateformat + '"';
                }
                inputdata += '>';
            }
            // Textarea à positionner
            else {
                var inputdata = '<textarea id="IDeditedcell"';
                if (datamaxlength != undefined && datamaxlength != '')
                    inputdata += ' maxlength="' + datamaxlength + '"';
                inputdata += '>' + oldvalue + '</textarea>';
            }
            // td contient now un input ou un textarea
            table.cell(thiscell).data(inputdata).draw('page');
            if (datadatepicker != undefined && datadatepicker != '') {
                CYDatePicker("#IDeditedcell", '', function (formatteddate) {
                    // Si une date a été selected
                    $("#IDeditedcell").off("focusout");
                    var newvalue = formatteddate;
                    $(thiscell).removeClass("ClassTDEditedCell");
                    if (oldvalue != newvalue) {
                        table.cell(thiscell).data(newvalue);
                        // callback doit mettre à jour la BD et doit appeler la fct avec la valeur finale
                        var v1 = callback(newvalue, oldvalue, colonne, row, function (v1) {
                            table.cell(thiscell).data(v1).draw();
                        });
                    } else
                        table.cell(thiscell).data(oldvalue).draw('page');
                });
            } else
                CYInput(".ClassTDEditedCell");
            $("#IDeditedcell").focus();
            if (datadatepicker == undefined || datadatepicker == '') {
                // Si on clique ailleurs ou si ENTER ou ESC
                $("#IDeditedcell").on("focusout keyup", function (event) {
                    // Validation par ENTER ou clic ailleurs
                    if (event.type == 'focusout' || event.keyCode == 13) {
                        $("#IDeditedcell").off("focusout");
                        var newvalue = $("#IDeditedcell").val().trim();
                        $(thiscell).removeClass("ClassTDEditedCell");
                        if (oldvalue != newvalue) {
                            table.cell(thiscell).data(newvalue);
                            // callback doit mettre à jour la BD et doit appeler la fct avec la valeur finale
                            var v1 = callback(newvalue, oldvalue, colonne, row, function (v1) {
                                table.cell(thiscell).data(v1).draw();
                            });
                        } else
                            table.cell(thiscell).data(oldvalue).draw('page');
                    }
                    // Retour ancienne valeur si ESC
                    else if (event.keyCode == 27) {
                        $("#IDeditedcell").off("focusout");
                        $(thiscell).removeClass("ClassTDEditedCell");
                        table.cell(thiscell).data(oldvalue).draw('page');
                    }
                });
            }
        }
    });
}
/*
 * Tooltip sur les title
 * Crée la méthode jQuery CYTooltip, permet de faire $(id).CYTooltip()
 * Les options prédéfinies seront utilisées, on pourra toujours faire $(id).CYTooltip(autreOptions) pour forcer autre chose.
 * ATT : id peut aussi etre une classe, donc ne pas automatiser un # devant id
 */
function CYTooltip(id, cyoptions) {
    var timer;

    if (cyIsmobile)
        return;
    //debug && console.log('CYJS : CYTooltip ' + (typeof (id) == "string") ? id : "");
    if (id == undefined || id == '')
        var id = "*";
    var options = {
        track: true,
        show: {
            effect: "fadeIn",
            delay: 500
        },
        hide: {
            effect: "fadeOut",
            delay: 500
        },
        position: {
            my: "center+10 bottom-20",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>").addClass("ClassTooltipArrow").addClass(feedback.vertical).addClass(feedback.horizontal).appendTo(this);
            }
        },
        content: function () {
            // Permet d'afficher du HTML dans un tooltip
            return $(this).prop('title');
        },
        open: function (event, ui) {
            if (CYAtoI($(".ui-tooltip").css("left")) < 0)
                $(".ui-tooltip").css("left", "0");
            timer = setTimeout(function () {
                $(".ui-tooltip").hide('fadeout');
            }, cyTooltipDuration);
            $('.ui-tooltip').mouseleave(function () {
                clearTimeout(timer);
            });
        },
        close: function (event, ui) {
            clearTimeout(timer);
            // Merdes inutiles laissées par tooltip
            $(".ui-helper-hidden-accessible[role=log]").remove();
        }
    };
    if (typeof cyoptions == "object")
        Object.assign(options, cyoptions);
    $(id).findall("[title],[ui-dialog-title]").tooltip(options);
}
/*
 * Applique traitements aux input
 * Un input doit etre défini ainsi pour être parfaitement affiché :
 *    <div class="ClassInput" data-clear='true'>
 *        <input id="IDinput" value="xxx" maxlength="15" type="text">
 *    </div>
 * id permet de restreindre l'application à l'id et à sa descendance
 * On peut masquer la saisie avec data-mask="Regex" (permet de limiter les saisies au clavier)
 * S'il y a un data-mask, on peut forcer la casse de la variable  avec data-case="lowercase" ou "uppercase" ou "uppercasefirst"
 * Sinon les attributs data-inputmask-XXXX seront utilisés
 * 
 * Exemple pour saisir un numero de telephone, les espaces seront affiches en saisie mais pas consideres
 * grace a autoUnmask :
 * <input id="IDphone" name="IDphone" value="" data-inputmask="'mask': '99 99 99 99 99', 'autoUnmask' : 'true'">
 * Exemple pour saisir un nom en majuscules, commencant par une lettre, 
 * et acceptant les espaces et apostrophes non consecutifs :
 * <input id="IDNom" name="IDNom" value="" maxlength="30" data-inputmask-casing="upper" data-mask="^[a-zA-Z]+([- ']{1}[a-zA-Z]+)*[a-zA-Z]*$">
 * Exemple pour saisir un numero de securite sociale, commencant par 1 ou 2 puis 12 chiffres, le autoUnmask
 * permet d'enlever les espaces :
 * <input id="IDNumeroSecu" name="IDNumeroSecu" value="" data-inputmask="'mask': '9 99 99 99 999 999', 'autoUnmask' : 'true'">
 * ATTENTION, ceci est bogué, le '1|2' fonctionne bien à la saisie mais disparait après .. :(
 *   <input id="inp" name="inp" data-inputmask="'mask': '1|2 99 99 99 999 999', 'autoUnmask' : 'true'">
 *
 * L'attribut data-autonext="ID" permet d'automatiquement passer au champd ID
 * lorsque le masque de saisie sera satisfait.
 * 
 * Info : donner les particularites de button à input
 *    $('input:text, input:password').button().css({
 *        'font' : 'inherit',
 *        'color' : 'inherit',
 *        'text-align' : 'left',
 *        'outline' : 'none',
 *        'cursor' : 'text'
 *    });
 */
function CYInput(id) {
    debug && console.log('CYJS : CYInput id=' + JSON.stringify(id));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassInput");
    else
        var ids = $(id).findall(".ClassInput,.ClassTDEditedCell");
    ids.not(".ClassReady").each(function () {
        var classdivinput = $(this);
        // Gere class data-icon="fa-xxxxxxxx" pour ajouter une icone à gauche
        if (classdivinput.data("icon") !== undefined) {
            var icon = classdivinput.data('icon');
            classdivinput.addClass("ClassInputIcon");
            classdivinput.prepend('<i class="ClassInputIconI ' + CYFontAwesomeIconName(icon) + '"></i>');
        }
        // Gere AFFICHAGE class 'data-clear' pour effacer le champ input
        if (($(this).data("clear") == '1' || $(this).data("clear") == 'true'))
            var clearok = true;
        else
            var clearok = false;
        var input = classdivinput.find("input,textarea");
        input.each(function () {
            $(this).off("focus").on("focus", function () {
                var inputactif = this;
                // Efface input actif et icone clear
                $("input,textarea").parents(".ClassInput").removeClass('ClassInputActive');
                $("i.data-clear").css('visibility', 'hidden');
                // Active input
                $(inputactif).parents(".ClassInput").addClass('ClassInputActive');
                // Active icone clear
                if (clearok) {
                    var iconeclear = $(inputactif).next('i.data-clear');
                    classdivinput.addClass("ClassClearIcon");
                    iconeclear.css('visibility', 'visible');
                    iconeclear.one("click", function (event) {
                        $(inputactif).val('').focus();
                        event.stopPropagation();
                        $(inputactif).change();
                        $(inputactif).trigger("input");
                    });
                }
                // Perte focus -> input inactif
                // Bug JS !!! Un time-out minimal est nécessaire sinon le clic initial qui a provoqué l'input est pris en compte
                setTimeout(function () {
                    $(document).one("click", function (e) {
                        var target = e.target.nodeName;
                        if ($(e.target).attr('id') == $(inputactif).attr('id'))
                            return;
                        $(inputactif).parents(".ClassInput").removeClass('ClassInputActive');
                        $(inputactif).next("i.data-clear").css('visibility', 'hidden');
                    });
                }, 500);
            });
            // Controles de saisie
            var attr = $(this).data('mask');
            var that = this;
            if (attr != undefined) {
                $(this).inputmask({
                    regex: attr,
                    clearIncomplete: true,
                    placeholder: '',
                    autoUnmask: true,
                    onincomplete: function (e) {
                        (debug & DEBUG_MORE) && console.log("CYJS : CYInput inputmask onincomplete1 [" + $(that).val() + "]");
                    },
                    // Appelée lorsque la saisie satisfait le masque
                    // A priori inutile avec derniere maj inputmask en utilisant
                    // l'attribut data-inputmask-casing="upper" ou "lower" ou "title"
                    oncomplete: function (e) {
                        (debug & DEBUG_MORE) && console.log("CYJS : CYInput inputmask oncomplete1 [" + $(that).val() + "]");
                        var casse = $(that).data('case');
                        if (casse != undefined) {
                            if (casse.toUpperCase() == "UPPERCASE")
                                $(that).val(e.target.value.toUpperCase());
                            else if (casse.toUpperCase() == "LOWERCASE")
                                $(that).val(e.target.value.toLowerCase());
                            else if (casse.toUpperCase() == "UPPERCASEFIRST") {
                                let v = e.target.value.toLowerCase();
                                $(that).val(v.charAt(0).toUpperCase() + v.slice(1));
                            }
                        }
                        if ($(that).data('autonext') != undefined)
                            $(that).next($(that).data('autonext')).focus();
                    }
                });
            } else
                $(this).inputmask({
                    clearIncomplete: true,
                    autoUnmask: true,
                    onincomplete: function (e) {
                        (debug & DEBUG_MORE) && console.log("CYJS : CYInput inputmask onincomplete2 [" + $(that).val() + "]");
                    },
                    oncomplete: function (e) {
                        (debug & DEBUG_MORE) && console.log("CYJS : CYInput inputmask oncomplete2 [" + $(that).val() + "]");
                        var next = $(that).data('autonext');
                        if (next != undefined) {
                            $("#" + next).focus();
                        }
                    }
                });
        });
        if (clearok)
            classdivinput.append('<i class="ClassClearIconI data-clear fa fa-times-circle-o" style="visibility: hidden"></i>');
        classdivinput.addClass("ClassInput ClassReady");
    });
}
/*
 * Lorsqu'un formulaire possède des input utilisant des inputmask avec des masques de saisies, les masques
 * ne sont effectivement PAS appliqués dans les données du FormData et peuvent donc être différents du $("#id") correspondant.
 * Donc si on veut poster les data du formulaire 'masquées', il faut leur appliquer le masque.
 * 
 * Explication :
 * <form id="form" name="form">
 *   <input id="inp" name="inp" data-inputmask="'mask': '9 99 99 99 999 999', 'autoUnmask' : 'true'">
 * </form>
 * Si l'user saisit "163026748214", le mask va permettre d'afficher "1 63 02 67 482 143"
 * Si 'autoUnmask' = 'false', alors $("#inp").val() vaudra "1 63 02 67 482 143"
 * Si 'autoUnmask' = 'true', alors $("#inp").val() vaudra "1630267482143" , mais le souci est que dans
 * ce cas FormData va conserver la valeur "1 63 02 67 482 143"
 *    => fd = new FormData(document.getElementById('form'));
 *    => fd.get("inp") va valoir"1 63 02 67 482 143" (incohérence totale....)
 * Cette fonction permet de balayer toutes les zones input du formulaire utilisant inputmask
 * trim = true par defaut, préciser false pour ne par 'trimer' les input
 */
function CYFormPostInput(idform, trim) {
    var fd = new FormData(document.getElementById(idform));
    (debug & DEBUG_MORE) && console.log("CYJS : CYPostForm idform '" + idform + "'");
    // values[0] est le nom de l'input, values[1] sa valeur brute (non masquées)
    for (var values of fd.entries()) {
        let id = "#" + values[0];
        if ($(id).attr("data-inputmask") != undefined || $(id).attr("data-input-mask") != undefined || $(id).attr("data-mask") != undefined) {
            // val est la valeur masquées de l'input
            let val = $(id).val();
            // Les espaces sont tjrs chiants ..
            if (trim == undefined || trim == true)
                val = val.trim();
            if (val != values[1]) {
                (debug & DEBUG_MORE) && console.log("CYJS : CYPostForm id " + id + " = '" + values[1] + "' => '" + val + "'");
                fd.set(values[0], val);
            }
        }
    }
    return fd;
}
/*
 *  Traite toutes les formes de boutons
 *      <button data-label="Lab" ...>..</button>
 *  les attributs seront affectés aux options
 *  Pour set un button :
 *    $("#bouton").CYButton("set");
 *  Pour unset un button :
 *    $("#bouton").CYButton("unset");
 *  Pour tester un button (si le sélecteur en désigne plusieurs,
 *  le retour sera true si l'un d'entre eux est set) :
 *    $("#bouton").CYButton("get"); -> true / false
 *  cf. http://api.jqueryui.com/button/
 */
function CYButton(id, cyoptions) {
    debug && console.log('CYJS : CYButton id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassButton,input[type=submit],input[type=button],a[type=button],button");
    else
        var ids = $(id).findall(".ClassButton,input[type=submit],input[type=button],a[type=button],button");
    // Pour activer ou désactiver un bouton
    if (typeof cyoptions == 'string') {
        let ret = true;
        if (cyoptions == 'set')
            ids.findall(".ClassReady").addClass("ui-state-active");
        else if (cyoptions == 'unset')
            ids.findall(".ClassReady").removeClass("ui-state-active");
        else if (cyoptions == 'get') {
            ret = ids.findall(".ClassReady").each(function () {
                ret = $(this).hasClass("ui-state-active");
            });
        }
        return ret;
    }
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var options = {};
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYButton id="' + $(this).attr('id') + '" ' + CYDumpObj(options));
        $(this).button(options);
        $(this).addClass("ClassButton ClassReady");
    });
}
/*
 * Traite toutes les formes de checkbox(classe ClassCheckbox)
 *      <input type="checkbox" data-icon="false" data-label="Label" ...>
 * les attributs seront affectés aux options.
 * IL FAUT UN id, value non nécessaire.
 * Exemple :
 * <fieldset>
 *   <legend>Zoom</legend>
 *   <label for="pcXL1">1/1</label>
 *   <input class="ClassCheckbox" type="radio" name="pcXL" id="pcXL1">
 *   <label for="pcXL2">XL</label>
 *   <input class="ClassCheckbox" type="radio" name="pcXL" id="pcXL2">
 *   <label for="pcXL3">Mini</label>
 *   <input class="ClassCheckbox" type="radio" name="pcXL" id="pcXL3">
 * </fieldset>
 * Get value d'une checkboxradio :
 *   $("input:radio[name=pcXL]:checked").attr('id')
 * Uncheck all d'une checkboxradio :
 *   $("input:radio[name=pcXL]").prop("checked", false).button("refresh");
 * Set value d'une checkboxradio : 
 *   $("#pcXL3").prop("checked", true).button("refresh");
 * cf. http://api.jqueryui.com/checkboxradio/
 */
function CYCheckBox(id, cyoptions) {
    debug && console.log('CYJS : CYCheckBox id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassCheckbox,input[type=checkbox],input[type=radio]");
    else
        var ids = $(id).findall(".ClassCheckbox,input[type=checkbox],input[type=radio]");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").not(".ClassFlipSwitch").each(function () {
        var options = {};
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYCheckBox id="' + $(this).attr('id') + '" ' + CYDumpObj(options));
        $(this).checkboxradio(options);
        $(this).addClass("ClassCheckbox ClassReady");
    });
}
/*
 *  Traite les spinner identifié par id du type :
 *      <input type="spinner" data-step="1" data-min="0" data-max="10" ...>
 *  ou
 *      <input class="ClassSpinner" data-step="1" data-min="0" data-max="10" ...>
 *  les attributs seront affectés aux options { "step":"1", "min": "0", "max": "10" }
 *  cf. http://api.jqueryui.com/spinner/
 */
function CYSpinner(id, cyoptions) {
    debug && console.log('CYJS : CYSpinner id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassSpinner,input[type=spinner]");
    else
        var ids = $(id).findall(".ClassSpinner,input[type=spinner]");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var options = {};
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYSpinner id="' + $(this).attr('id') + '" ' + CYDumpObj(options));
        $(this).spinner(options);
        $(this).addClass("ClassSpinner ClassReady");
    });
}
/*
 * Traite les select identifié par class="ClassSelect", limité à id si précisé.
 * Si id absent, parse tout le document
 * Les attributs seront affectés aux options { "delay":"150", "tolerance": "fit" }
 * cf. http://api.jqueryui.com/select
 * Note : pour effacer le champ :
 *      $("#id .ui-selectmenu-text").html('');
 */
function CYSelect(id, cyoptions) {
    debug && console.log('CYJS : CYSelect id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassSelect");
    else
        var ids = $(id).findall(".ClassSelect");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var options = {
            width: $(this).attr("width"),
            icons: {
                button: "ui-icon-triangle-1-s"
            },
            open: function (event) {
                $(".ui-selectmenu-open").css("z-index", CYMaxZindex() + 1);
            }
        }
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYSelect id=' + $(this).attr('id') + ' options=' + JSON.stringify(options));
        if (typeof $(this).selectmenu("instance") == "object")
            $(this).selectmenu("destroy");
        $(this).selectmenu(options);
        $(this).addClass("ClassSelect ClassReady");
    });
}
/*
 * Traite les "ClassSortable", limité à id si précisé.
 * Si id absent, parse tout le document
 * cf. http://api.jqueryui.com/sortable
 */
function CYSortable(id, cyoptions) {
    debug && console.log('CYJS : CYSortable id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassSortable");
    else
        var ids = $(id).findall(".ClassSortable");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var options = {
            items: ".ClassSort",
            placeholder: "ClassSortPlaceholder",
            opacity: 0.5,
            handle: false,
            start: function (e, ui) {
                debug && console.log('CYJS : CYSortable start');
                ui.placeholder.height(ui.helper.outerHeight());
                ui.placeholder.width(ui.helper.outerWidth());
            }
        };
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYSortable id=' + $(this).attr('id') + ' options=' + JSON.stringify(options));
        if (typeof $(this).sortable("instance") == "object")
            $(this).sortable("destroy");
        $(this).sortable(options);
        $(this).addClass("ClassSortable ClassReady");
    });
}
/*
 * Traite les select identifié par class="ClassSelectable", limité à id si précisé.
 * Si id absent, parse tout le document
 * Les attributs seront affectés aux options { "delay":"150", "tolerance": "fit" }
 * cyoptions peut être appelée avec différentes options :
 * CYSelectable("#selectable", {
 *      // Permet d'effectuer une action lors de sélections
 *      select: function(selectableobj, selection) {
 *          // Cette fonction permet d'afficher les li sélectionnées sous forme de boutons
 *          // avec la possibilité de supprimer chacun
 *          CYSelectableButtonsPool(selectableobj, "#selectedSociete", selection, {
 *              remove: true,
 *              onRemove: function(selected) {
 *                  console.log("REMOVE id=" + selected.id + " html=" + selected.html);
 *              }
 *          });
 *      }
 *      onlyone: true // pour sélection multiple, false par défaut)
 *      position: "under" // ou over ou before ou after: positionnement relatif à positionFrom
 *                         par défaut : under
 *      positionFrom: "#id" (référence pour positionnement : par défaut selon élément précédent, il faut le préciser
 *                     si rien avant),
 *      initDefaults: ["id1", "id4"]  // ou juste "id1" : permet d'initialiser une sélection
 *                                    // ou id4 est l'id du li à sélectionner
 * }
 * ou bien
 * $("#selectable").CYSelectable({
 *       select: ........
 * })
 * 
 * 2 moyens de récupérer la sélection:
 * - La liste des ids sélectionnés sera restituée via:
 *      $("#selectable").CYSelectable("get")
 * - Pour accéder à plus d'info que les id sélectionnés, tous les objets sélectionnés
 * seront également stockés dans:
 *      $("#selectable").data("selection")
 * On y trouvera un array:
 *   [  { index: 5, id: 'ID5', html: 'Option 5' },{ index: 7, id: 'ID7', html: 'Option 7' }  ... ]
 *   ou index est la position d'ordre du li, id est son id, html le contenu du li
 *
 * Pour resetter :
 *      $("#selectable").CYSelectable("reset")
 * 
 * Côté HTML, on peut afficher le sélecteur indéfiniment :
 * <div class="ClassCell">
 *   <label for="selectable">Multiselect</span></label>
 *   <ol class="ClassSelectable ClassBorder" id="selectable">
 *      <li class="ui-widget-content" id="id1">Peugeot</li>
 *      <li class="ui-widget-content" id="id2">Citroen</li>
 *      <li class="ui-widget-content" id="id3">Renault</li>
 *    </ol>
 * </div>
 * Ou l'ouvrir à la demande:
 * <div class="ClassCell">
 *   <label for="selectable">Multiselect</span></label>
 *   <ol class="ClassSelectable ClassBorder" id="selectable" style="display: none">
 *      <li class="ui-widget-content" id="id1">Peugeot</li>
 *      <li class="ui-widget-content" id="id2">Citroen</li>
 *      <li class="ui-widget-content" id="id3">Renault</li>
 *    </ol>
 * </div>
 * avec du code JS pour afficher au clic:
 *   $("#selectable").click(function(event) {
 *      event.stopPropagation();
 *      $('.ui-selectable-helper').remove();
 *      let selectable = $(this).next('.ClassSelectable');
 *      if ($(selectable).css("display") == "none") {
 *          $(".ClassSelectable").hide();
 *          $(selectable).show().CYToFront();
 *          $(document).mouseup(dummy => $(".ClassSelectable").hide());
 *      } else
 *          $(selectable).hide();
 *   });
 * 
 *  cf. http://api.jqueryui.com/selectable
 */
function CYSelectable(id, cyoptions) {
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassSelectable");
    else
        var ids = $(id).findall(".ClassSelectable");
    let positionSelectable = function (selectableObj, position, positionFrom) {
        let pos, width, height;
        if (position == "after") {
            pos = $(positionFrom).position();
            width = $(positionFrom).outerWidth();
            $(selectableObj).css({ "position": "absolute", "top": pos.top + "px", "left": pos.left + width + "px", "width": "fit-content" });
        }
        else if (position == "before") {
            pos = $(positionFrom).position();
            width = $(positionFrom).outerWidth();
            $(selectableObj).css({ "position": "absolute", "top": pos.top + "px", "right": width + "px", "width": "fit-content" });
        }
        else if (position == "over") {
            pos = $(positionFrom).position();
            $(selectableObj).css({ "position": "absolute", "top": pos.top + "px", "left": pos.left + "px", "width": "fit-content" });
        }
        else { // under par defaut
            pos = $(positionFrom).position();
            height = $(positionFrom).outerHeight();
            $(selectableObj).css({ "position": "absolute", "top": pos.top + height + "px", "left": pos.left + "px", "width": "fit-content" });
        }
        (debug & DEBUG_MORE) && console.log('CYJS : CYSelectable refresh position=' + position + ' positionFrom=' + positionFrom + ' id=' + id + " pos=" + JSON.stringify(pos) + " width=" + width + " height=" + height);
        //$(selectableObj).CYReCenter();
    }
    // Il faut récupérer la liste des li sélectionnés
    if (typeof cyoptions == 'string' && (cyoptions == 'get' || cyoptions == 'reset' || cyoptions == 'refresh')) {
        let selectedids = [];
        ids.findall(".ClassReady").each(function () {
            let that = this;
            // Class ui-selected is set when a li is selected
            $(this).find(".ui-selected").each(function () {
                let id = $(this).attr('id');
                selectedids.push(id);
            });
            if (cyoptions == 'reset') {
                $(this).find(".ui-selected").each(function () {
                    let id = $(this).attr('id');
                    selectedids.push(id);
                    $(this).removeClass("ui-selected");
                });
            }
            else if (cyoptions == 'refresh') {
                // jQuery-UI stocke les data-attributs en data()
                positionSelectable(that, $(that).data('position'), $(that).data('positionFrom'));
            }
        });
        debug && console.log("CYJS : CYSelectable " + cyoptions + ' ' + ids.attr('id') + ' ' + JSON.stringify(selectedids));
        return selectedids;
    }
    if (typeof cyoptions != "object")
        var cyoptions = { onlyone: false };
    ids.not(".ClassReady").each(function () {
        var that = this;
        $(that).addClass("ClassSelectable ClassReady");
        // Default options
        var options = {
            create: function (event, ui) {
                debug && console.log('CYJS : CYSelectable create id=' + $(this).attr('id'));
            },
            open: function (event, ui) {
                debug && console.log('CYJS : CYSelectable open id=' + $(this).attr('id'));
            },
            start: function (event, ui) {
                debug && console.log('CYJS : CYSelectable start id=' + $(this).attr('id'));
            },
            selecting: function (event, ui) {
                debug && console.log('CYJS : CYSelectable selecting id=' + $(this).attr('id'));
                if (cyoptions.onlyone == true) {
                    // Force single selection
                    $(event.target).find('.ui-selectee.ui-selecting').not(ui.selecting).removeClass('ui-selecting');
                    $(event.target).find('.ui-selectee.ui-selected').not(ui.selecting).removeClass('ui-selected');
                }
            },
            stop: function (event) {
                debug && console.log('CYJS : CYSelectable stop id=' + $(this).attr('id'));
                let selection = [];
                // Class ui-selected is set when a li is selected
                $(this).find(".ui-selected").each(function () {
                    let index = $(this).index();
                    let id = $(this).attr('id');
                    let html = $(this).html();
                    selection.push({ "index": index, "id": id, "html": html });
                });
                $(that).data("selection", selection);
                if (typeof cyoptions.select == 'function')
                    cyoptions.select(this, selection);
            },
            positionFrom: $(that).prev()[0],
            position: 'under'
        }
        let pos, width, height;
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(that)));
        debug && console.log('CYJS : CYSelectable id=' + $(that).attr('id') + " options=" + JSON.stringify(options));
        if (typeof $(that).selectable("instance") == "object")
            $(that).selectable("destroy");
        $(that).selectable(options);
        positionSelectable(that, options.position, options.positionFrom);
        // Il faut prédéfinir des valeurs
        if (options.initDefaults != undefined && typeof (options.select) == "function") {
            // Si options.initDefaults n'est pas un tableau, je le transforme en
            // tableau d'1 élément
            if (typeof (options.initDefaults) != "object") {
                let o = [];
                o.push(options.initDefaults);
                options.initDefaults = o;
            }
            debug && console.log('CYJS : CYSelectable initDefaults ' + JSON.stringify(options.initDefaults));
            let selection = [];
            // Purge les éventuels sélections
            $(that).find("li").each(function () {
                $(this).removeClass("ui-selected");
            });
            $(that).removeData("selection");
            // Pour chaque id à positionner par défaut,
            options.initDefaults.forEach(id => {
                if (id == undefined || id == '')
                    return;
                // je recherche le li qui lui correspond
                let li = $(that).find("li[id=" + id + "]");
                // et seulement s'il existe
                if (li != undefined && li.attr('id') != undefined) {
                    // je le sélectionne
                    li.addClass("ui-selected");
                    // et je prépare son bouton pour la fonction init
                    selection.push({ "index": li.index(), "id": id, "html": li.html() });
                }
            });
            if (selection.length) {
                $(that).data("selection", selection);
                options.select(that, selection);
            }
        }
    });
}
/* 
 *  Ajout de boutons dans un Selectable
 *      selectableobj : objet dans lequel se trouve la liste complète des li à sélectionner, chaque li DOIT AVOIR un id !!!
 *      selectedli : id dans lequel il faut créer les boutons, chaque bouton correspondant à un li de selectableobj
 *      selectionfromselectableobj : array contenant la sélection [{ "index": index, "id": id, "html": html}, ...]
 *      cyoptions: {
 *          remove : true // Permet d'afficher un x pour suppression
 *          minSelected: 1 // Il faut au moins 1 selection sinon suppression impossible
 *          // onRemove sera appelée si click sur la croix remove. Si retour de
 *          // la valeur CYSELECTABLEDONTREMOVE alors la suppression ne se fera pas
 *          onRemove : function(selected) {
 *              console.log("Remove id=" + selected.id + " html=" + selected.html);
 *              if(qqechose)
 *                  return CYSELECTABLEDONTREMOVE;
 *              return true;
 *          }
 *      }
 *  Usage typique : 
 *      <div class="ClassCell" style="width: 450px; padding: 5px">
 *          <label for="selectedAgences">Agences</label>
 *          <div class="ClassCell ClassBorder" id="selectedAgences" style="height: 85%; padding: 5px; overflow-y: auto"></div>
 *              <i class="fa fa-caret-down"></i>
 *          <ul class="ClassCell ClassBorder ClassSelectable" name="listeAgences" id="listeAgences" data-position-from="#selectedAgences">
 *              <li id="1">Choix UN</li>
 *              <li id="2">Choix DEUX</li>
 *          </ul>
 *      </div>   
 *
 *      $("#listeAgences").CYSelectable({
 *          select: function(selectableobj, selection) {
 *              CYSelectableButtonsPool(selectableobj, "#selectedAgences", selection, {
 *                  remove: true,
 *                  onRemove: function(selected) {
 *                      console.log("REMOVE id=" + selected.id + " html=" + selected.html);
 *                  }
 *              });
 *          }
 *      });
 * Reset : CYSelectableButtonsPool(selectableobj, "#selectedAgences") => purge la liste
 */
function CYSelectableButtonsPool(selectableobj, selectedli, selectionfromselectableobj, cyoptions) {
    debug && console.log('CYJS : CYSelectableButtonsPool selectedli=' + selectedli + ' selectionfromselectableobj=' + JSON.stringify(selectionfromselectableobj) + ' cyoptions=' + JSON.stringify(cyoptions));
    // Purge avant reconstruction
    $(selectedli).find("button").remove();
    if (selectionfromselectableobj == undefined || !selectionfromselectableobj.length)
        return;
    if (typeof cyoptions != "object")
        var cyoptions = { remove: false };
    if (cyoptions.minSelected == undefined)
        cyoptions.minSelected = 0;
    for (let i = 0; i < selectionfromselectableobj.length; i++) {
        let remove = '';
        if (cyoptions.remove && (cyoptions.minSelected == 0 || selectionfromselectableobj.length > cyoptions.minSelected))
            remove = '<i class="fa fa-close" style="cursor: pointer" data-id="' + selectionfromselectableobj[i].id + '"></i>';
        $(selectedli).append('<button class="ui-button" style="cursor: default">' + remove + selectionfromselectableobj[i].html + '</button>');
        if (remove != '' && typeof (cyoptions.onRemove) == "function") {
            $(selectedli).find("i[data-id='" + selectionfromselectableobj[i].id + "']").on("click", function (ev) {
                ev.stopPropagation();
                if (cyoptions.onRemove(selectionfromselectableobj[i]) == CYSELECTABLEDONTREMOVE)
                    return false;
                // Supprime l'élément dans la sélection associée à l'objet
                let selection = $(selectableobj).data("selection");
                $(selectableobj).data("selection", selection.removeKey($(this).data("id")));
                // Supprime la ligne sélectionnée dans le select jQuery
                $(selectableobj).find('li[id="' + $(this).data("id") + '"]').removeClass("ui-selected");
                // Supprime le bouton avec sa x
                $(this).parent().remove();
                return true;
            });
        }
    }
}
/*
 * Traite les onglets identifié par class="ClassTab", limité à id si précisé.
 * Si id absent, parse tout le document
 *  les attributs seront affectés aux options { collapsible: true }
 *  cf. http://api.jqueryui.com/select
 */
function CYTab(id, cyoptions) {
    debug && console.log('CYJS : CYTab id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassTab");
    else
        var ids = $(id).findall(".ClassTab");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var options = {};
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYTab id="' + $(this).attr('id') + '" ' + CYDumpObj(options));
        $(this).tabs(options);
        $(this).addClass("ClassReady");
    });
}
/*
 * Traite les accordéons identifié par class="ClassAccordion", limité à id si précisé.
 * Si id absent, parse tout le document
 *  les attributs seront affectés aux options { collapsible: true }
 *  cf. http://api.jqueryui.com/select
 */
function CYAccordion(id, cyoptions) {
    debug && console.log('CYJS : CYAccordion id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    debug && console.log('CYJS : CYAccordion id=' + JSON.stringify(id) + ' cyoptions=' + JSON.stringify(cyoptions));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassAccordion");
    else
        var ids = $(id).findall(".ClassAccordion");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var options = {};
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYAccordion id="' + $(this).attr('id') + '" ' + CYDumpObj(options));
        $(this).accordion(options);
        $(this).addClass("ClassReady");
    });
}
/*
 * Datepicker ou figure attribut datepicker ou bien la classe ClassDatePicker, qui peuvent figurer dans un tag quelconque
 * id permet de restreindre à l'id et à sa descendance
 * cyoptions permet d'outrepasser les options par defaut
 * Pour l'activer, il suffit de spécifier sur l'input l'attribut datepicker, par ex :
 *   <div class="ClassInput ClassInputLast">
 *       <input id="IDladate" class="ClassDatePicker" value="" data-multiple-dates='true' data-position='bottom left' data-min-view="days" data-view="years" data-timepicker="true" data-clear-button="false" data-today-button="true" data-date-format="yyyy-mm-dd" data-time-format="HH:mm">
 *
 *   <div class="ClassInput ClassInputLast">
 *       <input id="IDlheure" class="ClassDatePicker" value="" data-timepickeronly="true" data-clear-button="false">
 *   </div>
 * NOUVEAUTE AU 15/12/17 : $(id).data("date") contiendra la date sélectionnée au format interne JS
 */
function CYDatePicker(id, cyoptions, callbackselect) {
    debug && console.log('CYJS : CYDatePicker id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassDatePicker,[datepicker='true']");
    else
        var ids = $(id).findall(".ClassDatePicker,[datepicker='true']");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    // 15/12/17 : Je ne teste pas ids.not(".ClassReady") ici car je veux pouvoir utiliser callbackselect
    // après avoir fait CYInitAllTools(), et à priori ça ne gêne pas qu'il y ait plusieurs
    // appels successifs sur le même id
    ids.each(function () {
        var that = this;
        var options = {
            language: cyLang,
            todayButton: new Date(),
            clearButton: false,
            selectOtherYears: true,
            showOtherYears: true,
            selectOtherMonths: true,
            showOtherMonths: true,
            autoClose: true,
            view: 'days',
            minView: 'days',
            hoursStep: 1,
            minutesStep: 5,
            //dateFormat: 'dd/mm/yyyy',
            //timeFormat: 'hh:ii',
            //dateTimeSeparator: ' ',
            onSelect: function (formattedDate, date, inst) {
                $(that).data("date", date);
                if (callbackselect != undefined)
                    callbackselect(formattedDate, date, inst);
            },
            onRenderCell: function (date, cellType) {
                /*
                if (cellType != 'day')
                    return;
                let day = date.getDay();
                let feries = ['01/01', '01/05', '08/05', '14/07', '15/08', '01/11', '11/11', '25/12'];
                // Dimanche et jours fériés disabled
                if (day == 0 || ferie.indexOf(moment(date).format('DD/MM')) != -1)
                    return { disabled: true };
                // Pour mettre une pastille sur le 7 Février
                if(moment(date).format('DD/MM') == '07/02')
                    return { html: date.getDate() + '<span class="datepicker-note"></span>' }
                */
            },
            onShow: function(dp, animationCompleted){
                // Permet d'appeler onRenderCell à chaque affichage, et non seulement à chaque
                // changement de date
                if (!animationCompleted)
                    $(that).datepicker().data('datepicker');
                /*
                if (!animationCompleted) {
                    log('start showing')
                } else {
                    log('finished showing')
                }
                */
            },
            onHide: function(dp, animationCompleted){
                /*
                if (!animationCompleted) {
                    log('start hiding')
                } else {
                    log('finished hiding')
                }
                */
            }
        };
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        if ($(this).data("timepickeronly")) {
            options.dateFormat = ' ';
            options.timepicker = true;
            options.todayButton = false;
            options.classes = 'data-timepickeronly';
        }
        debug && console.log('CYJS : CYDatePicker id="' + $(this).attr("id") + '" ' + CYDumpObj(options));
        $(this).datepicker(options).data('datepicker');
        // Option maison pour présélectionner :  selected: Date()
        // Ex : si d = "2017-10-18", alors
        //      selected: new Date(d.substr(0, 4), parseInt(d.substr(5, 2)) - 1, d.substr(8, 2))
        // Ou : selected: "tomorrow" ou "today" ou "yesterday"
        // Ou dans HTML: data-selected="today"
        if (options.selected != undefined) {
            if (options.selected == "today")
                options.selected = new Date();
            else if (options.selected == "tomorrow") {
                var today = new Date();
                options.selected = new Date(today.getTime() + (24 * 60 * 60 * 1000));
            } else if (options.selected == "yesterday") {
                var today = new Date();
                options.selected = new Date(today.getTime() - (24 * 60 * 60 * 1000));
            }
            $(this).datepicker(options).data('datepicker').selectDate(options.selected);
        }
        // Note : $(this).datepicker().data('datepicker').selectDates est un array avec les valeurs
        $(this).addClass("ClassReady");
    });
}
/*
 *  Traitement des flipswitch style IOS. Si id absent, parse "input.ClassFlipSwitch"
 *  Ex :
 *       <input class="ClassFlipSwitch" type="checkbox" id="flipswitch" value="1">
 *  Sera remplacé par :
 *       <label class="ClassFlipSwitch">
 *           <input class="ClassFlipSwitch" type="checkbox" id="flipswitch" value="1">
 *           <--! ou pour un switch en read only : -->
 *           <input class="ClassFlipSwitch" type="checkbox" id="flipswitch" value="1" disabled> 
 *           <div class="ClassFlipSlider ClassFlipRound">
 *           </div>
 *       </label>
 * Ex JS :
 *   set ON :  $("#idflipswitch").val('1').prop("checked", true);
 *   set OFF : $("#idflipswitch").val('0').prop("checked", false);
 * Pour tester :
 *   $("#idflipswitch").is(':checked')) = true | false
 * ou
 *   $("#idflipswitch").val() = 1 | 0
 */
function CYFlipSwitch(id) {
    debug && console.log('CYJS : CYFlipSwitch id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $("input.ClassFlipSwitch");
    else
        var ids = $(id).findall("input.ClassFlipSwitch");
    $(ids).not(".ClassReady").each(function () {
        $(this).wrap('<label class="ClassFlipSwitch"></label>');
        if ($(this).attr('disabled') == 'disabled')
            $(this).after('<div class="ClassFlipSliderDisabled ClassFlipRound"></div>');
        else
            $(this).after('<div class="ClassFlipSlider ClassFlipRound"></div>');
        if ($(this).val() == 1)
            $(this).prop('checked', true);
        else {
            $(this).prop('checked', false);
            $(this).val('0');
        }
        $(this).click(function (event) {
            // Surtout pas ici de event.stopImmediatePropagation() !!!
            debug && console.log('CYJS : CYFlipSwitch id=' + $(this).attr('id') + ' status=' + $(this).is(':checked'));
            if ($(this).is(':checked'))
                $(this).val(1);
            else
                $(this).val(0);
        });
        $(this).addClass("ClassReady");
    });
}
/*
 * Fullcalendar.
 * Remove old data
 *   $(id).fullCalendar('removeEvents');
 * Getting new event json data
 *   $(id).fullCalendar('addEventSource', response);
 * Updating new events
 *   $(id).fullCalendar('rerenderEvents');
 * Getting latest Events
 *   $(id).fullCalendar('refetchEvents');
 * Getting latest Resources
 *   $(id).fullCalendar('refetchResources');
 * Lire les events
 *   $(id).fullCalendar('clientEvents')
 * Tout reinit
 *   $(id).fullCalendar('destroy')
 * https://fullcalendar.io/docs/
 */
function CYCalendar(id, cyoptions) {
    debug && console.log('CYJS : CYCalendar id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassCalendar");
    else
        var ids = $(id).findall(".ClassCalendar");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.each(function () {
        var options = {};
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYCalendar id="' + $(this).attr('id') + '" ' + CYDumpObj(options));
        $(this).fullCalendar(options);
    });
}
/*
 * jstree plugin.
 * https://www.jstree.com/
 */
function CYTree(id, cyoptions) {
    debug && console.log('CYJS : CYTree id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassTree");
    else
        var ids = $(id).findall(".ClassTree");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var options = {
            core: {
                multiple: false,
                animation: 0,
                themes: {
                    variant: false //"large"
                }
            }
        };
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYTree id="' + $(this).attr('id') + '" ' + CYDumpObj(options));
        $(this).jstree(options);
        $(this).addClass("ClassReady");
    });
}
/*
 * Trumbowyg editor.
 * http://alex-d.github.io/Trumbowyg/documentation.html
 */
function CYEditor(id, cyoptions) {
    debug && console.log('CYJS : CYEditor id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassEditor");
    else
        var ids = $(id).findall(".ClassEditor");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var options = {
            disabled: false,
            lang: cyLang,
            semantic: true,
            resetCss: true,
            autogrow: true,
            //fixedBtnPane: true,
            removeformatPasted: true,
            tagsToRemove: ['script', 'link'],
            imageWidthModalEdit: true,
            /*btnsDef:
            {
                custom: {
                    fn: function()
                    {
                        alert('some text')
                    },
                    ico: 'blockquote'
                },
                exampledropdown:
                {
                    dropdown: ['btnA', 'btnB'],
                    title: 'Displayed dropdown button name',
                    ico: 'iconName',
                    hasIcon: true
                }  ,
                examplealign:
                {
                    dropdown: ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                    ico: 'justifyLeft'
                }
            },   */
            btns: [
                ['formatting'],
                ['fontfamily'],
                ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                ['bold', 'italic', 'underline', 'strikethrough'],
                ['fontsize'],
                ['foreColor', 'backColor'],
                ['specialChars'],
                ['preformatted'],
                ['removeformat'],
                ['unorderedList', 'orderedList'],
                ['link'],
                ['insertImage'],
                ['base64'],
                ['emoji'],
                ['table'],
                ['undo', 'redo'],
                ['viewHTML']
                //['custom']
                //['fullscreen']
            ]
        };
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYEditor id="' + $(this).attr("id") + '" ' + CYDumpObj(options));
        $(this).trumbowyg(options);
        $(this).addClass("ClassReady");
    });
}
/*
 * Déclaration d'un panel. Les options peuvent être définies ds cyoptions ou en data-xxxx :
 *   - cyoptions.move ou data-move="true" : panel déplacable
 *   - cyoptions.icon ou data-icon="fa-XXXXXX" : panel iconifiable, icon sera l'icone utilisée (si "fa-" est absent, il sera préfixé)
 *   - cyoptions.iconify ou data-iconify="true" : panel iconifié par défaut
 *   - cyoptions.resize ou data-resize="true" : panel redimensionnable
 *   - cyoptions.nextdivoverflow ou data-nextdivoverflow="true" (true par défaut): le div qui
 *   - cyoptions.maxwidth ou data-maxwidth="4096" : max width pour resizable
 *   - cyoptions.maxheight ou data-maxheight="4096" : max height pour resizable
 *   - cyoptions.ghost ou data-ghost="true" : pour resizable
 *     suit immédiatement, s'il existe, sera stylé pour la gestion auto des scrollbar
 *     (nécessite CSS3)
 *   - cyoptions.magnify ou data-magnify="true" : panel agrandissable dans IDmain
 *   - cyoptions.container : restreint l'icône dans le container précisé, #IDmain par défaut défini dans CYIconify()
 *   Exemple :
 *       <div class="ClassCell" style="border: 1px solid #cccccc;">
 *         <div class="ClassPanel" data-move="true" data-icon="fa-cog">
 *              Moving panel
 *         </div>
 *         <div>
 *              Panel content here ..
 *         </div>
 *       </div>
 */
function CYPanel(id, cyoptions) {
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassPanel");
    else
        var ids = $(id).findall(".ClassPanel");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    debug && console.log('CYJS : CYPanel "' + id + '" ' + JSON.stringify(cyoptions));
    ids.not(".ClassReady").each(function () {
        var options = {
            move: false,
            resize: false,
            magnify: false,
            icon: false,
            nextdivoverflow: true,
            ghost: false,
            handle: '.ClassPanel:first'
        };
        var cellpanel = $(this);
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs(cellpanel));
        var cellwindow = cellpanel.parent();
        debug && console.log('CYJS : CYPanel parent id=' + cellwindow.attr("id") + ' ' + JSON.stringify(options));
        if (cellwindow.hasClass('ClassWindow')) // No loop
            return;
        cellwindow.addClass('ClassWindow');
        // ClassWindow au premier plan si click
        cellwindow.on("mousedown", function () {
            cellwindow.CYToTop(".ui-dialog,.ClassWindow,.ClassIconFile");
        });
        // S'il y a un div qui suit, je calcule height auto
        // Option activée par défaut pour compatibilité
        if (options.nextdivoverflow) {
            var next = cellpanel.next();
            if (next.prop("tagName") == "DIV")
                next.css({
                    height: "calc(100% - " + cellpanel.css("height") + ")"
                });
        }
        if (options.icon != false) {
            var icontitle, title;
            if (cellpanel.attr("title") != '')
                title = cellpanel.attr("title");
            else
                title = cellpanel.html().trim();
            // Title icon (tooltip) can be set with data-icontitle="Icon title"
            icontitle = options.icontitle;
            var icone = '<i class="ClassIconify fa fa-angle-down"></i>';
            var iconsRight = cellpanel.find('.ClassPanelIconsRight');
            if (!iconsRight.length)
                cellpanel.append('<div class="ClassPanelIconsRight">' + icone + '</div>');
            else
                iconsRight.append(icone);
            cellpanel.find('.ClassIconify').click(function () {
                CYIconify({
                    object: cellwindow,
                    title: title,
                    icontitle: icontitle,
                    icon: options.icon,
                    container: options.container,
                    dblclick: function () {
                        var zindex = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile") + 1;
                        cellwindow.css("z-index", zindex);
                    }
                });
            });
            if (options.iconify)
                cellpanel.find('.ClassIconify').trigger("click");
        }
        if (options.resize) {
            debug && console.log('CYJS : CYPanel resize options=' + JSON.stringify(options));
            cellwindow.resizable({
                //containment: options.container,
                maxWidth: options.maxwidth,
                maxHeight: options.maxheight,
                ghost: options.ghost,
                handles: "n, e, s, w, ne, se, sw, nw",
                autoHide: true,
                resize: function (event, ui) {
                    debug && console.log('CYJS : CYPanel width=' + ui.size.width + ' height=' + ui.size.height);
                }
            });
            // Si on resize, on doit pouvoir magnify
            cellwindow.resize(function () {
                cellwindow.data("fullscreen", false);
            });
        }
        if (options.move) {
            cellpanel.css("cursor", "move");
            cellwindow.draggable({
                handle: options.handle,
                start: function (event, ui) {
                    var zindex = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile");
                    var oldzindex = cellwindow.css("z-index");

                    if (zindex == oldzindex)
                        return;
                    zindex++;
                    cellwindow.css("z-index", zindex);
                    cellwindow.find('*').each(function () {
                        var thiszindex = $(this).css("z-index");
                        if (thiszindex != undefined)
                            $(this).css("z-index", zindex /* - oldzindex + thiszindex*/);
                    });
                    $(this).addClass('ui-dialog-dragging');
                },
                stop: function (event, ui) {
                    $(this).removeClass('ui-dialog-dragging');
                    // Si on move, on doit pouvoir magnify
                    cellwindow.data("fullscreen", false);
                }
            });
        }
        if (options.magnify) {
            var icone = '<i class="ClassMagnify fa fa-window-restore"></i>';
            var iconsRight = cellpanel.find('.ClassPanelIconsRight');
            if (!iconsRight.length)
                cellpanel.append('<div class="ClassPanelIconsRight">' + icone + '</div>');
            else
                iconsRight.append(icone);
            cellpanel.find('.ClassMagnify').click(function () {
                if (cellwindow.data("fullscreen") != true) {
                    debug && console.log('CYJS : CYPanel fullscreen');
                    cellwindow.data("fullscreen", true);
                    cellwindow.data("position", cellwindow.css("position"));
                    cellwindow.data("left", cellwindow.css("left"));
                    cellwindow.data("top", cellwindow.css("top"));
                    cellwindow.data("width", cellwindow.css("width"));
                    cellwindow.data("height", cellwindow.css("height"));
                    cellwindow.data("z-index", cellwindow.css("z-index"));
                    cellwindow.css({
                        "position": "absolute",
                        "left": "0px",
                        "top": "0px",
                        "width": (CYAtoI($("#IDmain").css("width")) - 10) + 'px',
                        "height": (CYAtoI($("#IDmain").css("height")) - 10) + 'px',
                        "z-index": CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile") + 1
                    });
                } else {
                    debug && console.log('CYJS : CYPanel not fullscreen');
                    cellwindow.data("fullscreen", false);
                    cellwindow.css("position", cellwindow.data("position"));
                    cellwindow.css("left", cellwindow.data("left"));
                    cellwindow.css("top", cellwindow.data("top"));
                    cellwindow.css("width", cellwindow.data("width"));
                    cellwindow.css("height", cellwindow.data("height"));
                    cellwindow.css("z-index", cellwindow.data("z-index"));
                }
            });
        }
        $(this).addClass("ClassReady");
    });
}
/*
 * Autocomplete
 *    CYAutoComplete("#IDnomclient", listeclients)
 * ou callback spécifique :
 *    CYAutoComplete("#IDnomclient", listeclients, { minLength: 3 }, function (request, response)
 *        {
 *            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
 *            response($.grep(listeclients, function(item)
 *            {
 *                if(matcher.test(item)) { // true si saisie courante match avec l'item de la liste
 *                    debug && console.log("CYJS : CYAutoComplete input '" + request.term + "' item '" + item + "'");
 *                    return true;
 *                }
 *                return false;
 *            }));
 *        });
 * Pour utiliser une valeur retournée différente de celle qui est affichée :
 *   let tags = [];
 *   liste.forEach(function(el) {
 *     let t = {};
 *     t.value = el.champ1;
 *     t.label = el.champ2 + " - " + el.champ3 + ', ' + el.champ4;
 *     tags.push(t);
 *   });
 *   $("#idselecteur").data('tags', tags); // Range les tags dans l'objet
 *   ...
 *   ("#idselecteur").CYAutoComplete($("#idselecteur").data('tags'), {
 *       minLength: 3
 *     }, function(request, response) {
 *       let matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
 *       response($.grep($("#idselecteur").data('tags'), function (item) {
 *       if(matcher.test(item.label)) { // Je recherche dans le label et value sera retourné
 *         return true;
 *       }
 *       return false;
 *     }));  
 *   });
 */
function CYAutoComplete(id, tags, cyoptions, callback) {
    debug && console.log('CYJS : CYAutoComplete id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassAutoComplete");
    else
        var ids = $(id).findall(".ClassAutoComplete");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.each(function () {
        var options = {
            minLength: 1,
            source: function (request, response) {
                if (callback == undefined) {
                    // Si recherche début : var matcher = new RegExp("^" +  $.ui.autocomplete.escapeRegex(request.term), "i");
                    var matcher = new RegExp( /*"^" + */ $.ui.autocomplete.escapeRegex(request.term), "i");
                    response($.grep(tags, function (item) {
                        (debug & DEBUG_MORE) && console.log("CYJS : CYAutoComplete input '" + request.term + "' item '" + item + ", matcher '" + matcher.test(item) + "'");
                        return (matcher.test(item));
                    }));
                } else
                    return (callback(request, response));
            }
        };
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        debug && console.log('CYJS : CYAutoComplete id="' + $(this).attr('id') + '" ' + CYDumpObj(options));
        // Si l'objet a été créé dans un dialog, il disparaitra à sa fermeture. Il faut donc le
        // réinitialiser à chaque appel, dans le doute ..
        if ($(this).hasClass("ClassReady"))
            $(this).autocomplete('destroy');
        $(this).autocomplete(options);
        $(this).addClass("ClassReady");
    });
}
/*
 * Déclare une dropzone dans id pour upload d'un seul fichier de taille maxi maxfilesize (en Mo)
 *  callbackcomplete sera appelée à la fin de l'opération avec le fichier uploadé
 *  callbackerror sera appelée pour chaque upload en erreur
 *
 *   Exemple CYDropZone() + CYFileList() + CYFileListClick() :
 *
 *   var mydropzone = CYDropZone("#IDdropzone",
 *   {
 *       maxFilesize: 100, // Mb
 *       //createImageThumbnails: true,
 *       //maxThumbnailFilesize: 10,
 *       thumbnailWidth: "150",
 *       thumbnailHeight: "150",
 *       autoProcessQueue: true,
 *       callbackaccept:  function(file)
 *       {
 *           if (test-exist(file) != undefined)
 *               return "existing";
 *           return;
 *       },
 *       callbackcomplete: function(file)
 *       {
 *           ListDocuments(dir);
 *       },
 *       callbackerror:  function(file)
 *       {
 *           if (test-exist(file) != undefined)
 *               CYAlert("Fichier " + file.name + " existant.");
 *           else
 *               CYAlert('ERROR LOADING');
 *       }
 *    });
 *    mydropzone.removeAllFiles();
 *    mydropzone.enable();
 *    // Url d'upload d'un fichier
 *    mydropzone.options.url = cyServerName + cyWebSvc + "?r=upload";
 *    ListDocuments(dir);
 *
 *    function ListDocuments(dir)
 *    {
 *        // Info fichiers retournées par url listant les fichiers et necessitees par CYFileList()
 *        var fields =  { id: 'id', filetype: 'type', filename: 'nom', filesize: 'taille', thumb: "" };
 *        // Url qui liste les fichiers
 *        var q = cyServerName + cyWebSvc + "?r=listfiles";
 *        // Il faut un id dans la liste des fichiers retournée pour CYFileListClick()
 *        CYFileList('#IDFileList', fichiers, fields, CYFileListClick);
 *   }
 */
function CYDropZone(id, cyoptions, callbackcomplete, callbackerror) {
    var options = {};

    if (id.indexOf('#') != 0)
        id = "#" + id;
    debug && console.log('CYJS : CYDropZone "' + id + '"');
    if (typeof cyoptions != "object")
        var cyoptions = {};
    if (callbackcomplete != undefined)
        cyoptions.callbackcomplete = callbackcomplete;
    if (callbackerror != undefined)
        cyoptions.callbackerror = callbackerror;
    Object.assign(options, {
        /*
        maxFiles: 1,
        maxFilesize: 10,
        acceptedFiles: ".jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.pdf",
        previewsTemplate: $("#IDdropzonepreview").html(),
        autoProcessQueue: true,
        parallelUploads: 10,
        createImageThumbnails: true,
        maxThumbnailFilesize: 10,
        parallelUploads: 2,
        thumbnailWidth: "120",
        thumbnailHeight: "120",
        */
        accept: function (file, done) {
            if (cyoptions.callbackaccept != undefined)
                done(cyoptions.callbackaccept(file));
            else
                done();
        },
        init: function () {
            this.on("addedfile", function (file, done) {
                /*
                var selected_filenames = $.map(this.getAcceptedFiles(), function (selected_file)
                    {
                        return (selected_file.name);
                    }
                );
                // selected_filenames contient les fichiers déjà enqueued
                if ($.inArray(file.name, selected_filenames) != -1)
                {
                    debug && console.log("CYJS : CYDropZone already added file " + file.name);
                    this.removeFile(file);
                    done();
                }
                else*/
                debug && console.log("CYJS : CYDropZone addedfile " + file.name);
                $(".ClassDropZone,.ClassDropZone:hover").addClass("ClassDropZoneInProgress");
            });
            // Je purge la file après upload
            this.on("complete", function (file) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    debug && console.log('CYJS : CYDropZone complete all uploaded');
                    this.removeAllFiles();
                    if (cyoptions.callbackcomplete != undefined)
                        cyoptions.callbackcomplete(file);
                    $(".ClassDropZone,.ClassDropZone:hover").removeClass("ClassDropZoneInProgress");
                }
            });
            this.on("error", function (file) {
                debug && console.log('CYJS : CYDropZone error ' + file.name);
                if (cyoptions.callbackerror != undefined)
                    cyoptions.callbackerror(file);
                else
                    CYAlert('ERROR LOADING');
            });
        }
    });
    Object.assign(options, cyoptions);
    Object.assign(options, CYExtrAttrs($(id)));
    debug && console.log('CYJS : CYDropZone "' + id + '" ' + JSON.stringify(options));
    var cyDropZone = new Dropzone(id, options);
    return (cyDropZone);
}
/*
 * Recherche et construit dans l'id la liste des thumbnails de l'objet précisé
 * id désigne le div contenant la liste des fichiers
 * files contient la liste un tableau de fichiers contenant :
 *      nom / type / taille / thumb
 * fields décrit la structure de l'objet fichier dans la liste fournie en retour de la requête
 *      fields.filename = 'nom'
 *      fields.filetype = 'type'
 *      fields.filesize = 'taille'
 *      fields.filethumb = 'thumbnail en base64'
 *      fields.fileclass = nom d'une classe (facultatif, pour mettre en évidence un élément
 *                          via la classe précisée)
 *      fields.filedate = 'mtime'
 *      fields.title = 'title'
 * callbackclick sera appelée si présente qd on clique un fichier
 * callbackdoubleclick sera appelée si présente qd on double clique un fichier
 * complete sera appelée si présente à la fin
 * A part l'id, tous les arguments peuvent être dans l'objet cyoptions :
 *          CYFileList('#IDFileList',
 *               {
 *                   files: fichiers,
 *                   fields: fields,
 *                   callbackdblclick: function(thumbnail, fichiers)
 *                   {
 *                       var fichier = $(thumbnail).data("fichier");
 *                       if (fichier.type == 'dir')
 *                       {
 *                           ...
 *                       }
 *                   },
 *                   callbackclick: function(thumbnail, event)
 *                   {
 *                       var fichier = $(thumbnail).data("fichier");
 *                       if (fichier.type == 'dir')
 *                       {
 *                           ...
 *                       }
 *                   },
 *                   callbackmove: function(thumbnail, event)
 *                   {
 *                       var fichier = $(thumbnail).data("fichier");
 *                       if (fichier.type == 'dir')
 *                       {
 *                           ...
 *                       }
 *                   },
 *                   callbackcomplete: function()
 *                   {
 *                      ...
 *                   }
 *               }
 */
function CYFileList(id, cyoptions, fields, callbackclick, callbackdblclick, callbackcomplete) {
    if (id.indexOf('#') != 0)
        id = "#" + id;
    debug && console.log('CYJS : CYFileList ' + id);
    if ($.isArray(cyoptions)) // Compat 1.0
    {
        var files = cyoptions;
        var cyoptions = {};
        cyoptions.files = files;
        if (fields != undefined)
            cyoptions.fields = fields;
        if (callbackclick != undefined)
            cyoptions.callbackclick = callbackclick;
        if (callbackdblclick != undefined)
            cyoptions.callbackdblclick = callbackdblclick;
        if (callbackcomplete != undefined)
            cyoptions.callbackcomplete = callbackcomplete;
    }
    if (cyoptions.files == null)
        return;
    if (cyoptions.startHtml != null)
        $(id).html(cyoptions.startHtml);
    else
        $(id).html('');
    // Cherche type d'icone
    for (var i = 0; i < cyoptions.files.length; i++) {
        var fichier = cyoptions.files[i];
        var unit = "Ko";
        var filesize = fichier[cyoptions.fields.filesize];
        var name = fichier[cyoptions.fields.filename].toLowerCase();
        var filetype = fichier[cyoptions.fields.filetype].toLowerCase();
        var fileclasse = fichier[cyoptions.fields.fileclass];
        var filedate = fichier[cyoptions.fields.filedate];
        var title = fichier[cyoptions.fields.title];
        if (title != undefined) {
            title = CYReplaceAll(title, "'", "&apos;");
            title = "title='" + title + "' ";
        } else
            title = '';
        // Affichage d'un dossier, filesize peut contenir le nb de fichiers
        // dans le dossier. Si filesize < 0 je n'affiche pas le nb de fichiers contenu ds le dir
        if (filetype == "dir") {
            var ic = "fa-folder";
            if (filesize >= 0)
                unit = CYTranslateString("FILES");
            else
                filesize = unit = "";
        } else if (filetype.indexOf('image') >= 0 || name.right('.jpg') || name.right('.jpeg') || name.right('.png') || name.right('.gif') || name.right('.tiff'))
            var ic = "fa-file-picture-o";
        else if (filetype.indexOf('pdf') >= 0 || name.right('.pdf'))
            var ic = "fa-file-pdf-o";
        else if (filetype.indexOf('zip') >= 0 || name.right('.zip'))
            var ic = "fa-archive";
        else if (filetype.indexOf('video') >= 0 || name.right('.mp4') || name.right('.m4v') || name.right('.mov'))
            var ic = "fa-file-video-o";
        else if (filetype.indexOf('mp3') >= 0 || name.right('.mp3'))
            var ic = "fa-file-audio-o";
        else if (filetype.indexOf('word') >= 0 || name.right('.doc') || name.right('.docx'))
            var ic = "fa-file-word-o";
        else if (filetype.indexOf('excel') >= 0 || name.right('.xls') || name.right('.xlsx'))
            var ic = "fa-file-excel-o";
        else if (filetype.indexOf('powerpoint') >= 0 || name.right('.ppt') || name.right('.pptx'))
            var ic = "fa-file-powerpoint-o";
        else if (filetype.indexOf('html') >= 0)
            var ic = "fa-file-text";
        else if (filetype.indexOf('text') >= 0 || filetype.indexOf('log') >= 0)
            var ic = "fa-file-text-o";
        else
            var ic = "fa-file";
        // Pas de thumb, je mets une icone
        if (fichier[cyoptions.fields.filethumb] == "")
            var img = '<i class="fa ' + ic + '"></i>';
        // Il y a un thumb, je le mets en image
        else
            var img = '<img src="data:image/png;base64,' + fichier[cyoptions.fields.filethumb] + '"/>';
        var classe = "ClassThumbnail";
        if (fileclasse != undefined && fileclasse != "")
            classe += " " + fileclasse;
        var iddiv = id.substring(1) + '-' + i;
        // Mode custom render
        if (cyoptions.render != undefined && typeof cyoptions.render === "function")
            var el = cyoptions.render(iddiv, classe, img, fichier);
        // Mode default render - pour cyjsdemo
        // Cree un ClassThumbnail id dont le nom est l'id du CYFileList() suivi de - et de l'indice du fichier dans le tableau
        else {
            let unit = CYTranslateString("BYTES");
            let taille = filesize;
            unit = unit.substr(0, 1);
            // filetype = 'dir' et filesize = '' => ne rien afficher en
            // plus du nom
            if (filetype == "dir") {
                if (filesize == '')
                    unit = '';
                else
                    unit = (taille > 1) ? CYTranslateString("FILES") : CYTranslateString("FILE");
            } else if (filesize >= 1048576) {
                taille = sprintf("%.2f", filesize / 1048576);
                unit = "M" + unit;
            } else if (filesize >= 1024) {
                taille = sprintf("%.2f", filesize / 1024);
                unit = "K" + unit;
            }
            var el = '<li ' + title + 'class="' + classe + '" id="' + iddiv + '"><div class="ClassThumbnailImage">' + img + '</div><div class="ClassThumbnailLabel">' + fichier[cyoptions.fields.filename] + '<br>' + taille + " " + unit + '</div></li>';
        }
        $(id).append(el);
        // Range l'objet fichier dans l'id du ClassThumbnail, les callback pourront ainsi
        // le récupérer
        $("#" + iddiv).data("fichier", fichier);
        // Un dossier peut recevoir un drag
        if (filetype == "dir" || filetype == "application/zip") {
            $("#" + iddiv).droppable({
                accept: ".ClassThumbnail",
                tolerance: "pointer",
                /* ui-droppable-active permet de mettre en évidence tous les droppable
                classes:
                {
                    "ui-droppable-active": "ui-state-default"
                },*/
                drop: function (event, ui) {
                    var draggable = ui.draggable;
                    var idsrc = draggable.attr("id");
                    var iddest = $(event.target).attr("id");
                    if (cyoptions.callbackmove != undefined)
                        cyoptions.callbackmove("#" + idsrc, "#" + iddest);
                },
                over: function (event, ui) {
                    $(this).addClass("droppable-active");
                },
                out: function (event, ui) {
                    $(this).removeClass("droppable-active");
                }
            });
        }
        // Clic/Doubleclick sur thumbnail
        if (cyoptions.callbackclick != undefined || cyoptions.callbackdblclick != undefined) {
            $("#" + iddiv).CYClick(cyoptions.callbackclick, cyoptions.callbackdblclick);
        }
    }
    CYTooltip(id);
    if (cyoptions.endHtml != null)
        $(id).append(cyoptions.endHtml);
    // Permettre le drag des icones
    $(".ClassThumbnail").draggable({
        delay: 200,
        opacity: 0.3,
        revert: true,
        helper: 'clone',
        appendTo: 'body',
        zIndex: 9999,
        cursorAt: // Moitié de l'icone
        {
            top: CYAtoI($(".ClassThumbnailImage").css("height")) / 2,
            left: CYAtoI($(".ClassThumbnailImage").css("width")) / 2
        },
        drag: function (e, ui) {
            $(this).addClass("ClassThumbnailSelected");
            // Sensé resizer l'icone draggé ... : ui.helper.css({ height: "50px", width: "50px" });
        },
        stop: function (event, ui) {
            $(this).removeClass("ClassThumbnailSelected");
            //var fichier = $(this).data("fichier");

        }
    });
    if (cyoptions.callbackcomplete != undefined)
        cyoptions.callbackcomplete();
}
/*
 * Iconification d'un objet (objet = $(id))
 * title est l'attribut title (tooltip)
 * icontitle est le nom de l'icone générée
 * containement est le conteneur limitant la position de l'icone, #IDpagecontent par défaut
 * container est un selecteur (#id), qui va contenir l'icone, #IDmain par défaut
 *      CYIconify(
 *      {
 *          object: objet ou "#id"
 *          title: "titre",
 *          icontitle: "titre",
 *          icon: "fa-cog",
 *          container: "page-content",
 *          containment: "page-content",
 *          dblclick: function(){ ... }
 *      });
 * Pour iconiser depuis le code JS :
 *      $(id).data("iconcss", { top: "260px", right: 0 });
 *      $(id).find(".ClassPanel .ClassIconify").trigger("click");
 */
function CYIconify(cyoptions) {
    var d = new Date();
    var idicon = "IDicon" + CYUniqueId();
    // La fct CYIsIconifyAble doit retourner true pour autoriser l'iconification
    if (typeof CYIsIconifyAble == "function" && !CYIsIconifyAble(cyoptions.object.attr("id"), true))
        return;
    if (cyoptions.icon == undefined)
        cyoptions.icon = '';
    if (cyoptions.icon == '')
        cyoptions.icon = 'fa-file';
    if (cyoptions.container == undefined || cyoptions.container == '')
        cyoptions.container = "#IDmain";
    if (cyoptions.title == undefined)
        cyoptions.title = '';
    if (cyoptions.icontitle == undefined)
        cyoptions.icontitle = '';
    if (cyoptions.containement == undefined || cyoptions.containement == '')
        cyoptions.containement = "#IDpagecontent";
    // Si un #id est fourni, je prends l'objet correspondant
    if (typeof cyoptions.object != "object")
        cyoptions.object = $(cyoptions.object);
    debug && console.log('CYJS : CYIconify title="' + cyoptions.title + '" icontitle="' + cyoptions.icontitle + '" in ' + cyoptions.container);
    // Préciser data-iconss="top: "20px", right: 0" sur le div parent du ClassPanel
    // pour pré-positionner l'icone
    var css = cyoptions.object.data("iconcss");
    if (css == undefined) {
        var offset = cyoptions.object.position();
        css = "top: " + offset.top + "px; left: " + offset.left + "px";
    } else
        css = CYImplode(";", css, true, true);
    var zi = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile") + 1;
    css += "; z-index: " + zi;
    cyoptions.object.css("visibility", "hidden");
    $(cyoptions.container).append('<i title="' + cyoptions.title + '" id="' + idicon + '" style="visibility: hidden;' + css + '" class="ClassIconFile ' + CYFontAwesomeIconName(cyoptions.icon) + '" data-idsrc="' + cyoptions.object.attr("id") + '"><span><br>' + cyoptions.icontitle + '</span></i>').CYTooltip();
    cyoptions.object.transfer({
        to: "#" + idicon,
        duration: cyEffectDuration
    }, function () {
        $("#" + idicon).css("visibility", "visible");
        cyoptions.object.hide();
    });
    // Icone draggable
    $("#" + idicon).draggable({
        //containment: cyoptions.containement,
        start: function (e, ui) {
            var zindex = CYMaxZindex(".ui-dialog,.ClassWindow,.ClassIconFile") + 1;
            $(this).css("z-index", zindex);
        }
    });
    // Icone doubleclickable
    $("#" + idicon).CYClick(undefined, function () {
        // La fct CYIsIconifyAble doit retourner true pour autoriser l'ouverture par double-clic
        if (typeof CYIsIconifyAble == "function" && !CYIsIconifyAble($("#" + idicon).attr("data-idsrc"), false))
            return;
        cyoptions.object.show();
        cyoptions.object.css("visibility", "hidden");
        cyoptions.object.data("iconcss", { top: $("#" + idicon).css("top"), left: $("#" + idicon).css("left") });
        $("#" + idicon).transfer({
            to: "#" + $("#" + idicon).attr("data-idsrc"),
            duration: cyEffectDuration
        }, function () {
            cyoptions.object.css("visibility", "visible");
            $("#" + idicon).remove();
        });
        if (typeof cyoptions.dblclick === "function")
            cyoptions.dblclick(cyoptions.object);
        // La fct CYIsIconified sera appelée avec l'id du panel iconifié et argument false pour indiquer magnify
        if (typeof CYIsIconified == "function")
            CYIsIconified($("#" + idicon).attr("data-idsrc"), false);
    });
    // La fct CYIsIconified sera appelée avec l'id du panel à iconifier et argument true pour indiquer iconify
    if (typeof CYIsIconified == "function")
        CYIsIconified(cyoptions.object.attr("id"), true);
}
/*
 * Ouvre un menu contenant n'importe quoi. Un tag de classe ClassMenu peut ainsi être défini dans la page html
 * L'ouverture du menu sera déclenchée par un click sur l'élément parent, une image , n'importe quoi ..
 *     <ul class="ClassMenu" style="width:150px">
 *         <li id="c1"><i class="fa fa-refresh"></i>Option 1</li>
 *         <li id="c2"><i class="fa fa-spinner"></i>Option 2</li>
 *         <li id="c3"><i class="fa fa-check-square-o"></i>Option 3</li>
 *     </ul>
 */
function CYMenu(id, cyoptions) {
    debug && console.log('CYJS : CYMenu id=' + (typeof id == 'object' ? 'object' : JSON.stringify(id)));
    if (typeof id == "object")
        var ids = id;
    else if (id == undefined || id == '')
        var ids = $(".ClassMenu");
    else
        var ids = $(id).findall(".ClassMenu");
    if (typeof cyoptions != "object")
        var cyoptions = {};
    ids.not(".ClassReady").each(function () {
        var menu = this;
        $(menu).hide();
        var options = {};
        Object.assign(options, cyoptions);
        Object.assign(options, CYExtrAttrs($(this)));
        $(this).addClass("ClassReady");
        debug && console.log('CYJS : CYMenu ' + CYDumpObj(options));
        $(menu).parent().on("click", function (event) {
            var defaultmargin = 10;
            event.stopPropagation();
            // Supprime les autres menus eventuels
            $(".ClassMenu").hide();
            $(".ClassContextMenu").remove();
            // Supprime tout tooltip
            $('.ui-tooltip').fadeOut(100, function () { $(this).remove() });
            $(menu).show({
                duration: cyEffectDuration,
                easing: "swing",
                start: function () {
                    $(menu).css({ left: '', top: '' });
                    // Menu au dessus du reste
                    var z = CYMaxZindex();
                    if (z >= CYAtoI($(this).css("z-index")))
                        $(this).css("z-index", ++z);
                    $(this).CYReCenter();
                    $(menu).one("mouseleave click", function (event) {
                        $(menu).hide();
                        $(menu).css("z-index", 0);
                    });
                    $(window).one("click keyup", function (event) {
                        $(menu).hide();
                        $(menu).css("z-index", 0);
                    });
                }
            });
        });
    });
}
/*
 * Crée et affiche un menu contextuel à l'endroit courant de la souris.
 * Il ne peut y en avoir qu'un seul d'ouvert à la fois.
 * Le contenu html est soit du code html brut, soit un tableau d'objects pour
 * créer une succession de <li></li>.
 * Dans tous les cas l'objet généré est encapsulé dans un class="ClassContextMenu"
 * Il est possible de spécifier dans options la position top, left ou la marge
 * (la marge permet de garder le curseur dans le popup)
 * Ainsi que la classe optionnelle à appliquer au <ul> parent des <li>
 * Exemple pour argument html :
 *       CYContextMenu(
 *          [
 *              { id: "c1", icon: "refresh", label: "Option 1" },
 *              { id: "c2", icon: "spinner", label: "Option 2", disabled: true },
 *              { id: "c3", icon: "check-square-o", label: "Option 3" }
 *          ],
 *          {
 *              margin: 10,
 *              top: 100,
 *              left: 100,
 *              class: "OptionalClass",
 *              draggable: false,
 *              style: "code CSS appliqué au <ul>"
 *          }
 *       );
 * Exemple d'objet :
 *     { id: "id", icon: "fa fa-user", label: "label" }
 *     { id: "id", icon: "user", label: "label" } // 'fa fa-' sera automatiquement prefixe
 *     { id: "id", icon: "", label: "label" } // Pas d'icone
 * Options :
 *     {
 *          margin: 10; // default - permet depositionner la souris à l'intérieur du popup créé
 *          left: 999; // default: calcul auto
 *          top: 999; // default: calcul auto
 *          class: 'ClassContextMenu'; // default
 *          style: "code CSS appliqué au div ClassContextMenu"
 *          // Fonction appelée lors de la selection uniquement pour un tableau d'objets
 *          // car dans ce cas il y a un id qui est positionné et je l'envoie en argument, index est la position du li
 *          select: function(index, id) { ... }
 *     }
 */
function CYContextMenu(htmlorarray, options) {
    var defaultmargin = 10, h;

    if (options == undefined) {
        var options = {
            margin: defaultmargin,
            left: cyCurrentMousePosition.x - defaultmargin,
            top: cyCurrentMousePosition.y - defaultmargin,
            class: "ClassContextMenu",
            style: ""
        };
    }
    else {
        if (options.margin == undefined)
            options.margin = defaultmargin;
        if (options.left == undefined)
            options.left = cyCurrentMousePosition.x - options.margin;
        if (options.top == undefined)
            options.top = cyCurrentMousePosition.y - options.margin;
        if (options.class == undefined)
            options.class = "ClassContextMenu";
        else
            options.class = "ClassContextMenu " + options.class;
        if (options.style == undefined)
            options.style = "";
    }
    debug && console.log("CYJS : CYContextMenu " + JSON.stringify(htmlorarray) + " options " + CYImplode(",", options));
    if (typeof htmlorarray !== 'string') {
        h = "<ul class='" + options.class + "' style='display: none;" + options.style + "'>";
        htmlorarray.forEach(function (l) {
            let disabled = (l.disabled != undefined && l.disabled) ? 'disabled ' : '';
            // Je ne veux pas afficher d'icone pour la ligne
            if (l.icon == '')
                h += '<li ' + disabled + 'id="' + l.id + '">' + CYTranslateString(l.label) + '</li>';
            else
                h += '<li ' + disabled + 'id="' + l.id + '"><i class="' + CYFontAwesomeIconName(l.icon) + '"></i>' + CYTranslateString(l.label) + '</li>';
        });
        h += "</ul>"
    } else
        h = "<div class='" + options.class + "' style='display: none;" + options.style + "'>" + htmlorarray + "</div>";
    // Supprime les autres menus eventuels
    $(".ClassContextMenu").remove();
    $(".ClassMenu").hide();
    // Supprime tout tooltip
    $('.ui-tooltip').fadeOut(100, dummy => $(this).remove());
    $(cyPageContent).append(h);
    // Controle selon largeur
    let xmax = CYAtoI($(cyPageContent).css('left')) + CYAtoI($(cyPageContent).css('width'));
    if (options.left + CYAtoI($(".ClassContextMenu").css('width')) > xmax + options.margin)
        options.left = xmax - CYAtoI($(".ClassContextMenu").css('width')) - 4 * options.margin;
    // Controle selon hauteur
    let ymax = CYAtoI($(cyPageContent).css('top')) + CYAtoI($(cyPageContent).css('height'));
    if (options.top + CYAtoI($(".ClassContextMenu").css('height')) > ymax + options.margin) {
        options.top = ymax - CYAtoI($(".ClassContextMenu").css('height')) - 4 * options.margin;
    }
    $(".ClassContextMenu").css({ left: options.left, top: options.top });
    // Menu au dessus du reste
    $(".ClassContextMenu").fadeIn(cyEffectDuration, function () {
        // Menu au dessus du reste
        var z = CYMaxZindex();
        if (z >= CYAtoI($(this).css("z-index")))
            $(this).css("z-index", ++z);
    });
    if (options.draggable)
        $(".ClassContextMenu").draggable();
    var sel = ".ClassContextMenu";
    // Si on a fournit un tableau d'objets je veux intercepter un click sur le li
    if (typeof htmlorarray !== 'string')
        sel += ' li';
    $(sel).one("click", function (event) {
        $(".ClassContextMenu").fadeOut(cyEffectDuration, function () { $(this).remove(); });
        // Si on a fournit un tableau d'objets et qu'on a prévu une fonction "select"
        if (typeof htmlorarray !== 'string' && typeof options.select == "function")
            // alors j'apl la fonction spécifiée par le select et lui transmets
            // l'index du li et son id
            options.select($(this).index(), $(this).attr('id'));
    });
    !(debug & DEBUG_DYN) && $(".ClassContextMenu").one("mouseleave", function (event) {
        $(".ClassContextMenu").fadeOut(cyEffectDuration, function () { $(this).remove(); });
    });
    $(window).one("click ckeyup", function (event) {
        $(".ClassContextMenu").fadeOut(cyEffectDuration, function () { $(this).remove(); });
    });
}
/*
 * Ajout/suppression icones pour édition fichiers d'une page
 * Default : status = true
 */
function CYSetIconFileEdit(status) {
    if (status != true && status != false)
        status = true;
    if (!status) {
        if ($("#IDsidemenucontent li i").hasClass("ClassIconEditPage"))
            $("#IDsidemenucontent li i.ClassIconEditPage").remove();
        return;
    }
    debug && console.log("CYSetIconFileEdit=" + (status ? "true" : "false"));
    if (!$("#IDsidemenucontent li i").hasClass("ClassIconEditPage"))
        $("#IDsidemenucontent li").each(function () {
            if ($(this).attr("page") != undefined)
                $(this).append('<i class="fa fa-edit ClassIconEditPage"></i>');
        });
    $(".ClassIconEditPage").click(function (event) {
        event.stopPropagation();
        page = $(this).parent().attr('page');
        CYFileEdit(page + '.html', {
            url: cyServerName + cyPath + "/php/downloadfile.php",
            data: { f: cyPath + "/html/" + page + ".html" },
            format: "text",
            save: function (content) {
                CYExec({
                    url: cyServerName + cyPath + "/php/uploadfile.php",
                    data: { f: cyPath + "/html/" + page + ".html", r: "writefile", content: content },
                    callbackok: function (ret) {
                        CYAlert({ message: ret, width: '200px' });
                    }
                });
                return true;
            }
        });
        CYFileEdit(page + '.js', {
            url: cyServerName + cyPath + "/php/downloadfile.php",
            data: { f: cyPath + "/js/" + page + ".js" },
            format: "text",
            save: function (content) {
                CYExec({
                    url: cyServerName + cyPath + "/php/uploadfile.php",
                    data: { f: cyPath + "/js/" + page + ".js", r: "writefile", content: content },
                    callbackok: function (ret) {
                        CYAlert({ message: ret, width: '200px' });
                    }
                });
                return true;
            }
        });
        CYFileEdit(page + '.css', {
            url: cyServerName + cyPath + "/php/downloadfile.php",
            data: { f: cyPath + "/css/" + page + ".css" },
            format: "text",
            save: function (content) {
                CYExec({
                    url: cyServerName + cyPath + "/php/uploadfile.php",
                    data: { f: cyPath + "/css/" + page + ".css", r: "writefile", content: content },
                    callbackok: function (ret) {
                        CYAlert({ message: ret, width: '200px' });
                    }
                });
                return true;
            }
        });
    });
}
/*
 * Edition fichier dans popup. Il suffit que format contienne le mot "html" pour passer en html
 *      CYFileEdit("filename",
 *      {
 *          url: "/php/websvc.php",
 *          data: { r: "download", f: "filename" ... },
 *          format: "text" par defaut ou doit contenir "html",
 *          save: function(html, alertobj)
 *          {
 *              sauver ici le contenu html dans le fichier
 *              alertobj est l'objet alert créé dynamiquement, si utile ..
 *          }
 *      })
 */
function CYFileEdit(file, cyoptions) {
    var id = "ID" + CYUniqueId();

    debug && console.log("CYJS : CYFileEdit " + file + " cyoptions " + JSON.stringify(cyoptions));
    CYExec({
        url: cyoptions.url,
        data: cyoptions.data,
        callbackok: function (ret) {
            if (ret == -1) {
                CYAlert("ERROR LOADING");
                return;
            }
            var msg = '<textarea class="ClassEditor" id="' + id + '"></textarea>';
            CYAlert({
                autofade: 0,
                modal: cyoptions.modal !== undefined ? cyoptions.modal : false,
                title: cyoptions.title !== undefined ? cyoptions.title : file,
                message: msg,
                okbuttontext: cyoptions.okbuttontext !== undefined ? cyoptions.okbuttontext : "SAVE",
                fullscreen: cyoptions.fullscreen !== undefined ? cyoptions.fullscreen : true,
                width: cyoptions.width != undefined ? cyoptions.width : $("#IDmain").width() * 0.75,
                height: cyoptions.height != undefined ? cyoptions.undefined : $("#IDmain").height() * 0.75,
                classes: {
                    "ui-dialog-content": "CYFileEdit"
                },
                okfunctionName: function (alertobj) {
                    if (cyoptions.format.search(/html$/i) != -1)
                        return cyoptions.save($('#' + id).trumbowyg('html'), alertobj);
                    else
                        return cyoptions.save($('#' + id).val(), alertobj);
                },
                resizeFunction: function (alertobj) {
                    // Reajuste la hauteur de l'editeur pour obtenir une scrollbar correcte
                    $(alertobj).find('.trumbowyg-box, .trumbowyg-editor').css('height', $(alertobj).height() - $(alertobj).find('.trumbowyg-button-pane').height());
                }
            });
            // Si le suffixe du fichier est html
            if (file.search(/[.]html$/i) != -1) {
                CYEditor("#" + id);
                $('#' + id).trumbowyg('html', ret);
                // Reajuste la hauteur de l'editeur pour obtenir une scrollbar correcte
                var h = $('#' + id).closest('.ClassAlert').height();
                h -= $('#' + id).prevAll('.trumbowyg-button-pane').height();
                $('#' + id).prev('.trumbowyg-editor').css('height', h);
            } else
                $("#" + id).val(ret);
            return (0);
        }
    });
}
/*
 * Formatte et retourne un tag quelconque.
 * Exemple :
 *      html = CYTag("p", { style: "color: blue; font-size: 10px", align: "left" }, "Mon texte");
 * ou
 *      html = CYTag("p", 'style: "color: blue; font-size: 10px" align: "left"', "Mon texte");
 * html vaudra :
 *      <p style="color: blue; font-size: 10px" align: "left">Mon texte</p>
 */
function CYTag(tag, attrs, value) {
    var html = '<' + tag;
    if (typeof attrs == "object") {
        for (attr in attrs)
            html += ' ' + attr + '="' + attrs[attr] + '"';
    } else
        html += ' ' + attrs;
    if (value == undefined)
        value = '';
    html += '>' + value + '</' + tag + '>';
    return (html);
}
/*
 * Conversion intelligente de date (si pas de jour -> 01)
 * - jj/mm/aaaa ou mm/aaaa converti en aaaa-mm-jj
 * - aaaa-mm ou aaaa-mm-jj converti en jj/mm/aaaa
 * Retourne inchangé si format non reconnu
 */
function CYConvDate(d) {
    var from = d.split("-");
    if (from.length == 2) // aaaa-mm
        return ('01/' + from[1] + '/' + from[0]);
    else if (from.length == 3) // aaaa-mm-jj
        return (from[2] + '/' + from[1] + '/' + from[0]);
    else {
        from = d.split("/");
        if (from.length == 2) // mm/aaaa
            return (from[1] + '-' + from[0] + '-01');
        else if (from.length == 3) // jj/mm/aaaa
            return (from[2] + '-' + from[1] + '-' + from[0]);
    }
    return (d);
}
/*
 * Conversion de date JJ/MM/AAAA ou AAAA-MM-JJ en AAAAMMJJ
 * Returne -1 si format entrée inconnu
 */
function CYConvDateAAAAMMJJ(d) {
    var from = d.split("-");
    if (from.length == 3) // aaaa-mm-jj
        return (sprintf("%04d%02d%02d", from[0], from[1], from[2]));
    else {
        from = d.split("/");
        if (from.length == 3) // jj/mm/aaaa
            return (sprintf("%04d%02d%02d", from[2], from[1], from[0]));
    }
    return (-1);
}
/*
 * Retourne le z-index le plus élevé de la sélection
 * Alternative possible plus rapide :
 *   zindex = Math.max.apply(null, $.map($("#IDmain " + id), function(e){
 *      return parseInt($(e).css('z-index')) || 1;
 *   }));
 * Si all = true, recherche également parmi les éléments cachés
 */
function CYMaxZindex(id, all) {
    var quoi = '';

    if (id == undefined || id == '')
        var id = "*";
    all = all == undefined ? false : all;
    var zindex = 0;
    $("#IDmain").find(id).each(function () {
        if (!all && $(this).css("display") == "none")
            return;
        var izindex = CYAtoI($(this).css('z-index'));
        if (izindex > zindex) {
            zindex = izindex;
        }
    });
    debug && console.log('CYJS : CYMaxZindex "' + id + '" = ' + zindex);
    return (parseInt(zindex));
}
/*
 * Calcule et retourne un unique id aléatoire
 */
function CYUniqueId() {
    var d = new Date();
    return (d.getTime() + Math.floor((Math.random() * 1000) + 1));
}
/*
 * Dump objet et le retourne si retour = false, sinon l'affiche en console (defautl)
 */
function CYDumpObj(obj, retour) {
    getCircularReplacer = function () {
        const ancestors = [];
        return function (key, value) {
            if (typeof value !== "object" || value === null) {
                return value;
            }
            // `this` is the object that value is contained in,
            // i.e., its direct parent.
            while (ancestors.length > 0 && ancestors.at(-1) !== this) {
                ancestors.pop();
            }
            if (ancestors.includes(value)) {
                return "[Circular]";
            }
            ancestors.push(value);
            return value;
        };
    };
    if (!retour)
        return (JSON.stringify(obj, getCircularReplacer()));
    console.log(JSON.stringify(obj, getCircularReplacer()));
}
/*
 * Convertit un obj en string, les elements seront separes par sep
 * Si varname = true, chaque element sera precede de son nom de variable
 * Si style = true (defaut = false), le séparateur sera un : plutot qu'un =
 * Exemples:
 *      CYImplode(',', {nom:"john",prenom:"doe"}) => john,doe
 *      CYImplode(',', {nom:"john",prenom:"doe"},true) => nom=john,prenom=doe
 *      CYImplode(';', {top:"135px",display:"none"},true,true) => nom:john;prenom:doe
 */
function CYImplode(sep, obj, varname, style) {
    var ret = '';
    if (style === undefined)
        var style = "=";
    else
        style = ":";
    if (typeof obj == 'object') {
        for (var v in obj) {
            if (obj.hasOwnProperty(v)) {
                if (ret != '')
                    ret += sep;
                if (varname != undefined && varname == true)
                    ret += v + style;
                ret += obj[v];
            }
        }
    } else
        ret = obj;
    return ret;
}

/*
 *  Conversion d'une chaine en tableau de n éléments.
 *  Si n est présent et qu'il n'y a pas assez d'éléments dans la chaine, alors les éléments
 *  manquants seront crées et initialisés avec val
 *  Ex :
 *      CYExplode(",", "27,69,,34", 5, -1) => [ "27", "69", "", "34", -1 ]
 *      CYExplode(",", "27,69,-2", 5, "-1") => [ "27", "69", "-2", "-1", "-1" ]
 *      CYExplode(",", "", 2, 0) => [ 0, 0 ]
 */
function CYExplode(sep, chaine, n, val) {
    var ret = [];

    if (val == undefined)
        var val = '';
    if (chaine == undefined)
        var chaine = val;
    else if (typeof chaine !== "string")
        chaine += "";
    ret = chaine.split(sep);
    if (n == undefined)
        var n = ret.length;
    if (n < ret.length)
        n = ret.length;
    for (var i = 0; i < n; i++) {
        if (ret[i] == undefined)
            ret[i] = val;
    }
    return (ret);
}

/*
 * Remplacement de toutes les occurences de find par replace dans str et retour de str
 */
function CYReplaceAll(str, find, replace) {
    return str.replace(new RegExp(find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), 'g'), replace);
}
/*
 * Comme en C .. ;)
 */
function CYAtoI(str) {
    if (str == undefined)
        return (0);
    return (parseInt(str) || 0);
}
/*
 * Copie d'événements de source vers une ou plusieurs destinations
 */
function CYCopyEvents(source, destination) {
    var events;

    source = $(source).first();
    events = $(source).data('events');
    if (!events)
        return;
    $(destination).each(function () {
        var that = $(this);
        $.each(events, function (index, event) {
            $.each(event, function (i, v) {
                that.bind(v.type, v.handler);
            });
        });
    });
}
/*
 * Supprime le(s) tag(s) précisé de la chaine htmlstring
 * Exemple:
 *      CYRemoveTag("<span><i clas="fa fa-cog"></i>Mon texte</span>", "i") => "<span>Mon texte</span>"
 */
function CYRemoveTag(htmlstring, tag) {
    var wrapper = $("<div>" + htmlstring + "</div>");

    wrapper.find(tag).remove();
    return wrapper.html();
}
/*
 * Localstorage - cyptions peut etre une valeur de clé pour faire un get :
 *      var valeur = CYLocalStorage("keyvalue");
 * Similaire à
 *      var valeur = CYLocalStorage({ key: "keyvalue" });
 * sinon l'objet suivant :
 *      var valeur = CYLocalStorage(
 *      {
 *          command: "set" | "get" | "remove";
 *          key: "keyvalue",
 *          value: "...."
 *      }
 *  );
 *  Retourne la valeur en cas de get sinon chaine vide. Si pas de commande, je suppose un get :
 */
function CYLocalStorage(cyoptions) {
    var storage = window.localStorage;

    if (cyoptions === undefined || typeof (storage) === undefined)
        return "";
    if (typeof cyoptions != "object")
        return storage.getItem(cyoptions);
    switch (cyoptions.command) {
        case "set":
            storage.setItem(cyoptions.key, cyoptions.value);
            return "";
        case "remove":
            storage.removeItem(cyoptions.key);
            return "";
        case "get":
        default:
            return storage.getItem(cyoptions.key);
    }
}
/*
 * Vérifie si string email - retourne true ou false
 */
function CYCheckEmail(email) {
    return (email.search(/^[\._a-z0-9]([\._a-z0-9-]+)*@[a-z0-9][a-z0-9-]*(\.[a-z0-9][a-z0-9-]+)*[a-z0-9](\.[a-z]{2,})$/i) == 0);
}
/*
 * Affichage d'un PDF
 *   data doit etre de type arraybuffer lors du chargement avec CYExec()
 * Retourne l'objet qui pourra etre ensuite libere avec URL.revokeObjectURL(fileURL);
 */
function CYPdfViewer(id, data) {
    debug && console.log('CYJS : CYPdfviewer "' + id + '"');
    if (id.indexOf('#') != 0)
        id = "#" + id;
    var file = new Blob([data], { type: 'application/pdf' });
    var fileURL = URL.createObjectURL(file);
    PDFObject.embed(fileURL, id);
    return fileURL;
}
/*
 * Conversion UTF8 en BASE64
 *   CYBtoa('✓ à la mode') => "4pyTIMOgIGxhIG1vZGU="
 */
function CYBtoa(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}
/*
 * Conversion BASE64 en UTF8
 *   CYAtob('4pyTIMOgIGxhIG1vZGU='); // "✓ à la mode"
 */
function CYAtob(str) {
    return decodeURIComponent(escape(window.atob(str)));
}
/*
 * Recherche une propriété dans un objet ou ses fils et retourne sa valeur.
 */
function CYFindVal(object, key) {
    let value;
    Object.keys(object).some(function (k) {
        if (k === key) {
            value = object[k];
            return true;
        }
        if (object[k] && typeof object[k] === 'object') {
            value = CYFindVal(object[k], key);
            return value !== undefined;
        }
    });
    return value;
}
/*
 * Recherche une valeur dans un objet ou ses fils et retourne sa propriété.
 * Si sub = true, valeur pourra être une substring (default = false)
 * Si rec = true, la recherche se fera dans les fils (default = false)
 */
function CYFindKey(object, val, sub, rec) {
    let key;
    Object.keys(object).some(function (k) {
        if (rec != undefined && rec == true && object[k] && typeof object[k] === 'object') {
            key = CYFindKey(object[k], val, sub);
            return key !== undefined;
        }
        else if (typeof (object[k]) == "string") {
            if ((sub == undefined || !sub) && object[k].toLowerCase() == val.toLowerCase()) {
                key = k;
                return true;
            }
            else if (sub != undefined && sub && object[k].toLowerCase().includes(val.toLowerCase())) {
                key = k;
                return true;
            }
        }
    });
    return key;
}
/*
 * Clone un objet et le retourne
 */
function CYClone(object){
    if(object == null || typeof(object) != 'object')
        return object;
    let ret = object.constructor();
    for(var key in object)
        ret[key] = CYClone(object[key]);
    return ret;
}
/*
 * Conversion auto d'un nom d'icone font awesome. 
 * En entrée :
 *   "fa fa-calendar" ou "fas fa-calendar-days" : inchangé
 *   "fa-calendar" ou "calendar" : fa fa-calendar
 */
function CYFontAwesomeIconName(icon) {
    if (icon == undefined || icon == '')
        return '';
    if (icon.search(/^fa[a-z]{0,1} .*/i) >= 0)
        return (icon);
    if (icon.search(/^fa-.*/i) >= 0)
        return ("fa " + icon);
    return ("fa fa-" + icon);
}

/*
 * Applique la classe ClassBlink pendant dureeMS millisecondes
 * ou infini si dureeMS undefined
 * ou enlève la classe si dureeMS = 0
 */
function CYBlink(obj, dureeMS) {
    if (!dureeMS) {
        obj.removeClass("ClassBlink");
        return;
    }
    obj.addClass("ClassBlink");
    if (dureeMS != undefined) {
        setTimeout(function () {
            obj.removeClass("ClassBlink");
        }, dureeMS);
    }
}
/*
 * Crée une classe CSS ou un id CSS avec un id si précisé. L'id est nécessaire pour l'unicité de la classe.
 * Exemples :
 *   CYCreateCSS(".ui-dialog-on-top", "z-index: " + zindex, "ui-dialog-on-top");
 *   CYCreateCSS("#dialog", "z-index: " + zindex, "dialog");
 */
function CYCreateCSS(ruleName, rulesCSS, id) {
    let s;
    if(id == undefined)
        s = "<style>";
    else {
        // Pour unicité de la règle CSS
         s = "<style id='" + id + "'>";
         $("#" + id).remove();
    }
    $(s).prop("type", "text/css").html(ruleName + "{" + rulesCSS + "}").appendTo("head");
}