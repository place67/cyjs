{
    "context": [{
        "oFeatures": {
            "bAutoWidth": false,
            "bDeferRender": false,
            "bFilter": true,
            "bInfo": true,
            "bLengthChange": true,
            "bPaginate": true,
            "bProcessing": false,
            "bServerSide": false,
            "bSort": true,
            "bSortMulti": true,
            "bSortClasses": true,
            "bStateSave": null
        },
        "oScroll": {
            "bCollapse": false,
            "iBarWidth": 17,
            "sX": "",
            "sXInner": "",
            "sY": ""
        },
        "oLanguage": {
            "fnInfoCallback": null,
            "oAria": {
                "sSortAscending": ": activate to sort column ascending",
                "sSortDescending": ": activate to sort column descending",
                "_hungarianMap": {
                    "sortAscending": "sSortAscending",
                    "sortDescending": "sSortDescending"
                }
            },
            "oPaginate": {
                "sFirst": "First",
                "sLast": "Last",
                "sNext": "Next",
                "sPrevious": "Previous",
                "_hungarianMap": {
                    "first": "sFirst",
                    "last": "sLast",
                    "next": "sNext",
                    "previous": "sPrevious"
                }
            },
            "sEmptyTable": "No data available in table",
            "sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
            "sInfoEmpty": "Showing 0 to 0 of 0 entries",
            "sInfoFiltered": "(filtered from _MAX_ total entries)",
            "sInfoPostFix": "",
            "sDecimal": "",
            "sThousands": ",",
            "sLengthMenu": "Show _MENU_ entries",
            "sLoadingRecords": "Loading...",
            "sProcessing": "Processing...",
            "sSearch": "Search:",
            "sSearchPlaceholder": "",
            "sUrl": "/myJSFW/js/lang/fr.json",
            "sZeroRecords": "No matching records found",
            "_hungarianMap": {
                "aria": "oAria",
                "paginate": "oPaginate",
                "emptyTable": "sEmptyTable",
                "info": "sInfo",
                "infoEmpty": "sInfoEmpty",
                "infoFiltered": "sInfoFiltered",
                "infoPostFix": "sInfoPostFix",
                "decimal": "sDecimal",
                "thousands": "sThousands",
                "lengthMenu": "sLengthMenu",
                "loadingRecords": "sLoadingRecords",
                "processing": "sProcessing",
                "search": "sSearch",
                "searchPlaceholder": "sSearchPlaceholder",
                "url": "sUrl",
                "zeroRecords": "sZeroRecords"
            },
            "url": "/myJSFW/js/lang/fr.json"
        },
        "oBrowser": {
            "bScrollOversize": false,
            "bScrollbarLeft": false,
            "bBounding": true,
            "barWidth": 17
        },
        "ajax": null,
        "aanFeatures": [],
        "aoData": [{
            "nTr": {
                "_DT_RowIndex": 0
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 0,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "1",
                "societe": "CYSTEME",
                "adresse": "67 RUE VIRGILE",
                "cp": "67200",
                "ville": "STRASBOURG",
                "tel": "+33 981 35 89 59",
                "fax": "",
                "email": "luc@cysteme.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 0
        },
        {
            "nTr": {
                "_DT_RowIndex": 1
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 1,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "2",
                "societe": "SCHROLL",
                "adresse": "PORT DU RHIN",
                "cp": "67000",
                "ville": "STRASBOURG",
                "tel": "",
                "fax": "",
                "email": "schroll.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 1
        },
        {
            "nTr": {
                "_DT_RowIndex": 2
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 2,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "3",
                "societe": "GLOBAL EXPO",
                "adresse": "RUE DU DOME",
                "cp": "67000",
                "ville": "STRASBOURG",
                "tel": " 33 6 58 68 42 42",
                "fax": "",
                "email": "globalexpo.fr",
                "siret": "SIRET",
                "ape": "APE",
                "ctime": "2016-08-19 19:56:50"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 2
        },
        {
            "nTr": {
                "_DT_RowIndex": 3
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 3,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "4",
                "societe": "DUPOND",
                "adresse": "7 Rue Dupond",
                "cp": "78000",
                "ville": "DUPONDVILLE",
                "tel": "01 23 45 67 89",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 3
        },
        {
            "nTr": {
                "_DT_RowIndex": 4
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 4,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "5",
                "societe": "DURAND",
                "adresse": "78 Rue Durand",
                "cp": "54008",
                "ville": "DURANDVILLE",
                "tel": "01 32 54 67 98",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 4
        },
        {
            "nTr": {
                "_DT_RowIndex": 5
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 5,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "6",
                "societe": "SCMITT",
                "adresse": "98 Rue Schmitt",
                "cp": "67045",
                "ville": "SCHMITVILLE",
                "tel": "98 74 56 32 10",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 5
        },
        {
            "nTr": {
                "_DT_RowIndex": 6
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 6,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "7",
                "societe": "LEGRAND",
                "adresse": "9 Rue Legrand",
                "cp": "67000",
                "ville": "LEGRANDVILLE",
                "tel": "56 89 78 41 23",
                "fax": "",
                "email": "legrand.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 6
        },
        {
            "nTr": {
                "_DT_RowIndex": 7
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 7,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "15",
                "societe": "Zorro",
                "adresse": "abcd",
                "cp": "67000",
                "ville": "ABCD",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 17:28:12"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 7
        },
        {
            "nTr": {
                "_DT_RowIndex": 8
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 8,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "23",
                "societe": "Bouygues",
                "adresse": "Bordeaux",
                "cp": "12345",
                "ville": "paris",
                "tel": "11 12 32 34 23",
                "fax": "99 99 99 99 99",
                "email": "ai@er.fr",
                "siret": "SSS",
                "ape": "",
                "ctime": "2016-08-20 23:55:35"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 8
        },
        {
            "nTr": {
                "_DT_RowIndex": 9
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 9,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "29",
                "societe": "Azerty",
                "adresse": "qefgqdfgqsd",
                "cp": "67000",
                "ville": "Strasbourg",
                "tel": "",
                "fax": "",
                "email": "stbg.fr",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 17:25:21"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 9
        },
        {
            "nTr": {
                "_DT_RowIndex": 10
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 10,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "36",
                "societe": "zzeerrtt",
                "adresse": "hhggggggg",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 10
        },
        {
            "nTr": {
                "_DT_RowIndex": 11
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 11,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "37",
                "societe": "AZUR",
                "adresse": "qegegf",
                "cp": "",
                "ville": "aaazzzzz",
                "tel": "",
                "fax": "",
                "email": "amer",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 18:33:17"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 11
        },
        {
            "nTr": {
                "_DT_RowIndex": 12
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 12,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "41",
                "societe": "sfgbsfgb",
                "adresse": "",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 12
        },
        {
            "nTr": {
                "_DT_RowIndex": 13
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 13,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "43",
                "societe": "super u",
                "adresse": "rue",
                "cp": "67890",
                "ville": "wolfisheim",
                "tel": "11 22 33 45 56",
                "fax": "22 33 44 55 66",
                "email": "superugmail.com",
                "siret": "44",
                "ape": "55",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 13
        },
        {
            "nTr": {
                "_DT_RowIndex": 14
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 14,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "47",
                "societe": "CORA",
                "adresse": "JE sais pas",
                "cp": "",
                "ville": "VENDENHEIM",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 14
        },
        {
            "nTr": {
                "_DT_RowIndex": 15
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 15,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "54",
                "societe": "artichaut",
                "adresse": "CHAUD",
                "cp": "",
                "ville": "ssssssssss",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 19:08:14"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 15
        },
        {
            "nTr": {
                "_DT_RowIndex": 16
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 16,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "55",
                "societe": "CORA1",
                "adresse": "",
                "cp": "",
                "ville": "Mundolsheim",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-20 20:09:47"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 16
        },
        {
            "nTr": {
                "_DT_RowIndex": 17
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 17,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "56",
                "societe": "CORA2",
                "adresse": "adresse",
                "cp": "67889",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "aa",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-20 01:41:58"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 17
        },
        {
            "nTr": {
                "_DT_RowIndex": 18
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 18,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "65",
                "societe": "CORA3",
                "adresse": "",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 18
        },
        {
            "nTr": {
                "_DT_RowIndex": 19
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 19,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "67",
                "societe": "schmitdddd",
                "adresse": "dfgqdfgqsdfsdf",
                "cp": "",
                "ville": "qdfgdfgqd",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "qdfgdfg",
                "ctime": "2016-08-19 10:08:01"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 19
        },
        {
            "nTr": {
                "_DT_RowIndex": 20
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 20,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "71",
                "societe": "ALAIN",
                "adresse": "sfghfg",
                "cp": "78000",
                "ville": "RAMBOUILLET",
                "tel": "",
                "fax": "",
                "email": "AAAOOO@ici.fr",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-21 12:16:28"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 20
        },
        {
            "nTr": {
                "_DT_RowIndex": 21
            },
            "anCells": [{
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 0
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 1
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 2
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 3
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 4
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 5
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 6
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 7
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 8
                }
            },
            {
                "_DT_CellIndex": {
                    "row": 21,
                    "column": 9
                }
            }],
            "_aData": {
                "id": "72",
                "societe": "ABSSSS",
                "adresse": "ici",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "la",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 19:52:58"
            },
            "_aSortData": null,
            "_aFilterData": null,
            "_sFilterRow": null,
            "_sRowStripe": "",
            "src": "data",
            "idx": 21
        }],
        "aiDisplay": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        "aiDisplayMaster": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        "aIds": {},
        "aoColumns": [{
            "idx": 0,
            "aDataSort": [0],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": true,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "societe",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {
                    "filter": true,
                    "maxlength": 50
                },
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "SOCIETE",
            "sType": null,
            "sWidth": "14%",
            "sWidthOrig": "14%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "filter": true,
            "maxlength": 50,
            "_setter": null,
            "width": "14%",
            "data": "societe",
            "searchable": true,
            "visible": true,
            "length": 20
        },
        {
            "idx": 1,
            "aDataSort": [1],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": true,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "adresse",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {
                    "maxlength": 50
                },
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "ADRESSE",
            "sType": null,
            "sWidth": "12%",
            "sWidthOrig": "12%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "maxlength": 50,
            "_setter": null,
            "width": "12%",
            "data": "adresse",
            "searchable": true,
            "visible": true,
            "length": 50
        },
        {
            "idx": 2,
            "aDataSort": [2],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": false,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "cp",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {
                    "maxlength": 10,
                    "mask": 99999
                },
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "CP",
            "sType": null,
            "sWidth": "5%",
            "sWidthOrig": "5%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "maxlength": 10,
            "mask": 99999,
            "_setter": null,
            "width": "5%",
            "data": "cp",
            "searchable": true,
            "visible": false
        },
        {
            "idx": 3,
            "aDataSort": [3],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": true,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "ville",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {
                    "maxlength": 50
                },
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "VILLE",
            "sType": null,
            "sWidth": "14%",
            "sWidthOrig": "14%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "maxlength": 50,
            "_setter": null,
            "width": "14%",
            "data": "ville",
            "searchable": true,
            "visible": true
        },
        {
            "idx": 4,
            "aDataSort": [4],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": true,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "tel",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {},
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "TELEPHONE",
            "sType": null,
            "sWidth": "10%",
            "sWidthOrig": "10%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "_setter": null,
            "width": "10%",
            "data": "tel",
            "searchable": true,
            "visible": true
        },
        {
            "idx": 5,
            "aDataSort": [5],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": false,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "fax",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {},
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "Fax",
            "sType": null,
            "sWidth": "10%",
            "sWidthOrig": "10%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "_setter": null,
            "width": "10%",
            "data": "fax",
            "searchable": true,
            "visible": false
        },
        {
            "idx": 6,
            "aDataSort": [6],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": true,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "email",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {},
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "EMAIL",
            "sType": null,
            "sWidth": "15%",
            "sWidthOrig": "15%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "_setter": null,
            "width": "15%",
            "data": "email",
            "searchable": true,
            "visible": true
        },
        {
            "idx": 7,
            "aDataSort": [7],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": false,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "siret",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {},
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "SIRET",
            "sType": null,
            "sWidth": "10%",
            "sWidthOrig": "10%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "_setter": null,
            "width": "10%",
            "data": "siret",
            "searchable": true,
            "visible": false
        },
        {
            "idx": 8,
            "aDataSort": [8],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": true,
            "bVisible": false,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "ape",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {},
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting",
            "sSortingClassJUI": "",
            "sTitle": "APE",
            "sType": null,
            "sWidth": "6%",
            "sWidthOrig": "6%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "_setter": null,
            "width": "6%",
            "data": "ape",
            "searchable": true,
            "visible": false
        },
        {
            "idx": 9,
            "aDataSort": [9],
            "asSorting": ["asc", "desc"],
            "bSearchable": true,
            "bSortable": false,
            "bVisible": false,
            "_sManualType": null,
            "_bAttrSrc": false,
            "fnCreatedCell": null,
            "mData": "id",
            "mRender": null,
            "nTh": {
                "jQuery310082950283955501992": {},
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "nTf": null,
            "sClass": "",
            "sContentPadding": "",
            "sDefaultContent": null,
            "sName": "",
            "sSortDataType": "std",
            "sSortingClass": "sorting_disabled",
            "sSortingClassJUI": "",
            "sTitle": "Id",
            "sType": null,
            "sWidth": "4%",
            "sWidthOrig": "4%",
            "iDataSort": -1,
            "sCellType": "td",
            "_hungarianMap": {
                "dataSort": "iDataSort",
                "sorting": "asSorting",
                "searchable": "bSearchable",
                "sortable": "bSortable",
                "visible": "bVisible",
                "createdCell": "fnCreatedCell",
                "data": "mData",
                "render": "mRender",
                "cellType": "sCellType",
                "class": "sClass",
                "contentPadding": "sContentPadding",
                "defaultContent": "sDefaultContent",
                "name": "sName",
                "sortDataType": "sSortDataType",
                "title": "sTitle",
                "type": "sType",
                "width": "sWidth"
            },
            "_setter": null,
            "width": "4%",
            "data": "id",
            "searchable": true,
            "visible": false,
            "orderable": false
        }],
        "aoHeader": [
            [{
                "cell": {
                    "jQuery310082950283955501992": {
                        "filter": true,
                        "maxlength": 50
                    },
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {
                        "maxlength": 50
                    },
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {
                        "maxlength": 10,
                        "mask": 99999
                    },
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {
                        "maxlength": 50
                    },
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {},
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {},
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {},
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {},
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {},
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            },
            {
                "cell": {
                    "jQuery310082950283955501992": {},
                    "jQuery310082950283955501991": {
                        "hasDataAttrs": true
                    }
                },
                "unique": true
            }]
        ],
        "aoFooter": [],
        "oPreviousSearch": {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true,
            "_hungarianMap": {
                "caseInsensitive": "bCaseInsensitive",
                "search": "sSearch",
                "regex": "bRegex",
                "smart": "bSmart"
            }
        },
        "aoPreSearchCols": [{
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        },
        {
            "bCaseInsensitive": true,
            "sSearch": "",
            "bRegex": false,
            "bSmart": true
        }],
        "aaSorting": [
            [0, "asc"]
        ],
        "aaSortingFixed": [],
        "asStripeClasses": ["odd", "even"],
        "asDestroyStripes": [],
        "sDestroyWidth": "",
        "aoRowCallback": [],
        "aoHeaderCallback": [],
        "aoFooterCallback": [],
        "aoDrawCallback": [{
            "sName": "user"
        },
        {},
        {
            "sName": "sc"
        }],
        "aoRowCreatedCallback": [],
        "aoPreDrawCallback": [],
        "aoInitComplete": [{
            "sName": "user"
        }],
        "aoStateSaveParams": [],
        "aoStateLoadParams": [],
        "aoStateLoaded": [],
        "sTableId": "IDtableclients",
        "nTable": {
            "jQuery310082950283955501992": {
                "pageLength": 5,
                "paging": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "order": [
                    [0, "asc"]
                ],
                "selectcolumns": true,
                "selectauto": false
            },
            "jQuery310082950283955501991": {
                "hasDataAttrs": true
            }
        },
        "nTHead": {},
        "nTFoot": null,
        "nTBody": {
            "jQuery310082950283955501991": {
                "events": {
                    "click": [{
                        "type": "click",
                        "origType": "click",
                        "guid": 119,
                        "selector": "tr",
                        "needsContext": false,
                        "namespace": ""
                    }]
                }
            }
        },
        "nTableWrapper": null,
        "bDeferLoading": false,
        "bInitialised": true,
        "aoOpenRows": [],
        "sDom": "lfrtip",
        "searchDelay": null,
        "sPaginationType": "simple_numbers",
        "iStateDuration": 7200,
        "aoStateSave": [],
        "aoStateLoad": [],
        "oSavedState": null,
        "oLoadedState": null,
        "sAjaxSource": null,
        "sAjaxDataProp": "data",
        "bAjaxDataGet": true,
        "jqXHR": null,
        "fnServerData": null,
        "aoServerParams": [],
        "sServerMethod": "GET",
        "aLengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "Tout"]
        ],
        "iDraw": 0,
        "bDrawing": false,
        "iDrawError": -1,
        "_iDisplayLength": 5,
        "_iDisplayStart": 0,
        "_iRecordsTotal": 0,
        "_iRecordsDisplay": 0,
        "bJUI": false,
        "oClasses": {
            "sTable": "dataTable",
            "sNoFooter": "no-footer",
            "sPageButton": "paginate_button",
            "sPageButtonActive": "current",
            "sPageButtonDisabled": "disabled",
            "sStripeOdd": "odd",
            "sStripeEven": "even",
            "sRowEmpty": "dataTables_empty",
            "sWrapper": "dataTables_wrapper",
            "sFilter": "dataTables_filter",
            "sInfo": "dataTables_info",
            "sPaging": "dataTables_paginate paging_",
            "sLength": "dataTables_length",
            "sProcessing": "dataTables_processing",
            "sSortAsc": "sorting_asc",
            "sSortDesc": "sorting_desc",
            "sSortable": "sorting",
            "sSortableAsc": "sorting_asc_disabled",
            "sSortableDesc": "sorting_desc_disabled",
            "sSortableNone": "sorting_disabled",
            "sSortColumn": "sorting_",
            "sFilterInput": "",
            "sLengthSelect": "",
            "sScrollWrapper": "dataTables_scroll",
            "sScrollHead": "dataTables_scrollHead",
            "sScrollHeadInner": "dataTables_scrollHeadInner",
            "sScrollBody": "dataTables_scrollBody",
            "sScrollFoot": "dataTables_scrollFoot",
            "sScrollFootInner": "dataTables_scrollFootInner",
            "sHeaderTH": "",
            "sFooterTH": "",
            "sSortJUIAsc": "",
            "sSortJUIDesc": "",
            "sSortJUI": "",
            "sSortJUIAscAllowed": "",
            "sSortJUIDescAllowed": "",
            "sSortJUIWrapper": "",
            "sSortIcon": "",
            "sJUIHeader": "",
            "sJUIFooter": "",
            "_hungarianMap": {}
        },
        "bFiltered": false,
        "bSorted": false,
        "bSortCellsTop": false,
        "oInit": {
            "columns": [{
                "width": "14%",
                "data": "societe",
                "searchable": true,
                "visible": true,
                "length": 20,
                "sWidth": "14%",
                "mData": "societe",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "12%",
                "data": "adresse",
                "searchable": true,
                "visible": true,
                "length": 50,
                "sWidth": "12%",
                "mData": "adresse",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "5%",
                "data": "cp",
                "searchable": true,
                "visible": false,
                "sWidth": "5%",
                "mData": "cp",
                "bSearchable": true,
                "bVisible": false
            },
            {
                "width": "14%",
                "data": "ville",
                "searchable": true,
                "visible": true,
                "sWidth": "14%",
                "mData": "ville",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "10%",
                "data": "tel",
                "searchable": true,
                "visible": true,
                "sWidth": "10%",
                "mData": "tel",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "10%",
                "data": "fax",
                "searchable": true,
                "visible": false,
                "sWidth": "10%",
                "mData": "fax",
                "bSearchable": true,
                "bVisible": false
            },
            {
                "width": "15%",
                "data": "email",
                "searchable": true,
                "visible": true,
                "sWidth": "15%",
                "mData": "email",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "10%",
                "data": "siret",
                "searchable": true,
                "visible": false,
                "sWidth": "10%",
                "mData": "siret",
                "bSearchable": true,
                "bVisible": false
            },
            {
                "width": "6%",
                "data": "ape",
                "searchable": true,
                "visible": false,
                "sWidth": "6%",
                "mData": "ape",
                "bSearchable": true,
                "bVisible": false
            },
            {
                "width": "4%",
                "data": "id",
                "searchable": true,
                "visible": false,
                "orderable": false,
                "bSortable": false,
                "sWidth": "4%",
                "mData": "id",
                "bSearchable": true,
                "bVisible": false
            }],
            "data": [{
                "id": "1",
                "societe": "CYSTEME",
                "adresse": "67 RUE VIRGILE",
                "cp": "67200",
                "ville": "STRASBOURG",
                "tel": "+33 981 35 89 59",
                "fax": "",
                "email": "luc@cysteme.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "2",
                "societe": "SCHROLL",
                "adresse": "PORT DU RHIN",
                "cp": "67000",
                "ville": "STRASBOURG",
                "tel": "",
                "fax": "",
                "email": "schroll.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "3",
                "societe": "GLOBAL EXPO",
                "adresse": "RUE DU DOME",
                "cp": "67000",
                "ville": "STRASBOURG",
                "tel": " 33 6 58 68 42 42",
                "fax": "",
                "email": "globalexpo.fr",
                "siret": "SIRET",
                "ape": "APE",
                "ctime": "2016-08-19 19:56:50"
            },
            {
                "id": "4",
                "societe": "DUPOND",
                "adresse": "7 Rue Dupond",
                "cp": "78000",
                "ville": "DUPONDVILLE",
                "tel": "01 23 45 67 89",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "5",
                "societe": "DURAND",
                "adresse": "78 Rue Durand",
                "cp": "54008",
                "ville": "DURANDVILLE",
                "tel": "01 32 54 67 98",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "6",
                "societe": "SCMITT",
                "adresse": "98 Rue Schmitt",
                "cp": "67045",
                "ville": "SCHMITVILLE",
                "tel": "98 74 56 32 10",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "7",
                "societe": "LEGRAND",
                "adresse": "9 Rue Legrand",
                "cp": "67000",
                "ville": "LEGRANDVILLE",
                "tel": "56 89 78 41 23",
                "fax": "",
                "email": "legrand.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "15",
                "societe": "Zorro",
                "adresse": "abcd",
                "cp": "67000",
                "ville": "ABCD",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 17:28:12"
            },
            {
                "id": "23",
                "societe": "Bouygues",
                "adresse": "Bordeaux",
                "cp": "12345",
                "ville": "paris",
                "tel": "11 12 32 34 23",
                "fax": "99 99 99 99 99",
                "email": "ai@er.fr",
                "siret": "SSS",
                "ape": "",
                "ctime": "2016-08-20 23:55:35"
            },
            {
                "id": "29",
                "societe": "Azerty",
                "adresse": "qefgqdfgqsd",
                "cp": "67000",
                "ville": "Strasbourg",
                "tel": "",
                "fax": "",
                "email": "stbg.fr",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 17:25:21"
            },
            {
                "id": "36",
                "societe": "zzeerrtt",
                "adresse": "hhggggggg",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "37",
                "societe": "AZUR",
                "adresse": "qegegf",
                "cp": "",
                "ville": "aaazzzzz",
                "tel": "",
                "fax": "",
                "email": "amer",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 18:33:17"
            },
            {
                "id": "41",
                "societe": "sfgbsfgb",
                "adresse": "",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "43",
                "societe": "super u",
                "adresse": "rue",
                "cp": "67890",
                "ville": "wolfisheim",
                "tel": "11 22 33 45 56",
                "fax": "22 33 44 55 66",
                "email": "superugmail.com",
                "siret": "44",
                "ape": "55",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "47",
                "societe": "CORA",
                "adresse": "JE sais pas",
                "cp": "",
                "ville": "VENDENHEIM",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "54",
                "societe": "artichaut",
                "adresse": "CHAUD",
                "cp": "",
                "ville": "ssssssssss",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 19:08:14"
            },
            {
                "id": "55",
                "societe": "CORA1",
                "adresse": "",
                "cp": "",
                "ville": "Mundolsheim",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-20 20:09:47"
            },
            {
                "id": "56",
                "societe": "CORA2",
                "adresse": "adresse",
                "cp": "67889",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "aa",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-20 01:41:58"
            },
            {
                "id": "65",
                "societe": "CORA3",
                "adresse": "",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "67",
                "societe": "schmitdddd",
                "adresse": "dfgqdfgqsdfsdf",
                "cp": "",
                "ville": "qdfgdfgqd",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "qdfgdfg",
                "ctime": "2016-08-19 10:08:01"
            },
            {
                "id": "71",
                "societe": "ALAIN",
                "adresse": "sfghfg",
                "cp": "78000",
                "ville": "RAMBOUILLET",
                "tel": "",
                "fax": "",
                "email": "AAAOOO@ici.fr",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-21 12:16:28"
            },
            {
                "id": "72",
                "societe": "ABSSSS",
                "adresse": "ici",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "la",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 19:52:58"
            }],
            "pageLength": 5,
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "order": [
                [0, "asc"]
            ],
            "selectcolumns": true,
            "selectauto": false,
            "aoColumns": [{
                "width": "14%",
                "data": "societe",
                "searchable": true,
                "visible": true,
                "length": 20,
                "sWidth": "14%",
                "mData": "societe",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "12%",
                "data": "adresse",
                "searchable": true,
                "visible": true,
                "length": 50,
                "sWidth": "12%",
                "mData": "adresse",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "5%",
                "data": "cp",
                "searchable": true,
                "visible": false,
                "sWidth": "5%",
                "mData": "cp",
                "bSearchable": true,
                "bVisible": false
            },
            {
                "width": "14%",
                "data": "ville",
                "searchable": true,
                "visible": true,
                "sWidth": "14%",
                "mData": "ville",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "10%",
                "data": "tel",
                "searchable": true,
                "visible": true,
                "sWidth": "10%",
                "mData": "tel",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "10%",
                "data": "fax",
                "searchable": true,
                "visible": false,
                "sWidth": "10%",
                "mData": "fax",
                "bSearchable": true,
                "bVisible": false
            },
            {
                "width": "15%",
                "data": "email",
                "searchable": true,
                "visible": true,
                "sWidth": "15%",
                "mData": "email",
                "bSearchable": true,
                "bVisible": true
            },
            {
                "width": "10%",
                "data": "siret",
                "searchable": true,
                "visible": false,
                "sWidth": "10%",
                "mData": "siret",
                "bSearchable": true,
                "bVisible": false
            },
            {
                "width": "6%",
                "data": "ape",
                "searchable": true,
                "visible": false,
                "sWidth": "6%",
                "mData": "ape",
                "bSearchable": true,
                "bVisible": false
            },
            {
                "width": "4%",
                "data": "id",
                "searchable": true,
                "visible": false,
                "orderable": false,
                "bSortable": false,
                "sWidth": "4%",
                "mData": "id",
                "bSearchable": true,
                "bVisible": false
            }],
            "aaData": [{
                "id": "1",
                "societe": "CYSTEME",
                "adresse": "67 RUE VIRGILE",
                "cp": "67200",
                "ville": "STRASBOURG",
                "tel": "+33 981 35 89 59",
                "fax": "",
                "email": "luc@cysteme.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "2",
                "societe": "SCHROLL",
                "adresse": "PORT DU RHIN",
                "cp": "67000",
                "ville": "STRASBOURG",
                "tel": "",
                "fax": "",
                "email": "schroll.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "3",
                "societe": "GLOBAL EXPO",
                "adresse": "RUE DU DOME",
                "cp": "67000",
                "ville": "STRASBOURG",
                "tel": " 33 6 58 68 42 42",
                "fax": "",
                "email": "globalexpo.fr",
                "siret": "SIRET",
                "ape": "APE",
                "ctime": "2016-08-19 19:56:50"
            },
            {
                "id": "4",
                "societe": "DUPOND",
                "adresse": "7 Rue Dupond",
                "cp": "78000",
                "ville": "DUPONDVILLE",
                "tel": "01 23 45 67 89",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "5",
                "societe": "DURAND",
                "adresse": "78 Rue Durand",
                "cp": "54008",
                "ville": "DURANDVILLE",
                "tel": "01 32 54 67 98",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "6",
                "societe": "SCMITT",
                "adresse": "98 Rue Schmitt",
                "cp": "67045",
                "ville": "SCHMITVILLE",
                "tel": "98 74 56 32 10",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "7",
                "societe": "LEGRAND",
                "adresse": "9 Rue Legrand",
                "cp": "67000",
                "ville": "LEGRANDVILLE",
                "tel": "56 89 78 41 23",
                "fax": "",
                "email": "legrand.fr",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "15",
                "societe": "Zorro",
                "adresse": "abcd",
                "cp": "67000",
                "ville": "ABCD",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 17:28:12"
            },
            {
                "id": "23",
                "societe": "Bouygues",
                "adresse": "Bordeaux",
                "cp": "12345",
                "ville": "paris",
                "tel": "11 12 32 34 23",
                "fax": "99 99 99 99 99",
                "email": "ai@er.fr",
                "siret": "SSS",
                "ape": "",
                "ctime": "2016-08-20 23:55:35"
            },
            {
                "id": "29",
                "societe": "Azerty",
                "adresse": "qefgqdfgqsd",
                "cp": "67000",
                "ville": "Strasbourg",
                "tel": "",
                "fax": "",
                "email": "stbg.fr",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 17:25:21"
            },
            {
                "id": "36",
                "societe": "zzeerrtt",
                "adresse": "hhggggggg",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "37",
                "societe": "AZUR",
                "adresse": "qegegf",
                "cp": "",
                "ville": "aaazzzzz",
                "tel": "",
                "fax": "",
                "email": "amer",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 18:33:17"
            },
            {
                "id": "41",
                "societe": "sfgbsfgb",
                "adresse": "",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "43",
                "societe": "super u",
                "adresse": "rue",
                "cp": "67890",
                "ville": "wolfisheim",
                "tel": "11 22 33 45 56",
                "fax": "22 33 44 55 66",
                "email": "superugmail.com",
                "siret": "44",
                "ape": "55",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "47",
                "societe": "CORA",
                "adresse": "JE sais pas",
                "cp": "",
                "ville": "VENDENHEIM",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "54",
                "societe": "artichaut",
                "adresse": "CHAUD",
                "cp": "",
                "ville": "ssssssssss",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 19:08:14"
            },
            {
                "id": "55",
                "societe": "CORA1",
                "adresse": "",
                "cp": "",
                "ville": "Mundolsheim",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-20 20:09:47"
            },
            {
                "id": "56",
                "societe": "CORA2",
                "adresse": "adresse",
                "cp": "67889",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "aa",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-20 01:41:58"
            },
            {
                "id": "65",
                "societe": "CORA3",
                "adresse": "",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "",
                "ctime": "0000-00-00 00:00:00"
            },
            {
                "id": "67",
                "societe": "schmitdddd",
                "adresse": "dfgqdfgqsdfsdf",
                "cp": "",
                "ville": "qdfgdfgqd",
                "tel": "",
                "fax": "",
                "email": "",
                "siret": "",
                "ape": "qdfgdfg",
                "ctime": "2016-08-19 10:08:01"
            },
            {
                "id": "71",
                "societe": "ALAIN",
                "adresse": "sfghfg",
                "cp": "78000",
                "ville": "RAMBOUILLET",
                "tel": "",
                "fax": "",
                "email": "AAAOOO@ici.fr",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-21 12:16:28"
            },
            {
                "id": "72",
                "societe": "ABSSSS",
                "adresse": "ici",
                "cp": "",
                "ville": "",
                "tel": "",
                "fax": "",
                "email": "la",
                "siret": "",
                "ape": "",
                "ctime": "2016-08-19 19:52:58"
            }],
            "bInfo": true,
            "bSort": true,
            "aaSorting": [
                [0, "asc"]
            ],
            "bPaginate": true,
            "iDisplayLength": 5,
            "bFilter": true
        },
        "aoDestroyCallback": [],
        "oInstance": {
            "0": {
                "jQuery310082950283955501992": {
                    "pageLength": 5,
                    "paging": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "order": [
                        [0, "asc"]
                    ],
                    "selectcolumns": true,
                    "selectauto": false
                },
                "jQuery310082950283955501991": {
                    "hasDataAttrs": true
                }
            },
            "length": 1,
            "internal": {},
            "oApi": {}
        },
        "sInstance": "IDtableclients",
        "iTabIndex": 0,
        "nScrollHead": null,
        "nScrollFoot": null,
        "aLastSort": [{
            "src": 0,
            "col": 0,
            "dir": "asc",
            "index": 0,
            "type": "string"
        }],
        "oPlugins": {},
        "rowId": "DT_RowId",
        "oApi": {},
        "renderer": null,
        "iInitDisplayStart": 0,
        "_rowReadObject": true
    }],
    "selector": {
        "rows": null,
        "cols": null,
        "opts": null
    },
    "ajax": {
        "__dt_wrapper": true
    }
}