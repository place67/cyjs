<?php

if ($argc < 3)
    usage();
switch($argv[1])
{
    case 'page' :
        if ($argv[2] == "")
            error ("Page name missing");
        $html = '<div class="ClassRow">
    <div class="ClassCell">
        Content page here.
    </div>
</div>
';
        createfile("html/" . $argv[2] . ".html", $html);
        $js = '/*
 * ' . $argv[2] . '.js
 */

function CyInit_' . $argv[2] . '()
{
    CYInitAllTools();
}
';
        createfile("js/" . $argv[2] . ".js", $js); 
        break;
    default :
        usage();
}

function usage()
{
    global $argv;
    
    echo "Create a new page : $argv[0] page newpagename";
    echo "\n";
    exit;
}

function error($msg)
{
    echo $msg;
    exit;
}

function createfile($file, $content)
{
    $fp = fopen($file, "w");
    fwrite($fp, $content);
    fclose($fp);
}

?>
