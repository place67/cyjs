/*
 * cyjscrm.js
 */

function CyInit_cyjscrm() {
    CYMainTitle('CRM Demo', 'address-card-o');

    CYOpenSideMenu(true, false, true);
    CYInitAllTools();
    var tablecompanies = CYDataTable("#IDcompanies", '', true);
    var tablecontacts = CYDataTable("#IDcontacts", '');

    // Dropzone - file list
    var mydropzone = CYDropZone("#crmdropzone", {}, function(file) {
        ListDocuments(currentclient);
    });
    mydropzone.removeAllFiles();
    mydropzone.enable();
    // Url for file uploading
    mydropzone.options.url = cyServerName + cyPath + cyWebSvc + "?r=upload&dir=" + currentclient;

    // Clic ligne
    CYDataTableSelectLine("#IDcompanies", function(row) {
        currentclient = row[0];
        ListDocuments(currentclient);
        mydropzone.options.url = cyServerName + cyPath + cyWebSvc + "?r=upload&dir=" + currentclient;
        console.log('currentclient=' + currentclient);
    });

    ListDocuments(currentclient);

    CYCalendar("#crmlist", {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        editable: true,
        droppable: true,
        height: 300,
        header: {
            left: 'listDay,listWeek,listMonth',
            center: 'title',
            right: 'today prevYear,prev,next,nextYear'
        },
    });
    // Initialize the droppable external events
    $('.ClassCalEventP').each(function() {
        var thisevent = this;
        var title = $.trim(CYTranslateString($(thisevent).text()));
        var eventdata = { title: title, eventType: $(thisevent).data('eventType'), duration: $(thisevent).data('eventDuration') };
        // Store 'title' and 'eventType' and 'eventDuration' so the calendar knows to render an event upon drop       
        $(thisevent).data('event-data', eventdata);
        $(thisevent).text(title);
        $(thisevent).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 0
        });
    });

    setTimeout(function() {
        CYCalendar("#crmagenda", {
                schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                // now: "2017-08-01",
                editable: true,
                droppable: true,
                dropAccept: ".ClassCalEventP",
                weekNumbers: true,
                minTime: "07:00:00",
                maxTime: "22:00:00",
                defaultTimedEventDuration: "01:00:00",
                height: "auto",
                eventTextColor: "#333",
                aspectRatio: 4,
                /*visibleRange: {
                    start: '2017-07-01',
                    end: '2017-07-05'
                },*/
                businessHours: {
                    dow: [1, 2, 3, 4, 5], // Lundi - Vendredi
                    start: '09:00',
                    end: '18:00'
                },
                header: {
                    left: 'agenda5Days, agendaDay,agendaWeek,month',
                    center: 'title',
                    right: 'today prevYear,prev,next,nextYear'
                },
                views: {
                    agenda5Days: {
                        type: 'agendaWeek',
                        duration: { days: 7 },
                        weekends: false,
                        firstDay: 1,
                        //hiddenDays: [3], // Wednesday hidden
                        buttonText: CYTranslateString("5 days")
                    }
                },
                dayClick: function(date, jsevent, view) {
                    InputEvent(date, false, jsevent, view); // Defined in cyjsui.js
                },
                eventClick: function(event, element) {
                    UpdateEvent(event, element); // Defined in cyjsui.js
                },
                eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
                    return true;
                },
                drop: function(date, jsEvent, ui, resourceId) {
                    var event = $(this).data("event-data");
                    var duration = CYExplode(":", event.duration, 2, 0);
                    event.resourceId = resourceId;
                    event.start = date;
                    event.end = moment(date).add(duration[0], 'h').add(duration[1], 'm');
                    event.backgroundColor = $(".ClassCalEventP[data-event-type='" + event.eventType + "']").css("background-color");
                    event.borderColor = $(".ClassCalEventP[data-event-type='" + event.eventType + "']").css("border-color");
                    event.textColor = $(".ClassCalEventP[data-event-type='" + event.eventType + "']").css("color");
                    if (event.eventType == "E" && moment(date).format("HH") < 18)
                        CYAlert("A bit early for dinner");
                    else {
                        $("#crmagenda").fullCalendar('renderEvent', event, true);
                        $("#crmlist").fullCalendar('renderEvent', event, true);
                        if ($('#IDUniqueEvent').val() == 1)
                            $(this).remove();
                    }
                }
            })
            // Setting events array
        SetDefaultEvents();
    }, 1000);
}