/*
 * rollmenu.js
 */
var cyRollMenuWidth = 0;

function CyInit_rollmenu()
{
    debug && console.log('rollmenu.js : CyInit_rollmenu');

    $(".ClassContextMenu").remove();
    $(".ClassMenu").hide();
    $("#IDrollmenu").menu();
    $("#IDiconrollmenu").on("click", function (event)
    {
        event.stopPropagation();
        var right = CYAtoI($("#IDiconrollmenu").css("right"));

        if (!cyRollMenuWidth)
            cyRollMenuWidth = right;

        var ToggleRightMenu = function(right)
        {
            if (!right)
                $("#IDiconrollmenu").css("z-index", CYMaxZindex() + 1);
            $("#IDiconrollmenu").animate(
            {
                right: right
            }, {
                duration: 500,
                specialEasing: {
                    width: 'linear'
                }
            });
        };

        ToggleRightMenu(right == 0 ? cyRollMenuWidth : 0);

        $("#IDrollmenu").one("mouseleave click", function (event)
        {
            ToggleRightMenu(right);
        });

        $("#IDrollmenu li").one("click", function (event)
        {
            var action = $(this).data('action');
            var num = $(this).data('num');

            event.stopPropagation();
            ToggleRightMenu(right);

            $("#IDiconrollmenu").css("right", right);
            // Desktop backup
            if (action == "save")
                CYSaveDesktop(num);
            // Desktop restore
            else if (action == "restore")
                CYRestoreDesktop(num);
            // Reset all desktops
            else if (action == "reset")
            {
                for (var i = 1; i <= 3; i++)
                {
                    CYLocalStorage({ command: "remove", key: cyProject + "-" + cyCurrentPage + "-panels" + i });
                    CYLocalStorage({ command: "remove", key: cyProject + "-" + cyCurrentPage + "-icons" + i });
                    CYLocalStorage({ command: "remove", key: cyProject + "-" + cyCurrentPage + "-IDsidemenuWidth" + i });
                    CYLocalStorage({ command: "remove", key: cyProject + "-" + cyCurrentPage + "-IDmainLeft" + i });
                }
            }
        });
    });
    CYSetRollMenu();
}
/*
 * Set automatic rollmenu position
 */
function CYSetRollMenu()
{
    var menuwidth = $("#IDiconrollmenu").width();
    var iconwidth = $("#IDiconrollmenu > div:first-child").width();
    var menuright = CYAtoI(menuwidth) - CYAtoI(iconwidth) + 1;
    var windowheight = $(window).height();
    var menuheight = $("#IDiconrollmenu").height();
    var menutop = (windowheight - menuheight) / 2;

    $("#IDiconrollmenu").css({ "right": "-" + menuright + "px", "top": menutop + "px" });
}

/*
 * Save current page on profile num
 */
function CYSaveDesktop(num)
{
    debug && console.log('rollmenu.js : CYSaveDesktop ' + num);

    // Look for panels
    var panels = [];

    $(".ClassPanel.ui-draggable-handle").each(function()
    {
         var idcellpanel = $(this).parent().attr("id");
         // Cell containing panel MUST have an id
         if (idcellpanel != undefined)
            panels.push(idcellpanel);
    });

    // Panels backup
    var positions = [];

    for (var i = 0; i < panels.length; i++)
    {
        var idcellpanel = "#" + panels[i];

        positions.push(
        {
            id: panels[i],
            top: $(idcellpanel).css("top"),
            left: $(idcellpanel).css("left"),
            width: $(idcellpanel).css("width"),
            height: $(idcellpanel).css("height"),
            display: $(idcellpanel).css("display"),
            visibility: $(idcellpanel).css("visibility"),
            zindex: $(idcellpanel).css("z-index")
        });
    }

    CYLocalStorage({ command: "set", key: cyProject + "-" + cyCurrentPage + "-panels" + num, value: JSON.stringify(positions) });

    // Look for desktop icons
    var icons = [];

    $(".ClassIconFile").each(function()
    {
         icons.push($(this).attr("id"));
    });

    // Desktop icons backup
    var iconslist = [];

    for (var i = 0; i < icons.length; i++)
    {
        iconslist.push(
        {
            id: icons[i],
            html: $('<i/>').append($("#" + icons[i]).clone()).html()
        });
    }

    CYLocalStorage({ command: "set", key: cyProject + "-" + cyCurrentPage + "-icons" + num, value: JSON.stringify(iconslist) });

    // Sidemenu size

    CYLocalStorage({ command: "set", key: cyProject + "-" + cyCurrentPage + "-IDsidemenuWidth" + num, value: $("#IDsidemenu").css("width") });
    CYLocalStorage({ command: "set", key: cyProject + "-" + cyCurrentPage + "-IDmainLeft" + num, value: $("#IDmain").css("left") });
}

/*
 * Restore currentpage from profile #num
 * Appel de CYIsIconified() si définie lorsqu'il faudra rafraichir un panel non iconisé
 */
function CYRestoreDesktop(num)
{
    debug && console.log('rollmenu.js : CYRestoreDesktop ' + num);

    // Le tableau panelsToRefresh va contenir tous les panels, qu'ils soient
    // sous forme d'icone ou non.
    let panelsToRefresh = {};

    // Panels restore
    var p = CYLocalStorage(cyProject + "-" + cyCurrentPage + "-panels" + num);

    if (p != null)
    {
        // Look for panels
        var panels = [];

        $(".ClassPanel.ui-draggable-handle").each(function()
        {
            var idcellpanel = $(this).parent().attr("id");
            // Cell containing panel MUST have an id
            if (idcellpanel != undefined)
               panels.push(idcellpanel);
        });

        var positions = JSON.parse(p);

        if (positions != null)
        {
            for (var i = 0; i < panels.length; i++)
            {
                var idcellpanel = "#" + panels[i];
                var p = positions.find(function (e) { return e.id == panels[i]; });

                $(idcellpanel).css("top", p.top);
                $(idcellpanel).css("left", p.left);
                $(idcellpanel).css("width", p.width);
                $(idcellpanel).css("height", p.height);
                $(idcellpanel).css("display", p.display);
                $(idcellpanel).css("visibility", p.visibility);
                $(idcellpanel).css("z-index", p.zindex);
                panelsToRefresh[panels[i]] = true;
            }
        }
    }
    // Icons restore
    var ic = CYLocalStorage(cyProject + "-" + cyCurrentPage + "-icons" + num);

    if (ic != null)
    {
        // Look for desktop icons
        var icons = [];

        $(".ClassIconFile").each(function()
        {
             icons.push($(this).attr("id"));
        });

        $(".ClassIconFile").remove();
        var iconslist = JSON.parse(ic);
        if (iconslist != null)
        {
            for (var i = 0; i < iconslist.length; i++)
            {
                $("#IDmain").append(iconslist[i].html);
                let idsrc =  $("#" + iconslist[i].id).data('idsrc');
                // Le panel est iconifié, il ne faudra pas le refresh
                panelsToRefresh[idsrc] = false;
                // Reset events comme dans CYIconify()
                // Icone draggable
                $("#" + iconslist[i].id).draggable(
                {
                    containment: "#IDmain"
                });
                // Icone doubleclickable
                $("#" + iconslist[i].id).CYClick(undefined, function(iconid, event)
                {
                    var idsrc = $(iconid).data("idsrc");

                    $("#" + idsrc).show();
                    $("#" + idsrc).css("visibility", "hidden");
                    //$("#" + idsrc).css("top", 
                    $(iconid).transfer(
                    {
                        to: "#" + $(iconid).attr("data-idsrc"),
                        duration: 500
                    }, function()
                    {
                        $("#" + idsrc).css("visibility", "visible");
                        $(iconid).remove();
                    });
                    var zindex = CYMaxZindex() + 1;
                    $("#" + idsrc).css("z-index", zindex);
                    // Il faut rafraichir le contenu
                    if (typeof CYIsIconified == "function")
                        CYIsIconified(idsrc, false);
                });
            }
        }
    }

    // Appel CYIsIconifieds avec les noms des panels à refresh
    if (typeof CYIsIconified == "function") {
        for(p in panelsToRefresh) {
            if(panelsToRefresh[p])
                CYIsIconified(p, false);
        }
    };

    // Sidemenu size

    var w = CYLocalStorage({ command: "get", key: cyProject + "-" + cyCurrentPage + "-IDsidemenuWidth" + num });
    if (w != null)
        $("#IDsidemenu").css("width", w);
    var l = CYLocalStorage({ command: "get", key: cyProject + "-" + cyCurrentPage + "-IDmainLeft" + num });
    if (l != null)
        $("#IDmain").css("left", l);
}
