/*
 * cyjstables.js
 */
function CyInit_cyjstables() {
    debug && console.log('cyjsui.js : CyInit_cyjstables');

    if (!cyIsmobile)
        CYOpenSideMenu(true, false, true);

    CYMainTitle('CYJS Tables', 'table');


    // Static data in HTML file
    CYDataTable("#IDtableusers1");

    // Static data in JSON
    var columns = [{
            width: "30%",
            data: "user",
            searchable: true,
            visible: true,
            orderable: true
        },
        {
            width: "30%",
            data: "password",
            searchable: false,
            visible: true,
            orderable: false
        },
        {
            width: "30%",
            data: "role",
            searchable: true,
            visible: true,
            orderable: true,
            render: function(data, type, full, meta) {
                if (data == 'Admin')
                    return '<span style="color:red">' + data + '</span>';
                else
                    return data;
            }
        },
        {
            width: "10%",
            data: "id",
            searchable: false,
            visible: true,
            orderable: true
        }
    ];

    var users = [
        { "id": "1", "user": "Luke", "password": "password1", "role": "Admin" },
        { "id": "2", "user": "Suwan", "password": "password2", "role": "User" },
        { "id": "3", "user": "Kevin", "password": "password4", "role": "Enduser" },
        { "id": "4", "user": "Brice", "password": "password3", "role": "User" },
        { "id": "5", "user": "Bertrand", "password": "password6", "role": "Client" },
        { "id": "6", "user": "Daniel", "password": "password8", "role": "Enduser" },
        { "id": "7", "user": "Brigitte", "password": "password5", "role": "Client" },
        { "id": "8", "user": "John", "password": "password7", "role": "Admin" }
    ];
    var table = CYDataTable("#IDtableusers2", { responsive: true, columns: columns, data: users }, true);

    // Static data in HTML file
    CYDataTable("#IDtableusers3", { responsive: true });

    // Dynamic data from webservice
    var columns = [{
            width: "20%",
            data: "user",
            searchable: true,
            visible: true,
            orderable: true,
            render: function(data, type, full, meta) {
                if (full.id == '0')
                    return '<span style="color:red">' + data + '</span>';
                else if (full.groupid == '0')
                    return '<span style="color:blue">' + data + '</span>';
                else
                    return data;
            }
        },
        {
            width: "5%",
            data: "password",
            searchable: false,
            visible: false,
            orderable: false
        },
        {
            width: "5%",
            data: "id",
            searchable: true,
            visible: false,
            orderable: true
        },
        {
            width: "5%",
            data: "groupid",
            searchable: true,
            visible: false,
            orderable: true
        },
        {
            width: "25%",
            data: "comment",
            searchable: false,
            visible: true,
            orderable: true
        },
        {
            width: "20%",
            data: "home",
            searchable: false,
            visible: true,
            orderable: true
        },
        {
            width: "20%",
            data: "shell",
            searchable: false,
            visible: true,
            orderable: true
        }
    ];

    /* Webservice will read a /etc/passwd file like */
    var webservice = cyServerName + cyPath + cyWebSvc + "?r=readpasswd";
    CYExec({
        url: webservice,
        sync: false,
        callbackok: function(ret, status) {
            if (ret < 0)
                var users = new Object();
            else
                var users = JSON.parse(ret);
            var table = CYDataTable("IDtableusers4", { responsive: true, columns: columns, data: users }, true);

            // Direct cell editing on double click
            // Use var table or $("#IDtableusers4").DataTable() in place of var table
            CYDataTableEditCell(table, function(newvalue, oldvalue, colonne, row, callback) {
                CYAlert({ width: '500', message: "Cell edited<br>new value=" + newvalue + "<br>old value=" + oldvalue + "<br>col=" + colonne + "<br>row=" + JSON.stringify(row) });
                /* Update field via webservice here */
                callback(newvalue); // Finally refresh field with the modified value
                //callback (oldvalue); // or reset  field with the previsous value
            });
            /* Line single clic / cannot be triggered if CYDataTableEditCell() used
            CYDataTableSelectLine("IDtableusers4", function (row)
            {
                CYAlert(JSON.stringify(row));
            });*/
            // Excel export
            $("#IDexcel0").show().on("click", function() {
                var webservice = cyServerName + cyPath + cyWebSvc;
                CYURL(webservice, { data: { r: "json2excel", header: "user;password;id;groupid;comment;home;shell", lines: JSON.stringify(users), download: "users.xlsx" } });
            });
        }
    });

    // Dynamic data from webservice
    var pcolumns = [{
            width: "9%",
            data: "user",
            searchable: true,
            visible: true,
            orderable: true
        },
        {
            width: "9%",
            data: "pid",
            searchable: true,
            visible: true,
            orderable: true
        },
        {
            width: "9%",
            data: "cpu",
            searchable: true,
            visible: true,
            orderable: true
        },
        {
            width: "9%",
            data: "mem",
            searchable: true,
            visible: true,
            orderable: true
        },
        {
            width: "9%",
            data: "vsz",
            searchable: true,
            visible: false,
            orderable: true
        },
        {
            width: "9%",
            data: "rss",
            searchable: true,
            visible: false,
            orderable: true
        },
        {
            width: "9%",
            data: "tty",
            searchable: true,
            visible: false,
            orderable: true
        },
        {
            width: "9%",
            data: "stat",
            searchable: true,
            visible: false,
            orderable: true
        },
        {
            width: "9%",
            data: "start",
            searchable: true,
            visible: true,
            orderable: true
        },
        {
            width: "9%",
            data: "time",
            searchable: true,
            visible: true,
            orderable: true
        },
        {
            width: "9%",
            data: "command",
            searchable: true,
            visible: true,
            orderable: true
        }
    ];
    /* Webservice will read process output */
    var webservice = cyServerName + cyPath + cyWebSvc + "?r=readprocess";
    CYExec({
        url: webservice,
        sync: false,
        callbackok: function(ret, status) {
            if (ret < 0)
                var process = new Object();
            else
                var process = JSON.parse(ret);
            CYDataTable("IDtableusers5", { responsive: true, columns: pcolumns, data: process }, true);

            /* Line single clic */
            CYDataTableSelectLine("IDtableusers5", function(row) {
                CYAlert(JSON.stringify(row));
            });

            // Excel export
            $("#IDexcel1").show().on("click", function() {
                var webservice = cyServerName + cyPath + cyWebSvc;
                CYURL(webservice, { data: { r: "json2excel", header: "user;pid;cpu;mem;vsz;rss;tty;stat;start;time;command", lines: JSON.stringify(process), download: "process.xlsx" } });
            });
        }
    });


    CYInitAllTools();
}