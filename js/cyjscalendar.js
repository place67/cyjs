/*
 * cyjscalendar.js
 */
function CyInit_cyjscalendar() {
    if (!cyIsmobile)
        CYOpenSideMenu(true, false, true);

    CYMainTitle('CYJS Calendar', 'calendar');
    CYInitAllTools();

    // Basic calendar
    CYCalendar("#calendarbasic", {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        editable: true,
        height: 250,
        eventTextColor: "#333",
        header: {
            left: 'basicDay,basicWeek,month',
            center: 'title',
            right: 'today prevYear,prev,next,nextYear'
        },
        dayClick: function(date, jsevent, view) {
            InputEvent(date, false, jsevent, view);
        },
        eventClick: function(event, element) {
            UpdateEvent(event, element);
        },
        eventDrop: function(event, delta, revertFunc) {
            //CYAlert('Internal Drag/Drop "' + event.title + '" was dropped on ' + event.start.format())
        }
    });

    // Tasks list
    CYCalendar("#calendarlist", {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        editable: false,
        height: 250,
        eventTextColor: "#333",
        header: {
            left: 'listDay,listWeek,listMonth,listYear',
            center: 'title',
            right: 'today prevYear,prev,next,nextYear'
        },
        dayClick: function(date, jsevent, view) {
            InputEvent(date, true, jsevent, view);
        },
        eventClick: function(event, element) {
            UpdateEvent(event, element);
        },
        eventDrop: function(event, delta, revertFunc) {
            //CYAlert('Internal Drag/Drop "' + event.title + '" was dropped on ' + event.start.format())
        }
    });

    // Enhanced calendar
    // Initialize the droppable external events
    $('.ClassCalEvent').each(function() {
        var thisevent = this;
        var title = $.trim(CYTranslateString($(thisevent).text()));
        var eventdata = { originalTitle: title, title: title, eventType: $(thisevent).data('eventType'), duration: $(thisevent).data('eventDuration') };
        // Store 'title' and 'eventType' and 'eventDuration' so the calendar knows to render an event upon drop
        $(thisevent).data('event-data', eventdata);
        $(thisevent).text(title);
        $(thisevent).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 0
        });
        $(thisevent).droppable({
            accept: ".ClassCalResource",
            zIndex: 999,
            revert: true,
            revertDuration: 0,
            drop: function(event, ui) {
                var resourceId = ui.draggable.data("resourceId");
                var eventdata = $(thisevent).data('event-data');
                eventdata.title = $(thisevent).text() + "-" + resourceId;
                $(thisevent).data('event-data', eventdata);
                $(thisevent).text($(thisevent).text() + "-" + resourceId);
                if ($('#IDUniqueResource').val() == 1)
                    ui.draggable.css("display", "none");
                else {
                    ui.draggable.draggable("option", "disabled", true);
                    ui.draggable.css({ cursor: "wait", opacity: "0.2" });
                }
            }
        });
    });
    // Initialize the droppable resources
    $('.ClassCalResource').each(function() {
        $(this).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 0
        });
    });
    // Reset to defaults
    $("#IDReset").click(function(event) {
        CYReloadPage();
    });
    CYCalendar("#calendaragenda", {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        // now: "2017-08-01",
        editable: true,
        droppable: true,
        dropAccept: ".ClassCalEvent",
        weekNumbers: true,
        minTime: "07:00:00",
        maxTime: "22:00:00",
        defaultTimedEventDuration: "01:00:00",
        height: "auto",
        slotDuration: '00:15:00',
        allDaySlot: true,
        //eventTextColor: "#333",
        aspectRatio: 4,
        /*visibleRange: {
            start: '2017-07-01',
            end: '2017-07-05'
        },*/
        businessHours: {
            dow: [1, 2, 3, 4, 5], // Lundi - Vendredi
            start: '09:00',
            end: '18:00'
        },
        header: {
            left: 'agenda5Days, agendaDay,agendaWeek,month',
            center: 'title',
            right: 'today prevYear,prev,next,nextYear'
        },
        views: {
            agenda5Days: {
                type: 'agendaWeek',
                duration: { days: 7 },
                weekends: false,
                firstDay: 1,
                //hiddenDays: [3], // Wednesday hidden
                buttonText: CYTranslateString("5 days")
            }
        },
        dayClick: function(date, jsevent, view) {
            var allday = !date.hasTime();
            InputEvent(date, allday, jsevent, view); // Defined in cyjsui.js
        },
        eventClick: function(event, element) {
            UpdateEvent(event, element); // Defined in cyjsui.js
        },
        eventDrop: function(event, delta, revertFunc) {
            if (event.eventType == "E" && moment(event.start).format("HH") < 18) {
                CYAlert("A bit early for dinner .. ;)");
                revertFunc();
            } else
                $(".ClassCalendar").fullCalendar('renderEvent', event, true);
        },
        drop: function(date, jsEvent, ui) {
            var event = $(this).data("event-data");
            var duration = CYExplode(":", event.duration, 2, 0);

            event.start = date;
            event.end = moment(date).add(duration[0], 'h').add(duration[1], 'm');
            event.backgroundColor = $(".ClassCalEvent[data-event-type='" + event.eventType + "']").css("background-color");
            event.borderColor = $(".ClassCalEvent[data-event-type='" + event.eventType + "']").css("border-color");
            event.textColor = $(".ClassCalEvent[data-event-type='" + event.eventType + "']").css("color");
            if (event.eventType == "E" && moment(date).format("HH") < 18)
                CYAlert("A bit early for dinner .. ;)");
            else {
                $(".ClassCalendar").fullCalendar('renderEvent', event, true);
                if ($('#IDUniqueEvent').val() == 1)
                    $(this).remove();
                $('.ClassCalResource').draggable("option", "disabled", false);
                $('.ClassCalResource').css({ cursor: "move", opacity: "1" });
                $('.ClassCalEvent').each(function() {
                    var eventdata = $(this).data('event-data');
                    eventdata.title = eventdata.originalTitle;
                    $(this).data('event-data', eventdata);
                    $(this).text(eventdata.title);
                });
            }
        },
        eventRender: function(event, element, view) {
            var duration = event.end - event.start;
            // Less then 1 hour        
            if (duration > 0 && duration < 3600000)
                element.css({ "font-size": "0.7em" });
        },
        events: // Static events
            [
            { id: CYUniqueId(), resourceId: "A", title: CYTranslateString("CYJS2.0 is online"), start: moment("01/08/2017", 'DD/MM/YYYY', true).set({ hour: 12, minute: 0, second: 0, millisecond: 0 }), backgroundColor: "#ffff11" }
        ]
    });
    // Setting dynamic calendar events
    var now = moment();
    // Setting 1 event
    $('.ClassCalendar').fullCalendar('renderEvent', {
        id: CYUniqueId(),
        resourceId: "B",
        title: CYTranslateString("This midday"),
        description: CYTranslateString("This midday"),
        start: now.set({ hour: 12, minute: 0, second: 0, millisecond: 0 }),
        backgroundColor: "#f6f6f6"
    }, 'stick');
    // Setting events array
    SetDefaultEvents();

    setTimeout(function() { CYRestoreDesktop(1), 50 });
}