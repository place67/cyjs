/*
 * sidemenu.js
 */
function CyInit_sidemenu() {

    debug && console.log('sidemenu.js : CyInit_sidemenu');

    CYMenuSlider();
    // File edit
    if (cyUsername == "admin")
        CYSetIconFileEdit();

    cyjsSidemenuStart("#IDsidemenucontent li");
}

// Traite un click sur un li du sidemenu
function cyjsSidemenuStart() {
    $("#IDsidemenucontent").menu({
        position: { my: "left top", at: "left+30 top+40" }
    }).show();
    // Click sur un li avec un page/link/iframe attr
    $("#IDsidemenucontent li").off('click').click(function() {
        console.log('CLIC ' + $(this).attr('page') + '/' + $(this).attr('link') + '/' + $(this).attr('iframe'));
        if ($(this).attr('page') === undefined && $(this).attr('link') === undefined && $(this).attr('iframe') === undefined)
            return;
        $("#IDusermenu").hide("slide", { direction: "right" }, 500);
        // Important : fermer sidemenu si necessaire
        if ($(window).width() < cyMinWindowWidth || $("#IDmain").css('left') == '0px')
            CYOpenSideMenu(false, false, true);
        cyjsSidemenuClick(this);
    });
}

// Recherche un attribut 'maison' dans le li: <li link="https://place67.com" data-newtab="true">
// - page="nom-de-page" : recherche une page CYJS
// - link="https://..." data-newtab="true" : lance un lien éventuellement dans un onglet
// - iframe="https:/.../?u=cyUsername&l=cyLang.." : lance un lien dans un iframe avec possibilité
//   de substitution via cyjsRebuilURL()
function cyjsSidemenuClick(theli) {
    let url = $(theli).attr('page');
    if (url != undefined) {
        // Positionné au lancement d'un iframe
        $("#IDmain").removeClass("iframefullsize");
        CYLoadPage(url, 'IDmain');
    } else {
        url = $(theli).attr('link');
        if (url != undefined) {
            var options = CYExtrAttrs($(theli));
            url = cyjsRebuildURL(url);
            CYURL(url, options);
        } else {
            url = $(theli).attr('iframe');
            if (url != undefined) {
                var options = CYExtrAttrs($(theli));
                url = cyjsRebuildURL(url);
                $("#IDmain").addClass("iframefullsize").html('<iframe sandbox="allow-same-origin allow-popups allow-forms allow-scripts" src="' + url + '" style="width:100%; height:100%; border: 0"></iframe>');
            }
        }
    }
}

// Traduction d'args eventuels dans une URL par leur correspondance JS
// https://......?u=cyUsername&l=cyLang...
// Remplacera cyUsername par le contenu de la variable JS cyUsername 
function cyjsRebuildURL(url0) {
    let urlall = CYExplode('?', url0);
    let url = urlall[0];
    let args = CYExplode('&', urlall.length == 2 ? urlall[1] : '');
    if (args.length)
        url += '?';
    let i = 0;
    args.forEach((a) => {
        let m = CYExplode('=', a);
        if (i++)
            url += '&';
        let m1 = window[m[1]];
        if (m1 != undefined)
            url += m[0] + '=' + m1;
        else
            url += a;
    });
    return (url);
}
