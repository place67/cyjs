/*
* usermenu.js
*/
function CyInit_usermenu()
{
    debug && console.log('usermenu.js : CyInit_usermenu');
    $("#IDusermenucontent").menu();
    // Recherche dans le li cliqu� l'attribut src qui d�signe l'url dans html/
    $("#IDusermenucontent li").click(function()
        {
            $("usermenu").toggle("fast", "swing");
            src = $(this).attr('src');
            switch (src)
            {
                case 'logoff':
                    // If logoff confirmation needed
                    //CYConfirm({message: 'LOGOFF', icon: 'sign-out'}, CYLogoff);
                    var webservice = cyServerName + cyPath + cyWebSvc +"?r=logoff";               
                    CYExec(
                    {
                        url: webservice,
                        sync: false,                     
                    });
                    CYLogoff();
                    break;
                case 'profil':
                    MyProfil();
                    break;
            }
        }
    );
}
function MyProfil()
{
    debug && console.log('usermenu.js : MyProfil');
    var idform = "#IDformprofil";
    var form = $(idform);
    var boutons =[
        {
        text: CYTranslateString('OK'),
        click: function()
            {
                if ($("#IDprofiluserpassword").val().trim() != $("#IDprofiluserpasswordagain").val().trim())
                    CYAlert('PASSWORD NOT MATCH');
                else if ($("#IDprofiluserpassword").val().trim() != '')
                {
                    // update user password here
                    CYAlert({message: 'Profile updated', icon: 'thumbs-up', autofade: cyAlertDelay});
                    $(this).dialog("close");
                }
            }
        },
        {
        text: CYTranslateString('CANCEL'),
        click: function()
            {
                $(this).dialog("close");
            }
        }
    ];
    CYForm(idform, {title: 'Profil', width: form.css("width"), buttons: boutons});
    $("#IDprofilusername").val(cyUsername);
}