/* 
 * myfirstpage.js
 */

function CyInit_myfirstpage()
{
    debug && console.log('myfirstpage.js : CyInit_myfirstpage');

    CYMainTitle('CYJS My first page', 'file');
    CYInitAllTools();
	
	$("#openmenubutton").click(function ()
    {
        CYOpenSideMenu(true, false, true);
    });
	$("#closemenubutton").click(function ()
    {
        CYOpenSideMenu(false, false, true);
    });
}
