/*
 * cyjsplanning.js
 */

function CyInit_cyjsplanning() {
    CYMainTitle('CYJS Planning', 'tasks');
    CYOpenSideMenu(false, false, true);
    CYInitAllTools();

    // Planning
    // Initialize the droppable external events
    $('.ClassCalEventP').each(function() {
        var thisevent = this;
        var title = $.trim(CYTranslateString($(thisevent).text()));
        var eventdata = { title: title, eventType: $(thisevent).data('eventType'), duration: $(thisevent).data('eventDuration') };
        // Store 'title' and 'eventType' and 'eventDuration' so the calendar knows to render an event upon drop       
        $(thisevent).data('event-data', eventdata);
        $(thisevent).text(title);
        $(thisevent).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 0
        });
    });

    $('#planning').fullCalendar({
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        now: moment(),
        editable: true,
        droppable: true,
        dropAccept: ".ClassCalEventP",
        minTime: "07:00:00",
        maxTime: "22:00:00",
        aspectRatio: 2,
        height: "auto",
        slotDuration: '00:15:00',
        slotWidth: 28,
        allDaySlot: true,
        header: {
            left: 'today,timelineDay,timelineWeek,timelineMonth,timeline3Months,timelineYear',
            center: 'title',
            right: 'prev,next,agendaDay,agendaWeek,month'
        },
        defaultView: 'timelineDay',
        views: {
            timelineWeek: {
                type: 'timeline',
                duration: { days: 7 }
            },
            timelineMonth: {
                type: 'timeline',
                duration: { days: 30 },
                slotWidth: 150
            },
            timeline3Months: {
                type: 'timeline',
                duration: { days: 90 },
                slotWidth: 50
            }
        },
        resourceLabelText: 'Teams',
        resources: [{
                id: 'M',
                title: CYTranslateString('Management'),
                children: [
                    { id: 'M1', title: 'Luke' },
                    { id: 'M2', title: 'Brice' }
                ]
            },
            {
                id: 'D',
                title: CYTranslateString('Devlopment'),
                children: [
                    { id: 'D1', title: 'Luke' },
                    { id: 'D2', title: 'Brice' },
                    { id: 'D3', title: 'Bruno' },
                    { id: 'D4', title: 'Fred' },
                ]
            },
            {
                id: 'S',
                title: CYTranslateString('Sales'),
                children: [
                    { id: 'S1', title: 'Luke' },
                    { id: 'S2', title: 'Marc' },
                    { id: 'S3', title: 'Kevin' }
                ]
            }
        ],
        events: [
            { id: '1', resourceId: 'M1', start: moment().set({ hour: 8, minute: 0, second: 0, millisecond: 0 }), end: moment().set({ hour: 12, minute: 0, second: 0, millisecond: 0 }), title: CYTranslateString('Project') + ' Luke', backgroundColor: "#ffff00", constraint: { resourceIds: ['M1'] } },
            { id: '2', resourceIds: ['M2', 'D2'], start: moment().set({ hour: 14, minute: 0, second: 0, millisecond: 0 }), end: moment().set({ hour: 17, minute: 0, second: 0, millisecond: 0 }), title: CYTranslateString('Project') + ' 2', backgroundColor: "#ff0011" },
            { id: '3', resourceId: 'D', start: moment().set({ hour: 8, minute: 0, second: 0, millisecond: 0 }), end: moment().set({ hour: 12, minute: 0, second: 0, millisecond: 0 }), title: CYTranslateString('Project') + ' 1', backgroundColor: "#ffff00" },
            { id: '4', resourceId: 'D2', start: moment().set({ hour: 8, minute: 0, second: 0, millisecond: 0 }), end: moment().set({ hour: 12, minute: 0, second: 0, millisecond: 0 }), title: CYTranslateString('Project') + ' 4', backgroundColor: "#ffff22" },
            { id: '5', resourceId: 'D3', start: moment().set({ hour: 8, minute: 0, second: 0, millisecond: 0 }), end: moment().set({ hour: 12, minute: 0, second: 0, millisecond: 0 }), title: CYTranslateString('Project') + ' 3', backgroundColor: "#ff0011" },
            { id: '6', resourceId: 'S2', start: moment().set({ hour: 8, minute: 0, second: 0, millisecond: 0 }), end: moment().set({ hour: 12, minute: 0, second: 0, millisecond: 0 }), title: CYTranslateString('Allday'), backgroundColor: "#00ff44", allDay: true },
        ],
        dayClick: function(date, jsevent, view, resourceObj) {
            var allday = !date.hasTime();
            CYPrompt({ label: "Event", width: 250 }, function(str) {
                var newEvent = { id: CYUniqueId(), resourceId: resourceObj.id, title: str, start: date, allDay: allday, backgroundColor: "#f6f6f6" };
                $("#planning").fullCalendar('renderEvent', newEvent, 'stick');
            });
        },
        eventClick: function(event, element) {
            UpdateEvent(event, element);
        },
        eventDrop: function(event, delta, revertFunc) {
            if (event.eventType == "E" && moment(event.start).format("HH") < 18) {
                CYAlert("A bit early for dinner .. ;)");
                revertFunc();
            } else
                $("#planning").fullCalendar('renderEvent', event, true);
        },
        drop: function(date, jsEvent, ui, resourceId) {
            var event = $(this).data("event-data");
            var duration = CYExplode(":", event.duration, 2, 0);
            event.resourceId = resourceId;
            event.start = date;
            event.end = moment(date).add(duration[0], 'h').add(duration[1], 'm');
            event.backgroundColor = $(".ClassCalEventP[data-event-type='" + event.eventType + "']").css("background-color");
            event.borderColor = $(".ClassCalEventP[data-event-type='" + event.eventType + "']").css("border-color");
            event.textColor = $(".ClassCalEventP[data-event-type='" + event.eventType + "']").css("color");
            if (event.eventType == "E" && moment(date).format("HH") < 18)
                CYAlert("A bit early for dinner .. ;)");
            else {
                $("#planning").fullCalendar('renderEvent', event, true);
                if ($('#IDUniqueEvent').val() == 1)
                    $(this).remove();
            }
        },
        eventRender: function(event, element, view) {
            var duration = event.end - event.start;
            // Less then 1 hour        
            if (duration > 0 && duration < 3600000)
                element.css({ "font-size": "0.7em" });
        }
    });

    // Setting dynamic calendar events
    var now = moment();
    // Setting 1 event
    $('#planning').fullCalendar('renderEvent', {
        id: CYUniqueId(),
        title: CYTranslateString("This midday"),
        description: CYTranslateString("This midday"),
        start: now.set({ hour: 12, minute: 0, second: 0, millisecond: 0 }),
        backgroundColor: "#f6f6f6",
        resourceIds: ['M', 'D', 'S']
    }, 'stick');
    // Setting events array
    //SetDefaultEvents();    
}