// index
cyString['IDheadericonmenu'] = 'Click to mask/open or Dbl-click to reset';
cyString['IDheadericonuser'] = 'User menu';
// sidemenu
cyString['CYJS UI'] = "CYJS UI";
cyString['CYJS Tables'] = "CYJS Tables";
// usermenu
cyString['Profil'] = "Profile";
cyString['Deconnecter'] = "Logoff";
cyString['Profile updated'] = "Profile updated";
cyString['Profile not updated'] = "Profile non updated";
cyString['PASSWORD AGAIN'] = "Repeat password";
cyString['PASSWORD NOT MATCH'] = "Different passwords";
// login
cyString['French'] = "Français";
cyString['English'] = "Anglais";
cyString['Thai'] = "Thaï";
cyString['German'] = "German";
cyString['Alsacian'] = "Alsacian";
cyString['LANGUAGE'] = "Language";
cyString['USERNAME'] = "Username";
cyString['PASSWORD'] = "Password";
cyString['PLEASE LOGIN'] = "Please login";
cyString['BurgerTitle'] = "Click 1 time to change menu width, 2 times to hide it. When hidden click that icon to display the menu over, then double-click to reset to default, or resize the main window.";
// portal
cyString['Please input any URL to display its output below'] = "Note: your browser may need an extension to allow cross-browsing (for Chrome : Allow-Control-Allow-Origin).<br>Please input any URL to display its output below -<i>some may not work due to cross browsing or self related href ..:(</i>-<br>The powerful thing here is that cache/offline feature works whatever the site contents !<br>Once you get the page, turn off your computer network, then jump to another page as CYJS UI, then come back here, you can input any past URL you already have input there and its content is still available in your app. WITHOUT NETWORK !"
    // cyjscrm
cyString['CRM demo page'] = "This CRM demo page has wuickly been made to give you an idea.";
cyString['CRM demo page description'] = 'Panel are all movable, all informations can be loaded from a database or hardcoded (as here).<br>You can click on a company name in Addressbook panel to manage its files below in Company documents panel.<br>You can click inside calendar to add an event, or drag/drop a preset one below.';
cyString['A bit early for dinner'] = "A bit early for dinner ..;) .. please move after 18";