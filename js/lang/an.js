// index
cyString['IDheadericonmenu'] = 'Klicken Sie zum Maskieren / Öffnen oder Doppelklicken zum Zurücksetzen';
cyString['IDheadericonuser'] = 'Benutzermenü';
// sidemenu
cyString['CYJS UI'] = "CYJS UI";
cyString['CYJS Tables'] = "CYJS Datentabelle";
// usermenu
cyString['Profil'] = "Profil";
cyString['Deconnecter'] = "Ausloggen";
cyString['Profile updated'] = "Profil aktualisiert";
cyString['Profile not updated'] = "Profil nicht aktualisiert";
cyString['PASSWORD AGAIN'] = "Wiederhole das Passwort";
cyString['PASSWORD NOT MATCH'] = "Unterschiedliche Passwörter";
// login
cyString['French'] = "Französisch";
cyString['English'] = "Englisch";
cyString['Thai'] = "Thai";
cyString['German'] = "Deutsche";
cyString['Alsacian'] = "Elsässisch";
cyString['LANGUAGE'] = "Sprache";
cyString['USERNAME'] = "Nutzername";
cyString['PASSWORD'] = "Passwort";
cyString['PLEASE LOGIN'] = "Bitte loggen Sie sich ein";
cyString['BurgerTitle'] = "Klicken Sie 1 Mal, um die Menübreite zu ändern, 2 Mal, um sie auszublenden. Wenn Sie ausgeblendet sind, klicken Sie auf dieses Symbol, um das Menü anzuzeigen, und doppelklicken Sie dann, um die Standardeinstellungen wiederherzustellen, oder ändern Sie die Größe des Hauptfensters.";
// portal
cyString['Please input any URL to display its output below'] = "Hinweis: Ihr Browser benötigt möglicherweise eine Erweiterung, um das Cross-Browsing zu ermöglichen (für Chrome: Allow-Control-Allow-Origin). <br> Bitte geben Sie eine URL ein, um die Ausgabe unten anzuzeigen. <i> Einige funktionieren möglicherweise aufgrund des Cross-Browsings nicht oder selbstbezogene href .. :( </ i> - <br> Das Mächtige hier ist, dass die Cache- / Offline-Funktion unabhängig vom Inhalt der Website funktioniert! <br> Wenn Sie die Seite erhalten haben, schalten Sie Ihr Computernetzwerk aus und springen Sie zu eine andere Seite als CYJS-Benutzeroberfläche, dann kommen Sie hierher zurück. Sie können jede frühere URL eingeben, die Sie bereits dort eingegeben haben, und der Inhalt ist weiterhin in Ihrer App verfügbar. OHNE NETZWERK!";
// cyjscrm
cyString['CRM demo page'] = "Diese CRM-Demoseite wurde schnell erstellt, um Ihnen eine Idee zu geben.";
cyString['CRM demo page description'] = 'Das Bedienfeld ist alle beweglich, alle Informationen können aus einer Datenbank geladen oder fest codiert werden (wie hier). <br> Sie können im Adressbuchfenster auf einen Firmennamen klicken, um die folgenden Dateien im Fenster Unternehmensdokumente zu verwalten. <br> Sie können hinein klicken Kalender, um ein Ereignis hinzuzufügen, oder ziehen Sie ein voreingestelltes unten.';
cyString['A bit early for dinner'] = "Ein bisschen früh zum Abendessen ..;) .. bitte nach 18 umziehen";