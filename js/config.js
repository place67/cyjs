debug = 2;
// Project and indexedDB name - NO SPACE !
cyProject = "CYJS-Demo";
// Available languages
// cyAvailableLang = "fr,en,th"
// Min window size to display permanent sidemanu
// Must be equal to max-width in media screen section in style.css
cyMinWindowWidth = 1200;
// Small sidemenu width
//cySideMenuSmallWidth = "62px";
// Auto sidemenu when window resizing
//cySideMenuAuto = true;
// Default language
cyLang = 'en';
// Path to app relative to vhost, empty if CYJS on vhost home
cyPath = '/cyjs';
// Default page - REQUIRED
cyDefaultPage = 'cyjsui';
// Login function
//cyAutoLogin = {
//    user: 'orrion',
//    password: 'orrion'
//};
CYLoginFct = function() {
    /* Default login process no control
    CYLogin('luke');   
    CYLogged('luke');
    CYLoadPage({
        page: cyDefaultPage,
        idToSet: 'IDmain',
        autoLoader: true
    });
    */
    CYLoadPage('login');
    // Pages preloading - care if user rule is important
    //CYLoadPage('cyjsui');
    //CYLoadPage('cyjstables');
    //CYLoadPage('cyjscalendar');
    //CYLoadPage('cyjscrm');
    //CYLoadPage('cyjsplanning');
    //CYLoadPage('myfirstpage');
};
// Main web service
cyWebSvc = "/php/websvc.php"
// Cache enabled
//cyCache = true;
// Sync enabled
//cySync = true;
// Default delay for alert autofade
cyAlertDelay = 2000;
// Tooltip display duration
//cyTooltipDuration = 5000;
// Keepalive - 0 to disable, default is 30s 
cyKeepAlive = 10000;
// CSS default patterns
//cyCSS.Font = "Varela Round";
//cyCSS.AltBackgroundColor = "#f5f5f5";
//cyCSS.BackgroundColor = "#007fff";
//cyCSS.BorderColor = "#ccc";
//cyCSS.ButtonBackgroundColor = "#eee";
//cyCSS.HoverTextColor: "#333",
//cyCSS.HoverBackgroundColor: "#c3c3c3",
//cyCSS.InputShadow = "#38c";
//cyCSS.InputBackgroundColor = "#fff";
//cyCSS.TextColor = "#333333";
//cyCSS.ReverseTextColor = "#fff";
//cyCSS.TooltipTextColor = "#fff";
//cyCSS.TooltipBackgroundColor = "#333";
// Selected theme
cyCSS.theme = (GET['theme'] != undefined) ? GET['theme'] : 'default';
// Themes settings
cyCSS.themes = {};
cyCSS.themes.default = {
    BackgroundColor: cyCSS.BackgroundColor
};
cyCSS.themes.Light = {
    ReverseTextColor: "#333",
    BackgroundColor: "#eee",
    ButtonBackgroundColor: "#fff",
    InputShadow: "#eee",
    HoverTextColor: "#333",
    HoverBackgroundColor: "#d1d1d1"
};
cyCSS.themes.Green = {
    ReverseTextColor: "#fff",
    BackgroundColor: "#549252",
    InputShadow: "#549252",
    HoverTextColor: "#fff",
    HoverBackgroundColor: "#549252"
};
cyCSS.themes.Grey = {
    ReverseTextColor: "#333",
    BackgroundColor: "#b8b8b8",
    InputShadow: "#b8b8b8",
    HoverTextColor: "#333",
    HoverBackgroundColor: "#b8b8b8"
};
cyCSS.themes.Brown = {
    ReverseTextColor: "#fff",
    BackgroundColor: "#9e4727",
    InputShadow: "#deC7C7",
    HoverTextColor: "#fff",
    HoverBackgroundColor: "#9e4727"
};
cyCSS.themes.Dark = {
    BackgroundColor: "#333",
    InputShadow: "#333",
    HoverTextColor: "#fff",
    HoverBackgroundColor: "#333"
};
// Overwrite defaut CSS with selected theme
for (var t in cyCSS.themes[cyCSS.theme])
    cyCSS[t] = cyCSS.themes[cyCSS.theme][t];
debug && console.log('config.js loaded');
