/*
 * login.js
 */

function CyInit_login() {
    debug && console.log('login.js : CyInit_login');


    // Add flags according to cyAvailableLang
    var langs = CYExplode(',', cyAvailableLang);
    var langsmenu = '';
    langs.forEach(function (l) {
        langsmenu += `<li data-value="${l}"><img src="CYJS/images/flags/${l}.png"></li>`;
    });
    if (typeof cyAutoLogin == 'object') {
        $("#IDusername").val(cyAutoLogin.user);
        $("#IDuserpassword").val(cyAutoLogin.password);
    }
    if (GET["u"] != undefined) {
        $("#IDusername").val(GET["u"]);
        if (GET["p"] != undefined)
            $("#IDuserpassword").val(GET["p"]);
    }
    if ($("#IDusername").val() != '' && $("#IDuserpassword").val() != '')
        CyInit_loginDone($("#IDusername").val(), $("#IDuserpassword").val(), langsmenu);
    else
        CyInit_loginForm(langsmenu, '');
}

function CyInit_loginForm(langsmenu, msg) {
    CYInitAllTools();
    var idform = "#IDdivlogin";
    var form = $(idform);
    CYForm(idform, {
        title: 'PLEASE LOGIN',
        closeOnEscape: false,
        fullscreen: false,
        close: false,
        position: { my: "center", at: "center", of: window },
        appendTo: "body",
        buttons: [{
            text: CYTranslateString('OK'),
            click: function () {
                CyInit_loginDone($("#IDusername").val().trim(), $("#IDuserpassword").val().trim(), langsmenu);
            }
        }]
    });
    $("#IDdivlangmenu ul").append(langsmenu);
    // Click on flag, reset url with original vars
    $("#IDdivlangmenu li").one("click", function (ev) {
        var url = cyServerName + cyPath + "/?lang=" + $(this).data('value');
        ev.stopPropagation();
        Object.keys(GET).forEach(function (e) {
            // lang is already set
            if (e != 'lang')
                url += "&" + e + "=" + GET[e];
        });
        window.location.href = url;
    });
    // cyLang automatically set by framework with ?lang=XX
    $("#IDlangmenu").attr("src", "CYJS/images/flags/" + cyLang + ".png");
    // Genere les li pour choisir le theme selon definition de cyCSS.themes dans config.js
    var li = '';
    for (var theme in cyCSS.themes) {
        li += '<li data-value="' + theme + '" style="background-color: ' + cyCSS.themes[theme].BackgroundColor + '" title="' + CYTranslateString(theme) + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>';
    };
    $("#IDdivthememenu .ClassMenu").html(li);
    // Click on theme, reset url with original vars
    $("#IDdivthememenu li").one("click", function (ev) {
        var url = cyServerName + cyPath + "/?theme=" + $(this).data('value');
        ev.stopPropagation();
        Object.keys(GET).forEach(function (e) {
            // themeis already set
            if (e != 'theme')
                url += "&" + e + "=" + GET[e];
        });
        window.location.href = url;
    });
    $("#IDthememenu").css("background-color", cyCSS.themes[cyCSS.theme].BackgroundColor);
    if(msg != '')
        CYAlert({ message: msg, icon: 'warning', autofade: 0, appendTo: "body" });
    if($("#IDusername").val() == '')
        $("#IDusername").focus();
    else if($("#IDuserpassword").val() == '')
        $("#IDuserpassword").focus();
}

function CyInit_loginDone(user, password, langsmenu) {
    var webservice = cyServerName + cyPath + cyWebSvc + "?r=login&u=" + user + "&p=" + password;
    CYExec({
        url: webservice,
        dataType: "json",
        sync: false,
        callbackok: function (ret, status) {
            console.log("login " + JSON.stringify(ret));
            if (!ret.success) {
                cyAutoLogin = '';
                // Je transmets l'erreur afin d'afficher une alerte
                CyInit_loginForm(langsmenu, CYTranslateString('Login error') + ' : ' + ret.error);
                return;
            }
            CYLogin(user);
            // Reloads sidemenu for "admin" to use CYSetIconFileEdit() in CyInit_sidemenu()
            if (user == "admin")
                CYLoadPage('sidemenu', 'IDsidemenu');
            // Header icon title
            $("#IDheaderapptitle").attr("title", CYTranslateString("BurgerTitle"));
            // Header App title
            $("#IDheaderapptitle").html(cyProject);
            // Footer title
            if (CYTranslateString('FOOTER') != 'FOOTER')
                $("#IDfooter").html(CYTranslateString('FOOTER'));
            // Flag display + flag selector
            $("#IDheaderflag").html('<img src="CYJS/images/flags/' + cyLang + '.png"/>');
            $("#IDheaderflag").append('<ul class="ClassMenu">' + langsmenu + '</ul>');
            $("#IDheaderflag li").one("click", function (ev) {
                var url = cyServerName + cyPath + "/?lang=" + $(this).data('value');
                ev.stopPropagation();
                Object.keys(GET).forEach(function (e) {
                    // lang is already set
                    if (e != 'lang')
                        url += "&" + e + "=" + GET[e];
                });
                window.location.href = url;
            });
            // Adds a calendar
            $("#IDheadercalendar").html('<span>' + moment().format("DD/MM/YYYY") + '</span><div id="IDcalendar" style="display:none" class="ClassDatePicker" data-clear-button="false" data-auto-close="false"></div>');
            $("#IDheadercalendar>span").click(function (ev) {
                ev.stopPropagation();
                $('#IDcalendar').toggle('slide', { direction: 'up' }, 500);
            });
            $("#IDcalendar").mouseleave(function (ev) {
                ev.stopPropagation();
                $('#IDcalendar').hide('slide', { direction: 'up' }, 500);
            });
            CYLogged(cyUsername);
            CYDatePicker("#IDcalendar");
            // Loads default page
            var page = (GET['page'] != undefined) ? GET['page'] : cyDefaultPage;
            CYLoadPage({
                page: page,
                idToSet: 'IDmain',
                autoLoader: true,
                callback: CYPageLoaded
            });
        }
    });
}

/*
 * Automatically called when initial page is loaded
 */
function CYPageLoaded(html) {
    debug && console.log("login.js : CYPageLoaded");
    if ($("#IDiconrollmenu").length && typeof (window["CYSetRollMenu"]) == "function") {
        CYSetRollMenu(); // Set in rollmenu.js
        $(window).resize(CYSetRollMenu);
    }
}