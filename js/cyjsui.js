/*
 * cyjsui.js
 */
/* For dropbox demo */
var fichiers;
/* For dropping files */
var currentclient = "PLACE67";
/* For file edit */
var currentfile = "readme.txt";
/* Disable writing feature */
var demo = true;

function CyInit_cyjsui() {
    debug && console.log('cyjsui.js : CyInit_cyjsui');

    $("#IDiconrollmenu").hide();

    if (!cyIsmobile)
        CYOpenSideMenu(true, false, true);

    CYInitAllTools();

    CYMainTitle('CYJS UI', 'cog');
    $(".ClassToolBar button, .ClassToolBar input, .ClassToolBar a").click(function(event) {
        CYAlert({
            title: CYTranslateString('Click on button'),
            // In HTML, data--Text must be set with 2 -- ! Important (ex: <button data--Test="...">)
            message: $(this).attr("id") + ' - ' + $(this).data('Test')
        });
    });

    // Autocomplete on input
    var tags = [
        "actionScript",
        "appleScript",
        "asp",
        "bASIC",
        "c",
        "c++",
        "cOBOL",
        "fortran",
        "java",
        "javaScript",
        "perl",
        "pHP",
        "python",
        "ruby"
    ];
    CYAutoComplete("#tags", tags);

    // Form declaration
    var idform = "#myform";
    var buttons = [{
            text: CYTranslateString('OK'),
            click: function() {
                $(idform).dialog("close");
                $("#speedform").selectmenu({
                    select: function(event, ui) {
                        CYAlert({
                            title: 'Select id ',
                            message: ui.item.value
                        });
                    }
                });
            }
        },
        {
            text: CYTranslateString('CANCEL'),
            click: function() {
                $(idform).dialog("close");
                $("#speedform").selectmenu({
                    select: function(event, ui) {
                        CYAlert({
                            title: 'Select id ',
                            message: ui.item.value
                        });
                    }
                });
            }
        }
    ];
    $("#description").text(CYTranslateString('Default'));
    // Form in popup
    $("#buttonform").click(function(event) {
        CYForm(idform, {
            title: 'Modal popup form',
            width: 650,
            buttons: buttons
        });
    });

    // Accordion
    $("#accordion").accordion({ 
        heightStyle: "content",
        active: false,
        collapsible: true       
    });

    // Radio buttons

    $(".radio input:radio").click(function(event) {
        CYAlert({
            title: 'Click on radio',
            message: $(this).attr("id")
        });
    });
    $(".radio input:checkbox").click(function(event) {
        if ($(this).is(":checked"))
            CYAlert({
                title: 'Checked',
                message: $(this).attr("id")
            });
        else
            CYAlert({
                title: 'Unchecked',
                message: $(this).attr("id")
            });
    });

    // Modal
    $("#modalalert").click(function() {
        $(this).css({
            'color': 'blue',
            'text-decoration': 'underline'
        });
        CYAlert({
            title: 'Alert message',
            message: 'With auto close in 2 sec',
            icon: 'bell',
            autofade: 2000
        });
    });

    $("#modalconfirm").click(function() {
        $(this).css({
            'color': 'blue',
            'text-decoration': 'underline'
        });
        CYConfirm({
            title: 'Confirm box',
            message: 'Confirm message',
            icon: 'question',
            defaultbutton: 1
        }, function() {
            CYAlert({
                title: 'Confirm',
                message: 'OK !'
            });
        });
    });

    $("#modalprompt").click(function() {
        $(this).css({
            'color': 'blue',
            'text-decoration': 'underline'
        });
        CYPrompt({
            title: 'Prompt box',
            message: 'Prompt message',
            defaultvalue: '...',
            required: true,
            icon: 'fa-file-text-o',
            lines: 3
        }, function(val) {
            CYAlert({
                title: 'Prompt',
                message: val
            });
        });
    });

    // Progressbar
    $("#progressbar").progressbar({
        value: 37
    });

    // Sliders
    $("#slider").slider({
        min: 1,
        max: 10,
        value: 2,
        slide: function(event, ui) {
            $("#total").val(ui.value);
        }
    });
    $("#total").val($("#slider").slider("value"));
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function(event, ui) {
            $("#amount").val(ui.values[0] + " - " + ui.values[1]);
        }
    });
    $("#amount").val($("#slider-range").slider("values", 0) + " - " + $("#slider-range").slider("values", 1));
    $("#eq > span").each(function() {
        $(this).css({
            'height': '120px',
            'float': 'left',
            'margin': '15px'
        });
        // read initial values from markup and remove that
        var value = parseInt($(this).text(), 10);
        $(this).empty().slider({
            value: value,
            range: "min",
            animate: true,
            orientation: "vertical",
            slide: function(event, ui) {
                $("#amounteq").val($(this).attr('id') + ':' + ui.value);
            }
        });
    });

    // Menu jQuery-ui
    $("#menu").menu({
        position: { my: "left top", at: "left+10 top+10" },
        select: function(event, ui) {
            CYAlert({
                title: 'Menu id ',
                message: ui.item.attr('id')
            });
        }
    });

    // Select speed
    $("#speedform,#speed,#files,#salutation").selectmenu({
        select: function(event, ui) {
            CYAlert({
                title: 'Select id ',
                message: ui.item.value
            });
        }
    });

    // Multiselect
    // Already initialized so need to parse again
    //$("#selectable").removeClass("ClassReady");
    //CYSelectable("#selectable", {
    $("#selectable").CYSelectable({
        select: function(selectableobj, selection) {
            console.log("CYSelectable selection=" + JSON.stringify(selection));
            CYSelectableButtonsPool(selectableobj, "#select-result", selection, {
                remove: true,
                onRemove: function(selected) {
                    console.log("REMOVE id=" + selected.id + " html=" + selected.html);
                }
            });
        }
    });

    // To get the list off selected li :
    // I cannot use $("#selectable").data("ids") which is automatically calculated by CYSelectable() because the buttons can remove
    // a selected item without telling it to CYSelectable()
    $("#buttonSelectable").click(function() {
        CYAlert({
            title: 'Selectable ids',
            message: JSON.stringify($("#selectable").CYSelectable('get'))
        });
    });

    // Spinner
    CYSpinner("#spinner", {
        step: 1,
        spin: function(event, ui) {
            if ((ui.value % 2) == 0)
                CYAlert({
                    title: 'Spinner',
                    message: ui.value + '  !'
                });
        }
    });
    $("#disable").on("click", function() {
        if ($("#spinner").spinner("option", "disabled")) {
            $("#spinner").spinner("enable");
        } else {
            $("#spinner").spinner("disable");
        }
    });
    $("#destroy").on("click", function() {
        if ($("#spinner").spinner("instance")) {
            $("#spinner").spinner("destroy");
        } else {
            $("#spinner").spinner();
        }
    });
    $("#getvalue").on("click", function() {
        CYAlert({
            title: 'Spinner',
            message: $("#spinner").spinner("value")
        });
    });
    $("#setvalue").on("click", function() {
        $("#spinner").spinner("value", 5);
    });

    // Tabs
    CYTab("#tabs", {
        collapsible: false
    })

    var flipswitch = $("#flipswitch");
    // Set
    //flipswitch.val(true).prop("checked", true);
    // Unset
    flipswitch.val(false).prop("checked", false);
    // Test
    // flipswitch.is(':checked'))
    flipswitch.on("click", function() {
        CYAlert({
            title: 'Flip Switch Value',
            message: flipswitch.val()
        });
    })

    // Dropzone
    var mydropzone = CYDropZone("#IDdropzone", {
        callbackcomplete: function(file) {
            ListDocuments(currentclient);
        }
    });
    mydropzone.removeAllFiles();
    mydropzone.enable();
    // Url for file uploading
    mydropzone.options.url = cyServerName + cyPath + cyWebSvc + "?r=upload";

    // Tree wih HTML data
    CYTree("#jstree1");
    $("#jstree1").on('changed.jstree', function(e, data) {
        var i, j, result = '';
        // Clicked level - root = 1
        var level = data.node.parents.length;
        // Has children ?
        var parent = data.instance.is_parent();
        // Has father ?
        var child = data.instance.is_leaf();
        for (i = 0, j = data.selected.length; i < j; i++)
            result += CYTranslateString("Select") + " " + data.instance.get_node(data.selected[i]).text + '/id=' + data.instance.get_node(data.selected[i]).id + "<br>";
        if (result != '')
            CYAlert({
                message: result,
                width: "600"
            });
    });
    // Get selected ids, returns one id or ids array
    var treeSelectedIds = $("#jstree1").jstree('get_selected');
    // Check if id is parent
    if (treeSelectedIds > 0)
        var parent = $("#jstree1").jstree().is_parent($("#" + treeSelectedIds));
    // Get selected nodes
    var treeSelectedNodes = $("#jstree1").jstree('get_selected', true);
    // Tree with JSON data
    var mytree = [{
            id: 'idjsonnochild',
            text: 'JSON root node no child'
        },
        {
            id: 'idjsonroot2',
            text: 'JSON Root node 3 childs',
            state: {
                opened: true,
                selected: false
            },
            children: [{
                    id: "idchild1",
                    text: "JSON Child 1"
                },
                {
                    id: "idchild2",
                    text: "JSON Child 2",
                    children: [{
                            id: "idchild21",
                            text: "JSON Child 21"
                        },
                        {
                            id: "idchild22",
                            text: "JSON Child 22"
                        }
                    ]
                },
                {
                    id: "idchild3",
                    text: "JSON Child 3"
                },
            ]
        }
    ];

    CYTree("#jstree2", {
        core: {
            data: mytree
        },
        checkbox: {
            keep_selected_style: false
        },
        plugins: ["wholerow", "checkbox"]
    });
    $("#jstree2").on('changed.jstree', function(e, data) {
        var i, j, result = '';
        for (i = 0, j = data.selected.length; i < j; i++)
            result += CYTranslateString("Select") + " " + data.instance.get_node(data.selected[i]).text + '/id=' + data.instance.get_node(data.selected[i]).id + "<br>";
        if (result != '')
            CYAlert({
                message: result,
                width: "600"
            });
    });
    // Editor
    CYEditor("#IDEditor");
    CYExec({
        url: cyServerName + cyPath + cyWebSvc,
        data: {
            r: "readfile",
            dir: currentclient,
            file: currentfile
        },
        cache: true,
        sync: false,
        callbackok: function(ret, status) {
            $('#IDEditor').trumbowyg('html', ret);
        },
        callbackfail: function(ret, status) {
            $('#IDEditor').text(CYTranslateString("ERROR LOADING"));
        }
    });
    CYExec({
        url: cyServerName + cyPath + cyWebSvc,
        data: {
            r: "readfile",
            dir: currentclient,
            file: "CloudFiles.pdf"
        },
        cache: false,
        sync: false,
        dataType: "arraybuffer", // Necessaire pour CYPdfViewer
        callbackok: function(data, status) {
            CYPdfViewer("#IDpdfviewer", data);
        },
        callbackfail: function(ret, status) {
            $('#IDpdfviewer').text(CYTranslateString("ERROR LOADING"));
        }
    });
    $("#IDSaveText").on("click", function() {
        if (demo) {
            CYAlert("Demo .. ;)");
            return;
        }
        CYExec({
            url: cyServerName + cyPath + cyWebSvc,
            data: {
                r: "writefile",
                file: currentfile,
                content: $('#IDEditor').trumbowyg('html')
            },
            callbackok: function(ret, status) {
                CYAlert("OK");
            }
        });
    });

    ListDocuments(currentclient);

    $("#IDiconrollmenu").show();
}

function ListDocuments(subdir) {
    // Files info retourned by url
    var fields = {
        id: 'id',
        filetype: 'type',
        filename: 'nom',
        filesize: 'taille',
        filethumb: 'thumb',
        title: 'nom'
    };
    /* Url that gets file list inside fichiers */
    var q = cyServerName + cyPath + cyWebSvc + "?r=listfiles";
    if (subdir !== undefined && subdir !== '')
        q += "&dir=" + subdir;
    CYExec({
        url: q,
        sync: false,
        callbackok: function(ret, status) {
            $('#IDFileList').html('');
            if (!(ret < 0)) {
                fichiers = JSON.parse(ret);
                CYFileList('#IDFileList', fichiers, fields, function(thumbnail) {
                    var attrid = $(thumbnail).attr('id');
                    var ifile = attrid.substring(attrid.lastIndexOf('-') + 1);
                    if (fichiers[ifile].type == 'dir')
                        var menu = [];
                    else
                        var menu = [{
                            id: "IDThumbnailView",
                            icon: "download",
                            label: "DOWNLOAD"
                        }];
                    menu.push({
                        id: "IDThumbnailEdit",
                        icon: "edit",
                        label: "EDIT"
                    }, {
                        id: "IDThumbnailRename",
                        icon: "pencil",
                        label: "RENAME"
                    }, {
                        id: "IDThumbnailDelete",
                        icon: "remove",
                        label: "DELETE"
                    });
                    CYContextMenu(menu);
                    // Download document
                    $("#IDThumbnailView").on("click", function() {
                        var q = cyServerName + cyPath + cyWebSvc + "?r=download&f=" + fichiers[ifile].nom + '&dir=' + subdir;
                        CYURL(q, {
                            download: fichiers[ifile].nom,
                            newtab: true
                        });
                    });
                    // Rename document
                    $("#IDThumbnailRename").on("click", function() {
                        CYPrompt({
                            title: CYTranslateString('RENAME'),
                            message: CYTranslateString('FILENAME'),
                            defaultvalue: fichiers[ifile].nom,
                            required: true,
                            icon: 'fa-edit',
                            lines: 1,
                            maxlength: 50
                        }, function(val) {
                            if (demo) {
                                CYAlert("Demo .. ;)");
                                return;
                            }
                            var q = cyServerName + cyPath + cyWebSvc + "?r=rename&f=" + fichiers[ifile].nom + "&fr=" + val + '&dir=' + subdir;
                            CYExec({
                                url: q,
                                callbackok: function() {
                                    $(thumbnail).hide().fadeIn('fast');
                                    ListDocuments(subdir);
                                }
                            });
                        });
                    });
                    // Edit document
                    $("#IDThumbnailEdit").on("click", function() {
                        if (fichiers[ifile].type.indexOf("text/") == -1 && fichiers[ifile].type.indexOf("x-empty") == -1 &&
                            fichiers[ifile].type.indexOf("pdf") == -1)
                            CYAlert("Cannot edit that file.");
                        else {
                            CYExec({
                                url: cyServerName + cyPath + cyWebSvc,
                                data: {
                                    r: "readfile",
                                    dir: currentclient,
                                    file: fichiers[ifile].nom
                                },
                                dataType: fichiers[ifile].type.indexOf("text/") != -1 ? "text" : "arraybuffer",
                                callbackok: function(ret, status) {
                                    if (fichiers[ifile].type.indexOf("text/") != -1) {
                                        currentfile = fichiers[ifile].nom;
                                        $('#IDEditor').trumbowyg('html', ret);
                                    } else if (fichiers[ifile].type.indexOf("pdf") != -1) {
                                        if (ret == -1)
                                            $("#IDpdfviewer").html(CYTranslateString("ERROR LOADING"));
                                        else
                                            CYPdfViewer("#IDpdfviewer", ret);
                                    }
                                }
                            });
                        }
                    });
                    // Delete document
                    $("#IDThumbnailDelete").on("click", function() {
                        if (demo) {
                            CYAlert("Demo .. ;)");
                            return;
                        }
                        var q = cyServerName + cyPath + cyWebSvc + "?r=delete&f=" + fichiers[ifile].nom + '&dir=' + subdir;
                        CYExec({
                            url: q,
                            callbackok: function() {
                                $(thumbnail).remove();
                                CYAlert("File deleted.");
                            }
                        });
                    });
                });
            }
        }
    });
}

function InputEvent(date, allday, jsevent, view) {
    CYPrompt({
        label: "Event",
        width: 250
    }, function(str) {
        var newEvent = {
            id: CYUniqueId(),
            title: str,
            start: date,
            allDay: allday,
            backgroundColor: "#f6f6f6"
        };
        StoreEvent(newEvent);
        return true;
    });
}

function StoreEvent(event) {
    $('.ClassCalendar').fullCalendar('renderEvent', event, 'stick');
}

function UpdateEvent(event, element) {
    CYPrompt({
        label: "Event",
        defaultvalue: event.title
    }, function(str) {
        event.title = str;
        if (event.title == '')
            $('.ClassCalendar').fullCalendar('removeEvents', event.id);
        else
            $('.ClassCalendar').fullCalendar('updateEvent', event);
        return true;
    });
}

function SetDefaultEvents() {
    $('.ClassCalendar').fullCalendar('renderEvents', [{
            id: CYUniqueId(),
            resourceId: "B",
            title: CYTranslateString("Now"),
            description: CYTranslateString("Now"),
            start: moment(),
            backgroundColor: "#f6f6aa"
        },
        {
            id: CYUniqueId(),
            resourceId: "C",
            title: CYTranslateString("3 hours before"),
            description: CYTranslateString("3 hours before"),
            start: moment().add(-3, "hours"),
            backgroundColor: "#f6f6bb"
        },
        {
            id: CYUniqueId(),
            resourceId: "C",
            title: CYTranslateString("2 hours later"),
            description: CYTranslateString("2 hours later"),
            start: moment().add(2, "hours"),
            backgroundColor: "#f6f6cc"
        },
        {
            id: CYUniqueId(),
            resourceId: "D",
            title: CYTranslateString("1 day before"),
            description: CYTranslateString("1 day before"),
            start: moment().add(-1, "days"),
            backgroundColor: "#f6f6dd"
        },
        {
            id: CYUniqueId(),
            resourceId: "D",
            title: CYTranslateString("1 day after"),
            description: CYTranslateString("1 day after"),
            start: moment().add(1, "days"),
            backgroundColor: "#f6f6ee"
        },
        {
            id: CYUniqueId(),
            resourceId: "E",
            title: CYTranslateString("Next week midday"),
            description: CYTranslateString("Next week midday"),
            start: moment().add(7, "days").set({ hour: 12, minute: 0, second: 0, millisecond: 0 }),
            backgroundColor: "#f6f6ff"
        }
    ], 'stick');
}