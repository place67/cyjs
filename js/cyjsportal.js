/*
 * cyjsportal.js
 */

function CyInit_cyjsportal()
{
    var url = "https://cyjs.fr";

    if (!cyIsmobile)
        CYOpenSideMenu(true, false, true);

    CYMainTitle('CYJS Portal', 'portal');
    CYInitAllTools();

    CyInit_cyjsportalexec(url);
    $("#panelurlinput").val(url);

    $("#panelurlok").click(function ()
    {
        CyInit_cyjsportalexec($("#panelurlinput").val().trim());
        $("#panelurlinput").focus();
    });
    $("#panelurlclear").click(function ()
    {
        var doc = document.getElementById('urlresult').contentWindow.document;
        doc.open();
        doc.write("");
        doc.close();
        $("#panelurlinput").focus();
    });
    setTimeout(function(){ CYRestoreDesktop(1), 50 });
}

function CyInit_cyjsportalexec(url)
{
    $('#IDportalloader').css("z-index", CYMaxZindex() + 1).fadeIn("slow");
    CYExec(
    {
        url: url,
        sync: false,
        cache: true,
        type: "GET",
        crossDomain: true,
        callbackok: function (ret, status)
        {
            var doc = document.getElementById('urlresult').contentWindow.document;
            doc.open();
            doc.write(ret);
            doc.close();
            $('#IDportalloader').fadeOut("slow", function ()
            {
                $(this).css("z-index", 0);
            });
        },
        callbackfail: function (xhr)
        {
            var doc = document.getElementById('urlresult').contentWindow.document;
            doc.open();
            doc.write(xhr.responseText);
            doc.close();
            $('#IDportalloader').fadeOut("slow", function ()
            {
                $(this).css("z-index", 0);
            });
        }
    });
}
