<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="apple-mobile-web-app-title" content="CYJS">
    <link rel="apple-touch-icon" href="./images/favicon.png">
    <link rel="apple-touch-startup-image" href="./images/favicon.png">
    <meta name="description" content="A CYSTEME CYJS FRAMEWORK PROJECT - cyjs.fr">
    <meta name="author" content="CYSTEME CYJS FRAMEWORK">
    <meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no,password=no">
    <title>
        PROJECT
    </title>
    <link rel="icon" href="images/favicon.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="CYJS/css/font-awesome.css?v=<?= date('YmdHis', filemtime('CYJS/css/font-awesome.css')) ?>">
    <link rel="stylesheet" type="text/css" href="CYJS/css/jquery-ui.css?v=<?= date('YmdHis', filemtime('CYJS/css/jquery-ui.css')) ?>">
    <link rel="stylesheet" type="text/css" href="CYJS/css/dropzone.css?v=<?= date('YmdHis', filemtime('CYJS/css/dropzone.css')) ?>">
    <script src="CYJS/js/localforage.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/localforage.min.js')) ?>"></script>
    <script src="CYJS/js/sprintf.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/sprintf.min.js')) ?>"></script>
    <script src="CYJS/js/jquery.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/jquery.min.js')) ?>"></script>
    <script src="CYJS/js/jquery.ajax.arraybuffer.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/jquery.ajax.arraybuffer.min.js')) ?>"></script>
    <script src="CYJS/js/jquery-ui.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/jquery-ui.min.js')) ?>"></script>
    <script src="CYJS/js/jquery.ui.touch-punch.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/jquery.ui.touch-punch.min.js')) ?>"></script>
    <script src="CYJS/js/jquery.inputmask.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/jquery.inputmask.min.js')) ?>"></script>
    <script src="CYJS/js/jquery.dataTables.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/jquery.dataTables.min.js')) ?>"></script>
    <script src="CYJS/js/jstree/jstree.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/jstree/jstree.min.js')) ?>"></script>
    <script src="CYJS/js/moment.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/moment.min.js')) ?>"></script>
    <script src="CYJS/js/datepicker/datepicker.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/datepicker/datepicker.min.js')) ?>"></script>
    <script src="CYJS/js/dropzone.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/dropzone.min.js')) ?>"></script>
    <script src="CYJS/js/fullcalendar/fullcalendar.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/fullcalendar/fullcalendar.min.js')) ?>"></script>
    <script src="CYJS/js/fullcalendar/scheduler.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/fullcalendar/scheduler.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/trumbowyg.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/trumbowyg.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/plugins/base64/trumbowyg.base64.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/plugins/base64/trumbowyg.base64.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/plugins/colors/trumbowyg.colors.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/plugins/colors/trumbowyg.colors.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/plugins/emoji/trumbowyg.emoji.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/plugins/emoji/trumbowyg.emoji.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/plugins/fontfamily/trumbowyg.fontfamily.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/plugins/fontfamily/trumbowyg.fontfamily.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/plugins/fontsize/trumbowyg.fontsize.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/plugins/fontsize/trumbowyg.fontsize.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/plugins/preformatted/trumbowyg.preformatted.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/plugins/preformatted/trumbowyg.preformatted.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/plugins/specialchars/trumbowyg.specialchars.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/plugins/specialchars/trumbowyg.specialchars.min.js')) ?>"></script>
    <script src="CYJS/js/trumbowyg/plugins/table/trumbowyg.table.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/trumbowyg/plugins/table/trumbowyg.table.min.js')) ?>"></script>
    <script src="CYJS/js/pdfobject.min.js?v=<?= date('YmdHis', filemtime('CYJS/js/pdfobject.min.js')) ?>"></script>
    <script src="CYJS/js/lang.default.js?v=<?= date('YmdHis', filemtime('CYJS/js/lang.default.js')) ?>"></script>
    <script src="CYJS/js/CYJS.js?v=<?= date('YmdHis', filemtime('CYJS/js/CYJS.js')) ?>"></script>
    <script src="js/config.js?v=<?= date('YmdHis', filemtime('js/config.js')) ?>"></script>
    <?php include 'index.head.php' ?>
</head>

<body>
    <page id="IDpage" style="display:none">
        <header id="IDheader">
            <i id="IDheadericonmenu" class="fa fa-bars ClassZoomInOut"></i>
            <span id="IDheaderapptitle">PROJECT</span>
            <i id="IDheadericonmain" title=""></i>
            <span id="IDheadermaintitle"></span>
            <i id="IDheadericonuser" class="fa fa-ellipsis-v ClassZoomInOut" title="Mes infos"></i>
            <i id="IDheadericonnetwork" class="fa fa-wifi"></i>
            <span id="IDheaderusername"></span>
            <span id="IDheadercalendar"></span>
            <span id="IDheaderflag"></span>
        </header>
        <page-content id="IDpagecontent">
            <sidemenu id="IDsidemenu"></sidemenu>
            <usermenu id="IDusermenu" style="display:none"></usermenu>
            <main id="IDmain"></main>
            <rollmenu id="IDiconrollmenu"></rollmenu>
        </page-content>
        <footer id="IDfooter" title="Edité par PL@CE67&#10avec CYJS Javascript Framework&#10contact@place67.com">
            CYJS JAVASCRIPT FRAMEWORK<i class="fa fa-copyright"></i> <a target='_blank' href='http://place67.com'>PL@CE67</a> (2017-2023)
        </footer>
    </page>
    <div id="dummy" class="ClassAlert ClassConfirm" style="display:none"></div>
    <script>
        $(document).ready(function() {
            CYInit(function() {
                document.title = cyString['PROJECT'];
                CYTranslate('IDheader');
                CYTranslate('IDfooter');
                // Loads lateral menu in background
                CYLoadPage('sidemenu', 'IDsidemenu');
                // Loads user menu in background
                CYLoadPage('usermenu', 'IDusermenu');
                // Loads roll menu in background
                CYLoadPage('rollmenu', 'IDiconrollmenu');
                CYLoginFct();
                window.onbeforeunload = function() {
                    return 'Leave ?';
                };
                // Default can be overriden in your own logoff
                window.onunload = function() {
                    CYLogoff();
                };
            });
        });
    </script>
</body>

</html>
