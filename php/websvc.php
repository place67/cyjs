<?php

session_start();

extract($_REQUEST);

// Taille maxi d'un cot� d'image pour thumbnails
$imgcrop = 150;

$cyPath = '/cyjs'; // Must match cyPath in config.js
$uploaddir = '../data/files/';

if (!empty($dir))
    $uploaddir .= $dir . '/';

if (!file_exists($uploaddir))
    mkdir($uploaddir, 0777, true);

error_log("========================================================================");
error_log("USER=". $_SESSION["CYJS"]["user"] ." - REQUEST=".print_r($_REQUEST,true));

switch ($r)
{
    case 'login' : // ?r=login&u=username&p=password
        session_destroy();
        session_start();
        $_SESSION["CYJS"]["user"] = $u;
        $_SESSION["CYJS"]["password"] = $p;
        echo '{ "success": true, "error": "" , "result": "" }';
        break;
    case 'logoff' : // ?r=logoff
        session_destroy();
        echo 'ok';
        break;
    case 'listfiles' : // ?r=listfiles
        if ($dir = opendir($uploaddir))
        {
            $i = 0;
            $list = [];
            while (($file = readdir($dir)) !== false)
            {
                // ignore dot files
                if (substr($file, 0, 1) == '.')
                    continue;
                if (!is_dir($uploaddir . $file))
                {
                    $e = explode(".", $file);
                    $l = count($e);
                    $ext = $e[$l - 1];
                    if (strtoupper($ext) == "PNG" || strtoupper($ext) == "JPG" || strtoupper($ext) == "JPEG") // too slow for demo || strtoupper($ext) == "PDF")
                        $thumb = generateThumbnail($uploaddir . $file, $imgcrop, $imgcrop);
                    else
                        $thumb = "";
                    $stat = stat($uploaddir . $file);
                    if (substr(strtolower($ext), 0, 3) == "htm")
                        $mime = "text/html";
                    else
                        $mime = mime_content_type($uploaddir . $file);
                    $list[] = [ 'id' => $i++, 'nom' => $file, 'type' => $mime, 'taille' => filesize($uploaddir . $file), 'thumb' => base64_encode($thumb) ];

                }
                else
                {
                    $subfiles = glob($uploaddir . $file . "/*.*");
                    $nb = count($subfiles);
                    $list[] = [ 'id' => $i++, 'nom' => $file, 'type' => 'dir', 'taille' => $nb, 'thumb' => "" ];
                }
            }
            closedir($dir);
        }
        echo json_encode($list);
        break;
    case 'upload' : // ?r=upload&dir=subdir
        $filename = basename($_FILES['file']['name']);
        if (substr($filename, 0, 1) == '.')
        {
            unlink($_FILES['file']['tmp_name']);
            echo '-1';
        }
        else
        {
            $uploadfile = $uploaddir . $dir . $filename;
            move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
            echo 'ok';
        }
        break;
    case 'delete' : // ?r=delete&f=nom&dir=subdir
        echo 'Demo .. ;)';
        //unlink($uploaddir . $f);
        break;
    case 'rename' : // ?r=rename&f=nom&fr=val&dir=subdir
        echo 'Demo .. ;)';
        //rename($uploaddir . $f, $uploaddir . $fr);
        break;
    case 'download' : // ?r=download&f=nom&dir=subdir
        header('Content-Type: ' . mime_content_type($uploaddir . $f));
        header('Content-Disposition: inline');//attachment; filename="' . $f . '"');
        readfile($uploaddir . $f);
        break;
    case 'readfile' : // ?r=readfile&file=readme.txt
        if (substr($file, 0, 1) == '.')
            echo "Filename forbidden";
        else if (is_readable($uploaddir . $file))
        {
            header('Content-Type: ' . mime_content_type($uploaddir . $file));
            header('Content-Disposition: attachment; filename="' . $file . '"');
            readfile($uploaddir . $file);
        }
        else   
            echo "-1";
        break;
    case 'writefile' : // ?r=writefile&content=....
        echo 'Demo .. ;)';
        break;
        if (substr($file, 0, 1) == '.')
            echo "Filename forbidden";
        else
            echo file_put_contents($uploaddir . $file, $content);
        break;
    case 'readpasswd' :
        $clients = [];
        if (($fp = fopen("files/passwd", "r")) !== FALSE)
        {
            while (($data = fgetcsv($fp, 1000, ":")) !== FALSE)
            {
                $c["user"] = $data[0];
                $c["password"] = $data[1];
                $c["id"] = $data[2];
                $c["groupid"] = $data[3];
                $c["comment"] = $data[4];
                $c["home"] = $data[5];
                $c["shell"] = $data[6];
                $clients[] = $c;
            }
        }
        echo json_encode($clients);
        break;
    case 'readprocess' :
        $process = [];
        if (($fp = popen("ps aux | awk '{ print $1 \" \" $2 \" \" $3 \" \" $4 \" \" $5 \" \" $6 \" \" $7 \" \" $8 \" \" $9 \" \" $10 \" \" $11 }'", "r")) !== FALSE)
        {
            fgets($fp, 1000);
            while (($line = fgets($fp, 1000)) !== FALSE)
            {
                $data = explode(" ", $line);
                $c["user"] = $data[0];
                $c["pid"] = $data[1];
                $c["cpu"] = $data[2];
                $c["mem"] = $data[3];
                $c["vsz"] = $data[4];
                $c["rss"] = $data[5];
                $c["tty"] = $data[6];
                $c["stat"] = $data[7];
                $c["start"] = $data[8];
                $c["time"] = $data[9];
                $c["command"] = $data[10];
                $process[] = $c;
            }
        }
        echo json_encode($process);
        break;
    case 'json2excel' :
        $lines = json_decode($lines, true);
        foreach ($lines as $l)
            error_log("json2excel args=".print_r($l,true));
        require_once dirname(__FILE__) . '/PHPExcel/Classes/PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("CYSTEME")
                                     ->setLastModifiedBy("Luke")
                                     ->setTitle("Office XLSX Document")
                                     ->setSubject("Office XLSX Document")
                                     ->setDescription("Test document for Excel XLSX generated by CYJS framework")
                                     ->setKeywords("office excel openxml php")
                                     ->setCategory("Test result file");
        $headerstyleArray = array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'ffffff'),
                'size'  => 15,
                'name'  => 'Verdana'
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'007fff')
            )
        );
        $styleArray = array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 15,
                'name'  => 'Verdana'
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb'=>'ffffff')
            )
        );
        $header = explode(';', $header);
        $col = 65; // 'A'
        $row = 1;
        foreach($header as $h)
        {
            $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue(chr($col) . $row, $h);
            $objPHPExcel->getActiveSheet()->getStyle(chr($col) . $row)->applyFromArray($headerstyleArray);
            $col++;
        }
        foreach ($lines as $l)
        {
            $col = 65;
            $row++;
            foreach($l as $k => $c)
            {
                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue(chr($col) . $row, $c);
                $objPHPExcel->getActiveSheet()->getStyle(chr($col) . $row)->applyFromArray($styleArray);
                $col++;
            }
        }
        $objPHPExcel->getActiveSheet()->setTitle('Process table');
        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        if ($download == '')
            $download = "export.xlsx";
        header('Content-Disposition: attachment;filename="' . $download . '"');
        header('Cache-Control: max-age=0');
        header ('Expires: Mon, 1 Jan 2017 00:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        break;
}

function generateThumbnail($img, $width, $height, $quality = 90)
{
    try
    {
        $imagick = new Imagick($img);
        $imagick->setImageFormat('png');
        $imagick->setImageCompressionQuality($quality);
        $imagick->thumbnailImage($width, $height, true, false);
        return ($imagick);
    } catch (ImagickException $e)
    {
        error_log($e);
        return ("");
    }
}

function htmlToPDF($html)
{
    $html2pdf = new HTML2PDF('P', 'A4');
    $html2pdf->writeHTML($html);
    $file = $html2pdf->Output('temp.pdf','F');
}

?>
