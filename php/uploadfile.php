<?php

// usage: uploadfile.php?f=html/page.html&content=XXXXXXXXXXXXXXXXXXXXXXXXXXX.....

extract($_REQUEST);
// User can change filename .. :(
if (substr($f, 0, 1) == '.')
{
    echo "Oops .. :(";
    exit;
}
// Check user access here or bypass test
session_start();
if ($_SESSION["CYJS"]["useraccount"]["user"] == "")
{
    echo 'Demo .. ;)';
    exit;
}
// Update here to permit file writing
if ($_SESSION["CYJS"]["useraccount"]["user"] != "")
{
    file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/" . $f, $content);
    echo "Ok";
}
// That echo will be displayed in CYAlert()
else
    echo "Oops .. :(";
?>
